﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Npgsql;
using System.Configuration;
using System.IO;
using System.Text;
namespace IOTServiceAPI.Models
{
    public class IOTValidateGeofence
    {


        public static DataTable GetGeofence(Int64 user_id)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                NpgsqlCommand cmd = new NpgsqlCommand();

                connection.Open();
                cmd.Connection = connection;

                cmd.CommandText = "SELECT geofence_user_mapping.blnmaster,geofencemaster.gc_id,geofencemaster.geofence_name, geofencemaster.geofenec_desc,	geofencemaster.lat_lng FROM geofence_user_mapping geofence_user_mapping, geofencemaster geofencemaster WHERE geofence_user_mapping.gc_id = geofencemaster.gc_id AND geofencemaster.lat_lng is not null  AND  geofencemaster.lat_lng <>'' AND geofence_user_mapping.user_id =@user_id and geofence_user_mapping.is_active=true  and geofencemaster.is_active=true";
                cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                DataTable dt = new DataTable();
                NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(cmd);
                myDataAdapter.Fill(dt);
                myDataAdapter.Dispose();
                cmd.Dispose();
                //dt.Dispose();
                connection.Close();
                return dt;
            }
        }

        public static int point_inside_polygon(Int64 user_id, double x, double y, out Int32 gc_id, out string within_gc_id)
        {
            Int32 m_gc_id = 0;
            bool m_geofenece_alert = false;
            bool within_m_geofenece = false;
            gc_id = 0;
            within_gc_id = "";
            try
            {

                DataSet ds = new DataSet();
                DataTable dt = GetGeofence(user_id);
                if (dt.Rows.Count == 0) return 2;

                DataRow[] drmaster = dt.Select("blnmaster=true");
                foreach (DataRow dr in drmaster)
                {
                    if (dr["blnmaster"] != null && (Convert.ToBoolean(dr["blnmaster"])))
                    {
                        m_gc_id = Convert.ToInt32(dr["gc_id"].ToString());
                    }
                }

                foreach (DataRow dr in dt.Rows)
                {
                    int counter = 0;
                    int n = 0;
                    int i = 0;
                    double p1x = 0;
                    double p1y = 0;
                    double p2x = 0;
                    double p2y = 0;
                    double xinters = 0;

                    var lat_lngArray = dr["lat_lng"].ToString().Split(';');
                    gc_id = Convert.ToInt32(dr["gc_id"].ToString());

                    n = lat_lngArray.Length;
                    p1x = Convert.ToDouble(lat_lngArray[0].ToString().Split(',')[0]); //latitude
                    p1y = Convert.ToDouble(lat_lngArray[0].ToString().Split(',')[1]); //longitude

                    for (i = 0; i <= n - 1; i++)
                    {
                        //p2x = Convert.ToDouble(ds.Tables(0).Rows(i % n)(0));
                        //p2y = Convert.ToDouble(ds.Tables(0).Rows(i % n)(1));
                        p2x = Convert.ToDouble(lat_lngArray[i % n].ToString().Split(',')[0]); //latitude
                        p2y = Convert.ToDouble(lat_lngArray[i % n].ToString().Split(',')[1]); //longitude

                        if (y > Math.Min(p1y, p2y))
                        {
                            if (y <= Math.Max(p1y, p2y))
                            {
                                if (x <= Math.Max(p1x, p2x))
                                {
                                    if (p1y != p2y)
                                    {
                                        xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x;
                                    }
                                    if ((p1x == p2x) | (x <= xinters))
                                    {
                                        counter = counter + 1;
                                    }
                                }
                            }
                        }

                        p1x = p2x;
                        p1y = p2y;
                    }


                    if (counter % 2 != 0)
                    {

                        if (within_gc_id == "")
                        {
                            within_gc_id = Convert.ToString(gc_id);
                        }
                        else
                        {
                            within_gc_id = within_gc_id + "," + gc_id;
                        }

                        if (m_gc_id == gc_id)
                        {
                            within_m_geofenece = true;
                        }
                    }
                }

                if (m_gc_id == 0) return 2;
                if (within_m_geofenece)
                {
                    gc_id = m_gc_id;
                    m_geofenece_alert = false;
                    return 0;
                }
                else
                {
                    gc_id = m_gc_id;
                    m_geofenece_alert = true;
                    return 1;
                }

            }
            catch (Exception ex)
            {

              //  ErrorLog.LogGeofeneceError("IOTMob_JSON_TM_v1_GeofeneceError", user_id + ":" + x + "," + y + "=>" + ex.ToString(), ConfigurationManager.AppSettings["flgParseError"].ToString(), false);
                ErrorLog.SaveLogToDatabase(user_id, "M-dataobj-GeofeneceError", ex.ToString(), user_id + ":" + x + "," + y);
                return 0;
            }

        }



    }


}