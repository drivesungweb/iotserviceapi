﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTServiceAPI.Models;

namespace IOTService.Models
{
    public class IOTMobDataParse
    {
        public Task<bool> TM_IOTMobData(dynamic jsonIOT, string strjson)
        {
            try
            {
                //ErrorLog.SaveLogToDatabase(0, "M-dataobj-save", "", strjson);
                TM_LocationData(jsonIOT, strjson);

                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(0, "TM_IOTMobData", "", ex.ToString());
                return Task.FromResult(false);

            }
        }

        private bool TM_LocationData(dynamic jsonIOT, string strjson)
        {
            try
            {
                #region variable declaration adn json packet value assign
                string mobileno = jsonIOT.mobileno;
                long deviceId = jsonIOT.DeviceID;
                string state = jsonIOT.state;
                decimal latitude = jsonIOT.lat;
                decimal longitude = jsonIOT.lng;
                decimal speed = jsonIOT.Speed;
                decimal heading = jsonIOT.Heading;
                string time = jsonIOT.time;
                string date = jsonIOT.date;
                long sequencenumbr = 0;
                decimal dist = jsonIOT.dist;
                int GPSValid = jsonIOT.GPSValid;
                int History = jsonIOT.History;
                int ign = jsonIOT.ign;
                int is_self_driving = 0;
                try
                {
                    is_self_driving = jsonIOT.is_self_driving;
                }
                catch
                {
                }
                if (is_self_driving == null) is_self_driving = 0;

                string provTime = "";
                string prov = "";
                string ptype = "";
                string hardware_id = "";
                try
                {
                    provTime = jsonIOT.provTime;
                    prov = jsonIOT.prov;
                    ptype = jsonIOT.ptype;
                    hardware_id = jsonIOT.hardware_id;
                }
                catch
                {
                }
                if (provTime == null) provTime = "";
                if (prov == null) prov = "";
                if (ptype == null) ptype = "";
                if (hardware_id == null) hardware_id = "";

                DateTime dtDateTime = Convert.ToDateTime(ConvertDateFormat(date) + " " + time);
                string RecordTimeStamp = ConvertDateFormat(date) + " " + time;
                //DateTime dtDateTime = ConvertToDatetimeFormat(date.ToString(), time.ToString());

                int PktType = 1;
                int mCall = 0;
                int AccX = 0;
                int AccY = 0;
                int AccZ = 0;
                string DTCData = string.Empty;
                int intBreak = 0;
                int intacceleration = 0;
                int intOvrspd = 0;
                int intweather = 0;
                int intgps_error = 0;
                int intobd_error = 0;

                int intgeo_fence = 0;
                #endregion

                #region for check vehicle in geofenec
                ////intgeo_fence => 0=out of geofence,1=with in geofence,2=geofence not assign
                int gc_id = 0;
                string within_gc_id = "";
                //intgeo_fence = IOTValidateGeofence.point_inside_polygon(deviceId, Convert.ToDouble(latitude), Convert.ToDouble(longitude), out gc_id, out within_gc_id);
                #endregion

                #region Json object parsing
                JArray Commands = jsonIOT.@params;
                if (Commands != null)
                {
                    #region foreach
                    foreach (JObject items in Commands)
                    {
                        dynamic jsonChildObject = items;
                        string command = jsonChildObject.command;
                        if (command == "Accelerometer")
                        {
                            #region Accelerometer command
                            AccX = jsonChildObject.@params.x;
                            AccY = jsonChildObject.@params.y;
                            AccZ = jsonChildObject.@params.z;
                            #endregion
                        }
                        else if (command == "alarms")
                        {
                            #region alarm command
                            JArray paramCommands = jsonChildObject.@params;
                            foreach (JObject item in paramCommands)
                            {
                                dynamic jsonalarmObject = item;
                                string alarmKey = jsonalarmObject.key;
                                if (alarmKey == "Break")
                                {
                                    intBreak = 1;
                                }
                                else if (alarmKey == "acceleration")
                                {
                                    intacceleration = 1;
                                }
                                else if (alarmKey == "Ovspd")
                                {
                                    intOvrspd = 1;
                                }
                                else if (alarmKey == "Call")
                                {
                                    mCall = 1;
                                }
                                else if (alarmKey == "Ovspd")
                                {
                                    intOvrspd = 1;
                                }
                                else if (alarmKey == "gps_error")
                                {
                                    intgps_error = 1;
                                }
                                else if (alarmKey == "obd_error")
                                {
                                    intobd_error = 1;
                                }
                                else if (alarmKey.ToLower() == "bad_weather")
                                {
                                    intweather = 1;
                                }
                                else if (alarmKey == "Crnr" || alarmKey == "n_crnr")
                                {
                                    #region Crnr and n_crnr
                                    string crnr_state = jsonalarmObject.@params.state;
                                    decimal crnr_latitude = jsonalarmObject.@params.lat;
                                    decimal crnr_longitude = jsonalarmObject.@params.lng;
                                    decimal crnr_speed = jsonalarmObject.@params.Speed;
                                    decimal crnr_heading = jsonalarmObject.@params.Heading;
                                    string crnr_time = jsonalarmObject.@params.time;
                                    string crnr_date = jsonalarmObject.@params.date;
                                    decimal crnr_dist = jsonalarmObject.@params.dist;
                                    int crnr_GPSValid = jsonalarmObject.@params.GPSValid;
                                    DateTime crnr_dtDateTime = Convert.ToDateTime(ConvertDateFormat(crnr_date) + " " + crnr_time);
                                    string crnRecordTimeStamp = ConvertDateFormat(date) + " " + time;
                                    if (alarmKey == "Crnr")
                                    {
                                        SaveLocationDataToDatabase(strjson, deviceId, mobileno, crnr_state, crnr_latitude, crnr_longitude, crnr_speed, Convert.ToInt32(crnr_heading), crnr_dtDateTime, sequencenumbr, crnr_dist, crnr_GPSValid, History, ign, AccX, AccY, AccZ, 0, 0, 1, 0, PktType, 0, 0, intgeo_fence, gc_id, within_gc_id, intgps_error, intobd_error, crnRecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, null, null, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                                    }
                                    else if (alarmKey == "n_crnr")
                                    {
                                        SaveLocationDataToDatabase(strjson, deviceId, mobileno, crnr_state, crnr_latitude, crnr_longitude, crnr_speed, Convert.ToInt32(crnr_heading), crnr_dtDateTime, sequencenumbr, crnr_dist, crnr_GPSValid, History, ign, AccX, AccY, AccZ, 0, 0, 0, 0, PktType, 0, 0, intgeo_fence, gc_id, within_gc_id, intgps_error, intobd_error, crnRecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, null, null, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

                                    }
                                    #endregion
                                }

                            }
                            #endregion
                        }
                        else if (command == "OBDdata")
                        {
                            #region OBD command
                            foreach (JProperty OBDItem in jsonChildObject.@params)
                            {
                                int OBDindex = Convert.ToInt32(OBDItem.Name, 16);
                                if (OBDindex < arrOBDData.Length)
                                {
                                    arrOBDData[OBDindex, 1] = OBDItem.Value.ToString();
                                }
                            }
                            #endregion
                        }
                        else if (command == "DTC")
                        {
                            DTCData = jsonChildObject.@params;
                        }
                    }
                    #endregion
                }

                #endregion

                SaveLocationDataToDatabase(strjson, deviceId, mobileno, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, History, ign, AccX, AccY, AccZ, intBreak, intOvrspd, 0, mCall, PktType, intacceleration, intweather, intgeo_fence, gc_id, within_gc_id, intgps_error, intobd_error, RecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, null, null, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

                //comment OBD update because OBD data not recived in this version             
                //SaveOBDDataToDatabase(strjson, deviceId, latitude, longitude, mobileno, 1, dtDateTime, sequencenumbr, DTCData, arrOBDData, RecordTimeStamp);

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(0, "M-dataobj-ParseError", ex.ToString(), strjson);
                return false;
            }
        }

        public Task<bool> Drivesung_IOTTextData(string rowDataPacket)
        {
            try
            {
                string[] rowDataArray = rowDataPacket.Split(new string[] { "," }, StringSplitOptions.None); //seperate recived packtes by comma

                #region variable declaration and packet value parsing
                string mobileno = "";
                long sequencenumbr = Convert.ToInt64(rowDataArray[1]);
                int GPSValid = Convert.ToInt16(rowDataArray[3]);
                int History = Convert.ToInt16(rowDataArray[4]);

                string state = Convert.ToString(rowDataArray[5]);

                string RecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowDataArray[6]));
                DateTime dtDateTime = Convert.ToDateTime(RecordTimeStamp);

                decimal heading = Convert.ToDecimal(rowDataArray[7]);
                decimal latitude = Convert.ToDecimal(rowDataArray[8]);
                decimal longitude = Convert.ToDecimal(rowDataArray[9]);
                int ign = Convert.ToInt16(rowDataArray[10]);
                int is_self_driving = Convert.ToInt16(rowDataArray[11]);
                decimal speed = Convert.ToDecimal(rowDataArray[12]);
                decimal dist = Convert.ToDecimal(rowDataArray[13]);
                long deviceId = Convert.ToInt64(rowDataArray[14]);
                long user_id = Convert.ToInt64(rowDataArray[15]);  //new
                string hardware_id = Convert.ToString(rowDataArray[16]);
                string provTime = Convert.ToString(rowDataArray[17]);
                string prov = Convert.ToString(rowDataArray[18]);
                string ptype = Convert.ToString(rowDataArray[19]);
                decimal accuracy = 0;
                if (rowDataArray[20].ToString() != "null") accuracy = Convert.ToDecimal(rowDataArray[20]);  //new
                string timeZone = Convert.ToString(rowDataArray[21]); //new
                int intBreak = Convert.ToInt16(rowDataArray[22]);
                int intacceleration = Convert.ToInt16(rowDataArray[23]);
                int intOvrspd = Convert.ToInt16(rowDataArray[24]);
                int mCall = Convert.ToInt16(rowDataArray[25]);
                int intweather = Convert.ToInt16(rowDataArray[26]);

                int intCrnrVal = Convert.ToInt16(rowDataArray[27]);
                int intCrnr = intCrnrVal;
                if (intCrnr != 1) intCrnr = 0;

                #region for check vehicle in geofenec
                int intgeo_fence = 0;
                ////intgeo_fence => 0=out of geofence,1=with in geofence,2=geofence not assign
                int gc_id = 0;
                string within_gc_id = "";
                //intgeo_fence = IOTValidateGeofence.point_inside_polygon(deviceId, Convert.ToDouble(latitude), Convert.ToDouble(longitude), out gc_id, out within_gc_id);
                #endregion

                #region Crnr packet paring
                if (intCrnrVal != 0)
                {
                    string rowCrnrPacket = Convert.ToString(rowDataArray[28]);
                    string[] rowCrnrPacketArray = rowCrnrPacket.Split(new string[] { ":" }, StringSplitOptions.None); //seperate recived packtes by :
                    long Crnrseqno = Convert.ToInt64(rowCrnrPacketArray[0]);
                    string crnRecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowCrnrPacketArray[1]));
                    DateTime crnr_dtDateTime = Convert.ToDateTime(crnRecordTimeStamp);
                    int crnr_GPSValid = Convert.ToInt16(rowCrnrPacketArray[2]);
                    decimal crnr_heading = Convert.ToDecimal(rowCrnrPacketArray[3]);
                    decimal crnr_latitude = Convert.ToDecimal(rowCrnrPacketArray[4]);
                    decimal crnr_longitude = Convert.ToDecimal(rowCrnrPacketArray[5]);
                    decimal crnr_speed = Convert.ToDecimal(rowCrnrPacketArray[6]);
                    string crnr_provTime = Convert.ToString(rowCrnrPacketArray[7]);
                    string crnr_prov = Convert.ToString(rowCrnrPacketArray[8]);
                    string crnr_ptype = Convert.ToString(rowCrnrPacketArray[9]);
                    string crnr_accuracy = Convert.ToString(rowCrnrPacketArray[10]);
                    SaveLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, crnr_latitude, crnr_longitude, crnr_speed, Convert.ToInt32(crnr_heading), crnr_dtDateTime, Crnrseqno, 0, crnr_GPSValid, History, ign, 0, 0, 0, 0, 0, intCrnr, 0, 1, 0, 0, intgeo_fence, gc_id, within_gc_id, 0, 0, crnRecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
                #endregion

                Int32 intcallduration = 0, intpenalize = 0;
                double acc_x = 0, acc_y = 0, acc_z = 0, gravity_x = 0, gravity_y = 0,
                    gravity_z = 0, gyroscope_x = 0, gyroscope_y = 0, gyroscope_z = 0, magnetometer_x = 0, magnetometer_y = 0,
                    magnetometer_z = 0, rotation_x = 0, rotation_y = 0, rotation_z = 0, barometer_sen = 0, proximity_sen = 0, light_sen = 0,
                    humidity_sen = 0, temperature_sen = 0, linear_acc_x = 0, linear_acc_y = 0, linear_acc_z = 0;

                try
                {
                    intcallduration = Convert.ToInt32(rowDataArray[29]);
                    intpenalize = Convert.ToInt32(rowDataArray[30]);
                    acc_x = Convert.ToDouble(rowDataArray[31]);
                    acc_y = Convert.ToDouble(rowDataArray[32]);
                    acc_z = Convert.ToDouble(rowDataArray[33]);
                    gravity_x = Convert.ToDouble(rowDataArray[34]);
                    gravity_y = Convert.ToDouble(rowDataArray[35]);
                    gravity_z = Convert.ToDouble(rowDataArray[36]);
                    gyroscope_x = Convert.ToDouble(rowDataArray[37]);
                    gyroscope_y = Convert.ToDouble(rowDataArray[38]);
                    gyroscope_z = Convert.ToDouble(rowDataArray[39]);
                    magnetometer_x = Convert.ToDouble(rowDataArray[40]);
                    magnetometer_y = Convert.ToDouble(rowDataArray[41]);
                    magnetometer_z = Convert.ToDouble(rowDataArray[42]);
                    rotation_x = Convert.ToDouble(rowDataArray[43]);
                    rotation_y = Convert.ToDouble(rowDataArray[44]);
                    rotation_z = Convert.ToDouble(rowDataArray[45]);
                    barometer_sen = Convert.ToDouble(rowDataArray[46]);
                    proximity_sen = Convert.ToDouble(rowDataArray[47]);
                    light_sen = Convert.ToDouble(rowDataArray[48]);
                    humidity_sen = Convert.ToDouble(rowDataArray[49]);
                    temperature_sen = Convert.ToDouble(rowDataArray[50]);
                    linear_acc_x = Convert.ToDouble(rowDataArray[51]);
                    linear_acc_y = Convert.ToDouble(rowDataArray[52]);
                    linear_acc_z = Convert.ToDouble(rowDataArray[53]);

                }
                catch (Exception ex)
                {

                }

                SaveLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, History, ign, 0, 0, 0, intBreak, intOvrspd, 0, mCall, 1, intacceleration, intweather, intgeo_fence, gc_id, within_gc_id, 0, 0, RecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, intcallduration, intpenalize,
                    acc_x, acc_y, acc_z, gravity_x, gravity_y, gravity_z, gyroscope_x, gyroscope_y, gyroscope_z, magnetometer_x, magnetometer_y, magnetometer_z,
                    rotation_x, rotation_y, rotation_z, barometer_sen, proximity_sen, light_sen, humidity_sen, temperature_sen, linear_acc_x, linear_acc_y, linear_acc_z);
                #endregion

                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(0, "IOTTextData-ParseError", ex.ToString(), rowDataPacket);
                return Task.FromResult(false);
            }
        }

        public Task<bool> Drivesung_IOTTextDatas2Sec(string rowDataPacket)
        {
            try
            {
                string[] rowDataArray = rowDataPacket.Split(new string[] { "," }, StringSplitOptions.None); //seperate recived packtes by comma

                #region variable declaration and packet value parsing
                string mobileno = "";
                long sequencenumbr = Convert.ToInt64(rowDataArray[1]);
                int GPSValid = Convert.ToInt16(rowDataArray[3]);
                int History = Convert.ToInt16(rowDataArray[4]);

                string state = Convert.ToString(rowDataArray[5]);

                string RecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowDataArray[6]));
                DateTime dtDateTime = Convert.ToDateTime(RecordTimeStamp);

                decimal heading = Convert.ToDecimal(rowDataArray[7]);
                decimal latitude = Convert.ToDecimal(rowDataArray[8]);
                decimal longitude = Convert.ToDecimal(rowDataArray[9]);
                int ign = Convert.ToInt16(rowDataArray[10]);
                int is_self_driving = Convert.ToInt16(rowDataArray[11]);
                decimal speed = Convert.ToDecimal(rowDataArray[12]);
                decimal dist = Convert.ToDecimal(rowDataArray[13]);
                long deviceId = Convert.ToInt64(rowDataArray[14]);
                long user_id = Convert.ToInt64(rowDataArray[15]);  //new
                string hardware_id = Convert.ToString(rowDataArray[16]);
                string provTime = Convert.ToString(rowDataArray[17]);
                string prov = Convert.ToString(rowDataArray[18]);
                string ptype = Convert.ToString(rowDataArray[19]);
                decimal accuracy = 0;
                if (rowDataArray[20].ToString() != "null") accuracy = Convert.ToDecimal(rowDataArray[20]);  //new
                string timeZone = Convert.ToString(rowDataArray[21]); //new
                int intBreak = Convert.ToInt16(rowDataArray[22]);
                int intacceleration = Convert.ToInt16(rowDataArray[23]);
                int intOvrspd = Convert.ToInt16(rowDataArray[24]);
                int mCall = Convert.ToInt16(rowDataArray[25]);
                int intweather = Convert.ToInt16(rowDataArray[26]);

                int intCrnrVal = Convert.ToInt16(rowDataArray[27]);
                int intCrnr = intCrnrVal;
                if (intCrnr != 1) intCrnr = 0;

                #region for check vehicle in geofenec
                int intgeo_fence = 0;
                ////intgeo_fence => 0=out of geofence,1=with in geofence,2=geofence not assign
                int gc_id = 0;
                string within_gc_id = "";
                //intgeo_fence = IOTValidateGeofence.point_inside_polygon(deviceId, Convert.ToDouble(latitude), Convert.ToDouble(longitude), out gc_id, out within_gc_id);
                #endregion

                #region Crnr packet paring
                if (intCrnrVal != 0)
                {
                    string rowCrnrPacket = Convert.ToString(rowDataArray[28]);
                    string[] rowCrnrPacketArray = rowCrnrPacket.Split(new string[] { ":" }, StringSplitOptions.None); //seperate recived packtes by :
                    long Crnrseqno = Convert.ToInt64(rowCrnrPacketArray[0]);
                    string crnRecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowCrnrPacketArray[1]));
                    DateTime crnr_dtDateTime = Convert.ToDateTime(crnRecordTimeStamp);
                    int crnr_GPSValid = Convert.ToInt16(rowCrnrPacketArray[2]);
                    decimal crnr_heading = Convert.ToDecimal(rowCrnrPacketArray[3]);
                    decimal crnr_latitude = Convert.ToDecimal(rowCrnrPacketArray[4]);
                    decimal crnr_longitude = Convert.ToDecimal(rowCrnrPacketArray[5]);
                    decimal crnr_speed = Convert.ToDecimal(rowCrnrPacketArray[6]);
                    string crnr_provTime = Convert.ToString(rowCrnrPacketArray[7]);
                    string crnr_prov = Convert.ToString(rowCrnrPacketArray[8]);
                    string crnr_ptype = Convert.ToString(rowCrnrPacketArray[9]);
                    string crnr_accuracy = Convert.ToString(rowCrnrPacketArray[10]);
                    SaveLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, crnr_latitude, crnr_longitude, crnr_speed, Convert.ToInt32(crnr_heading), crnr_dtDateTime, Crnrseqno, 0, crnr_GPSValid, History, ign, 0, 0, 0, 0, 0, intCrnr, 0, 1, 0, 0, intgeo_fence, gc_id, within_gc_id, 0, 0, crnRecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
                #endregion

                Int32 intcallduration = 0, intpenalize = 0;
                double acc_x = 0, acc_y = 0, acc_z = 0, gravity_x = 0, gravity_y = 0,
                    gravity_z = 0, gyroscope_x = 0, gyroscope_y = 0, gyroscope_z = 0, magnetometer_x = 0, magnetometer_y = 0,
                    magnetometer_z = 0, rotation_x = 0, rotation_y = 0, rotation_z = 0, barometer_sen = 0, proximity_sen = 0, light_sen = 0,
                    humidity_sen = 0, temperature_sen = 0, linear_acc_x = 0, linear_acc_y = 0, linear_acc_z = 0,
                   device_orientation_alpha = 0, device_orientation_beta = 0, device_orientation_gamma = 0;
                string activity_recognition = "";

                try
                {
                    intcallduration = Convert.ToInt32(rowDataArray[29]);
                    intpenalize = Convert.ToInt32(rowDataArray[30]);
                    acc_x = Convert.ToDouble(rowDataArray[31]);
                    acc_y = Convert.ToDouble(rowDataArray[32]);
                    acc_z = Convert.ToDouble(rowDataArray[33]);
                    gravity_x = Convert.ToDouble(rowDataArray[34]);
                    gravity_y = Convert.ToDouble(rowDataArray[35]);
                    gravity_z = Convert.ToDouble(rowDataArray[36]);
                    gyroscope_x = Convert.ToDouble(rowDataArray[37]);
                    gyroscope_y = Convert.ToDouble(rowDataArray[38]);
                    gyroscope_z = Convert.ToDouble(rowDataArray[39]);
                    magnetometer_x = Convert.ToDouble(rowDataArray[40]);
                    magnetometer_y = Convert.ToDouble(rowDataArray[41]);
                    magnetometer_z = Convert.ToDouble(rowDataArray[42]);
                    rotation_x = Convert.ToDouble(rowDataArray[43]);
                    rotation_y = Convert.ToDouble(rowDataArray[44]);
                    rotation_z = Convert.ToDouble(rowDataArray[45]);
                    barometer_sen = Convert.ToDouble(rowDataArray[46]);
                    proximity_sen = Convert.ToDouble(rowDataArray[47]);
                    light_sen = Convert.ToDouble(rowDataArray[48]);
                    humidity_sen = Convert.ToDouble(rowDataArray[49]);
                    temperature_sen = Convert.ToDouble(rowDataArray[50]);
                    linear_acc_x = Convert.ToDouble(rowDataArray[51]);
                    linear_acc_y = Convert.ToDouble(rowDataArray[52]);
                    linear_acc_z = Convert.ToDouble(rowDataArray[53]);
                    device_orientation_alpha = Convert.ToDouble(rowDataArray[54]);
                    device_orientation_beta = Convert.ToDouble(rowDataArray[55]);
                    device_orientation_gamma = Convert.ToDouble(rowDataArray[56]);
                    activity_recognition = Convert.ToString(rowDataArray[57]);


                }
                catch (Exception ex)
                {

                }

                Save2SecLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, History, ign, 0, 0, 0, intBreak, intOvrspd, 0, mCall, 1, intacceleration, intweather, intgeo_fence, gc_id, within_gc_id, 0, 0, RecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, intcallduration, intpenalize,
                    acc_x, acc_y, acc_z, gravity_x, gravity_y, gravity_z, gyroscope_x, gyroscope_y, gyroscope_z, magnetometer_x, magnetometer_y, magnetometer_z,
                    rotation_x, rotation_y, rotation_z, barometer_sen, proximity_sen, light_sen, humidity_sen, temperature_sen, linear_acc_x, linear_acc_y, linear_acc_z,
                    device_orientation_alpha,device_orientation_beta,device_orientation_gamma,activity_recognition);
                #endregion

                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(0, "IOTTextData2Sec-ParseError", ex.ToString(), rowDataPacket);
                return Task.FromResult(false);
            }
        }
        //public Task<bool> Drivesung_IOTTextDatas2Sec(string rowDataPacket)
        //{
        //    try
        //    {
        //        decimal accuracy2sec = 0;
        //        try
        //        {
        //            accuracy2sec = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["accuracy2sec"].ToString());
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        if (accuracy2sec == 0) accuracy2sec = 65;

        //        string[] rowDataArray = rowDataPacket.Split(new string[] { "," }, StringSplitOptions.None); //seperate recived packtes by comma

        //        #region variable declaration and packet value parsing
        //        string mobileno = "";
        //        long sequencenumbr = Convert.ToInt64(rowDataArray[1]);
        //        int GPSValid = Convert.ToInt16(rowDataArray[3]);
        //        int History = Convert.ToInt16(rowDataArray[4]);

        //        string state = Convert.ToString(rowDataArray[5]);

        //        string RecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowDataArray[6]));
        //        DateTime dtDateTime = Convert.ToDateTime(RecordTimeStamp);

        //        decimal heading = Convert.ToDecimal(rowDataArray[7]);
        //        decimal latitude = Convert.ToDecimal(rowDataArray[8]);
        //        decimal longitude = Convert.ToDecimal(rowDataArray[9]);
        //        int ign = Convert.ToInt16(rowDataArray[10]);
        //        int is_self_driving = Convert.ToInt16(rowDataArray[11]);
        //        decimal speed = Convert.ToDecimal(rowDataArray[12]);
        //        if (speed < 0) speed = 0;
        //        decimal dist = Convert.ToDecimal(rowDataArray[13]);
        //        long deviceId = Convert.ToInt64(rowDataArray[14]);
        //        long user_id = Convert.ToInt64(rowDataArray[15]);  //new
        //        string hardware_id = Convert.ToString(rowDataArray[16]);
        //        string provTime = Convert.ToString(rowDataArray[17]);
        //        string prov = Convert.ToString(rowDataArray[18]);
        //        string ptype = Convert.ToString(rowDataArray[19]);
        //        decimal accuracy = 0;
        //        if (rowDataArray[20].ToString() != "null") accuracy = Convert.ToDecimal(rowDataArray[20]);  //new
        //        string timeZone = Convert.ToString(rowDataArray[21]); //new
        //        int intBreak = Convert.ToInt16(rowDataArray[22]);
        //        int intacceleration = Convert.ToInt16(rowDataArray[23]);
        //        int intOvrspd = Convert.ToInt16(rowDataArray[24]);
        //        int mCall = Convert.ToInt16(rowDataArray[25]);
        //        int intweather = Convert.ToInt16(rowDataArray[26]);

        //        int intCrnrVal = Convert.ToInt16(rowDataArray[27]);
        //        int intCrnr = intCrnrVal;
        //        if (intCrnr != 1) intCrnr = 0;

        //        #region for check vehicle in geofenec
        //        int intgeo_fence = 0;
        //        ////intgeo_fence => 0=out of geofence,1=with in geofence,2=geofence not assign
        //        int gc_id = 0;
        //        string within_gc_id = "";
        //        //intgeo_fence = IOTValidateGeofence.point_inside_polygon(deviceId, Convert.ToDouble(latitude), Convert.ToDouble(longitude), out gc_id, out within_gc_id);
        //        #endregion

        //        #region Crnr packet paring
        //        if (intCrnrVal != 0)
        //        {
        //            string rowCrnrPacket = Convert.ToString(rowDataArray[28]);
        //            string[] rowCrnrPacketArray = rowCrnrPacket.Split(new string[] { ":" }, StringSplitOptions.None); //seperate recived packtes by :
        //            long Crnrseqno = Convert.ToInt64(rowCrnrPacketArray[0]);
        //            string crnRecordTimeStamp = ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowCrnrPacketArray[1]));
        //            DateTime crnr_dtDateTime = Convert.ToDateTime(crnRecordTimeStamp);
        //            int crnr_GPSValid = Convert.ToInt16(rowCrnrPacketArray[2]);
        //            decimal crnr_heading = Convert.ToDecimal(rowCrnrPacketArray[3]);
        //            decimal crnr_latitude = Convert.ToDecimal(rowCrnrPacketArray[4]);
        //            decimal crnr_longitude = Convert.ToDecimal(rowCrnrPacketArray[5]);
        //            decimal crnr_speed = Convert.ToDecimal(rowCrnrPacketArray[6]);
        //            if (crnr_speed < 0) crnr_speed = 0;
        //            string crnr_provTime = Convert.ToString(rowCrnrPacketArray[7]);
        //            string crnr_prov = Convert.ToString(rowCrnrPacketArray[8]);
        //            string crnr_ptype = Convert.ToString(rowCrnrPacketArray[9]);
        //            string crnr_accuracy = Convert.ToString(rowCrnrPacketArray[10]);
        //            if (accuracy <= accuracy2sec)
        //            {
        //                SaveLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, crnr_latitude, crnr_longitude, crnr_speed, Convert.ToInt32(crnr_heading), crnr_dtDateTime, Crnrseqno, 0, crnr_GPSValid, History, ign, 0, 0, 0, 0, 0, intCrnr, 0, 1, 0, 0, intgeo_fence, gc_id, within_gc_id, 0, 0, crnRecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        //            }
        //        }
        //        #endregion

        //        Int32 intcallduration = 0, intpenalize = 0;
        //        double acc_x = 0, acc_y = 0, acc_z = 0, gravity_x = 0, gravity_y = 0,
        //          gravity_z = 0, gyroscope_x = 0, gyroscope_y = 0, gyroscope_z = 0, magnetometer_x = 0, magnetometer_y = 0,
        //          magnetometer_z = 0, rotation_x = 0, rotation_y = 0, rotation_z = 0, barometer_sen = 0, proximity_sen = 0, light_sen = 0,
        //          humidity_sen = 0, temperature_sen = 0, linear_acc_x = 0, linear_acc_y = 0, linear_acc_z = 0;
        //        try
        //        {
        //            intcallduration = Convert.ToInt32(rowDataArray[29]);
        //            intpenalize = Convert.ToInt32(rowDataArray[30]);
        //            acc_x = Convert.ToDouble(rowDataArray[31]);
        //            acc_y = Convert.ToDouble(rowDataArray[32]);
        //            acc_z = Convert.ToDouble(rowDataArray[33]);
        //            gravity_x = Convert.ToDouble(rowDataArray[34]);
        //            gravity_y = Convert.ToDouble(rowDataArray[35]);
        //            gravity_z = Convert.ToDouble(rowDataArray[36]);
        //            gyroscope_x = Convert.ToDouble(rowDataArray[37]);
        //            gyroscope_y = Convert.ToDouble(rowDataArray[38]);
        //            gyroscope_z = Convert.ToDouble(rowDataArray[39]);
        //            magnetometer_x = Convert.ToDouble(rowDataArray[40]);
        //            magnetometer_y = Convert.ToDouble(rowDataArray[41]);
        //            magnetometer_z = Convert.ToDouble(rowDataArray[42]);
        //            rotation_x = Convert.ToDouble(rowDataArray[43]);
        //            rotation_y = Convert.ToDouble(rowDataArray[44]);
        //            rotation_z = Convert.ToDouble(rowDataArray[45]);
        //            barometer_sen = Convert.ToDouble(rowDataArray[46]);
        //            proximity_sen = Convert.ToDouble(rowDataArray[47]);
        //            light_sen = Convert.ToDouble(rowDataArray[48]);
        //            humidity_sen = Convert.ToDouble(rowDataArray[49]);
        //            temperature_sen = Convert.ToDouble(rowDataArray[50]);
        //            linear_acc_x = Convert.ToDouble(rowDataArray[51]);
        //            linear_acc_y = Convert.ToDouble(rowDataArray[52]);
        //            linear_acc_z = Convert.ToDouble(rowDataArray[53]);

        //        }
        //        catch (Exception ex)
        //        {

        //        }


        //        if (accuracy <= accuracy2sec)
        //        {
        //            SaveLocationDataToDatabase(rowDataPacket, deviceId, mobileno, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, History, ign, 0, 0, 0, intBreak, intOvrspd, 0, mCall, 1, intacceleration, intweather, intgeo_fence, gc_id, within_gc_id, 0, 0, RecordTimeStamp, is_self_driving, provTime, prov, ptype, hardware_id, user_id, accuracy, timeZone, intcallduration, intpenalize,
        //            acc_x, acc_y, acc_z, gravity_x, gravity_y, gravity_z, gyroscope_x, gyroscope_y, gyroscope_z, magnetometer_x, magnetometer_y, magnetometer_z,
        //            rotation_x, rotation_y, rotation_z, barometer_sen, proximity_sen, light_sen, humidity_sen, temperature_sen, linear_acc_x, linear_acc_y, linear_acc_z);
        //        }
        //        #endregion

        //        return Task.FromResult(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.SaveLogToDatabase(0, "IOTTextData2Sec-ParseError", ex.ToString(), rowDataPacket);
        //        return Task.FromResult(false);
        //    }
        //}

        public bool SaveLocationDataToDatabase(string strjson, long DeviceNo, string mobileno, string state, decimal Latitude, decimal Longitude, decimal Speed, int Heading, DateTime RecordDateTime, long sequencenumbr, decimal GPSOdo, int GPSValid, int history, int ign, int? AccelerometerX, int? AccelerometerY, int? AccelerometerZ, int AlarmBreak, int AlarmOvrspd, int Crnrpkt, int mCall, int PktType, int intacceleration, int AlarmWeather, int geo_fence, Int32 gc_id, string within_gc_id, int gps_error, int obd_error, string RecordTimeStamp, int is_self_driving, string provTime, string prov, string ptype, string hardware_id, long? user_id, decimal? accuracy, string timezone, Int32 intcallduration, Int32 intpenalize,
            double acc_x, double acc_y, double acc_z, double gravity_x, double gravity_y, double gravity_z, double gyroscope_x, double gyroscope_y, double gyroscope_z, double magnetometer_x, double magnetometer_y, double magnetometer_z, double rotation_x, double rotation_y, double rotation_z, double barometer_sen, double proximity_sen, double light_sen, double humidity_sen, double temperature_sen, double linear_acc_x, double linear_acc_y, double linear_acc_z)
        {

            long cntCount;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    #region check  data avaliable in iot_hub
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_hub WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_hub

                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_hub(deviceno, mobileno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                        " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                        " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate,mcall,packettype,sysdatetime,weather,geo_fence_alert,gc_id,within_gc_id,obd_error,gps_error,is_self_driving,provTime,prov,ptype,hardware_id,user_id,accuracy,timezone,call_duration,call_penalize," +
                        " acc_x,acc_y,acc_z,gravity_x,gravity_y,gravity_z,gyroscope_x,gyroscope_y,gyroscope_z,magnetometer_x,magnetometer_y,magnetometer_z,rotation_x,rotation_y,rotation_z,barometer_sen,proximity_sen,light_sen,humidity_sen,temperature_sen,linear_acc_x,linear_acc_y,linear_acc_z)" +
                        " VALUES(@deviceno, @mobileno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                        " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                        " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate,@mcall,@packettype,@sysdatetime,@weather,@geo_fence_alert,@gc_id,@within_gc_id,@obd_error,@gps_error,@is_self_driving,@provTime,@prov,@ptype,@hardware_id,@user_id,@accuracy,@timezone,@call_duration,@call_penalize," +
                        " @acc_x,@acc_y,@acc_z,@gravity_x,@gravity_y,@gravity_z,@gyroscope_x,@gyroscope_y,@gyroscope_z,@magnetometer_x,@magnetometer_y,@magnetometer_z,@rotation_x,@rotation_y,@rotation_z,@barometer_sen,@proximity_sen,@light_sen,@humidity_sen,@temperature_sen,@linear_acc_x,@linear_acc_y,@linear_acc_z)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_hub
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_hub SET mobileno=@mobileno, latitude = @latitude, latitudedir = @latitudedir, longitude = @longitude, longitudedir = @longitudedir, " +
                                        " speed = @speed, distance = @distance, directionindegree = @directionindegree, overspeedstarted = @overspeedstarted, mainpowerstatus = @mainpowerstatus, " +
                                        " ignitionstatus = @ignitionstatus, cornerpacket = @cornerpacket, hardacceleration = @hardacceleration, hardbraking = @hardbraking,  " +
                                        " accelerometerx = @accelerometerx, accelerometery = @accelerometery, accelerometerz = @accelerometerz, isactive = @isactive, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate,mcall=@mcall,packettype=@packettype,weather=@weather,geo_fence_alert=@geo_fence_alert, " +
                                        " gc_id=@gc_id,within_gc_id=@within_gc_id,gps_error=@gps_error,obd_error=@obd_error,is_self_driving=@is_self_driving,provTime=@provTime,prov=@prov,ptype=@ptype,hardware_id=@hardware_id,user_id=@user_id,accuracy=@accuracy,timezone=@timezone," +
                                        " call_duration=@call_duration,call_penalize=@call_penalize,acc_x = @acc_x ,acc_y =@acc_y ,acc_z =@acc_z ,gravity_x =@gravity_x ,gravity_y =@gravity_y ,gravity_z =@gravity_z ,gyroscope_x =@gyroscope_x , " +
                                        " gyroscope_y =@gyroscope_y  ,gyroscope_z =@gyroscope_z ,magnetometer_x =@magnetometer_x  ,magnetometer_y =@magnetometer_y  ,magnetometer_z =@magnetometer_z,rotation_x =@rotation_x,rotation_y =@rotation_y,rotation_z =@rotation_z, " +
                                        " barometer_sen =@barometer_sen  ,proximity_sen =@proximity_sen,light_sen =@light_sen  ,humidity_sen =@humidity_sen  ,temperature_sen =@temperature_sen,linear_acc_x=@linear_acc_x,linear_acc_y=@linear_acc_y,linear_acc_z=@linear_acc_z WHERE deviceno = @deviceno and recorddatetime=@recorddatetime";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        //cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));

                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));


                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    #region check  data avaliable in iot_live
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_live(deviceno, mobileno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                       " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                       " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate,mcall,packettype,sysdatetime,weather,geo_fence_alert,gc_id,within_gc_id,gps_error,obd_error,is_self_driving,provTime,prov,ptype,hardware_id,user_id,accuracy,timezone,call_duration,call_penalize," +
                       " acc_x,acc_y,acc_z,gravity_x,gravity_y,gravity_z,gyroscope_x,gyroscope_y,gyroscope_z,magnetometer_x,magnetometer_y,magnetometer_z,rotation_x,rotation_y,rotation_z,barometer_sen,proximity_sen,light_sen,humidity_sen,temperature_sen,linear_acc_x,linear_acc_y,linear_acc_z)" +
                       " VALUES(@deviceno, @mobileno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                       " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                       " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate,@mcall,@packettype,@sysdatetime,@weather,@geo_fence_alert,@gc_id,@within_gc_id,@gps_error,@obd_error,@is_self_driving,@provTime,@prov,@ptype,@hardware_id,@user_id,@accuracy,@timezone,@call_duration,@call_penalize," +
                       " @acc_x,@acc_y,@acc_z,@gravity_x,@gravity_y,@gravity_z,@gyroscope_x,@gyroscope_y,@gyroscope_z,@magnetometer_x,@magnetometer_y,@magnetometer_z,@rotation_x,@rotation_y,@rotation_z,@barometer_sen,@proximity_sen,@light_sen,@humidity_sen,@temperature_sen,@linear_acc_x,@linear_acc_y,@linear_acc_z)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));


                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_live SET mobileno=@mobileno, recorddatetime = @recorddatetime, latitude = @latitude, latitudedir = @latitudedir, longitude = @longitude, longitudedir = @longitudedir, " +
                                        " speed = @speed, distance = @distance, directionindegree = @directionindegree, overspeedstarted = @overspeedstarted, mainpowerstatus = @mainpowerstatus, " +
                                        " ignitionstatus = @ignitionstatus, cornerpacket = @cornerpacket, hardacceleration = @hardacceleration, hardbraking = @hardbraking,  " +
                                        " accelerometerx = @accelerometerx, accelerometery = @accelerometery, accelerometerz = @accelerometerz, isactive = @isactive, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate,mcall=@mcall,packettype=@packettype,weather=@weather,geo_fence_alert=@geo_fence_alert,gc_id=@gc_id,within_gc_id=@within_gc_id,gps_error=@gps_error,obd_error=@obd_error,is_self_driving=@is_self_driving,provTime=@provTime,prov=@prov,ptype=@ptype,hardware_id=@hardware_id,user_id=@user_id,accuracy=@accuracy,timezone=@timezone, " +
                                        " call_duration=@call_duration,call_penalize=@call_penalize,acc_x = @acc_x ,acc_y =@acc_y ,acc_z =@acc_z ,gravity_x =@gravity_x ,gravity_y =@gravity_y ,gravity_z =@gravity_z ,gyroscope_x =@gyroscope_x , " +
                                        " gyroscope_y =@gyroscope_y  ,gyroscope_z =@gyroscope_z ,magnetometer_x =@magnetometer_x  ,magnetometer_y =@magnetometer_y  ,magnetometer_z =@magnetometer_z,rotation_x =@rotation_x,rotation_y =@rotation_y,rotation_z =@rotation_z, " +
                                        " barometer_sen =@barometer_sen  ,proximity_sen =@proximity_sen,light_sen =@light_sen,humidity_sen =@humidity_sen ,temperature_sen =@temperature_sen,linear_acc_x=@linear_acc_x,linear_acc_y=@linear_acc_y,linear_acc_z=@linear_acc_z WHERE deviceno = @deviceno ";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        //cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));
                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));


                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    tran.Commit();
                    connection.Close();

                }

                return true;

            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(DeviceNo, "M-dataobj-DBSaveError", ex.ToString(), strjson);
                return false;
            }

        }

        public bool Save2SecLocationDataToDatabase(string strjson, long DeviceNo, string mobileno, string state, decimal Latitude, decimal Longitude, decimal Speed, int Heading, DateTime RecordDateTime, long sequencenumbr, decimal GPSOdo, int GPSValid, int history, int ign, int? AccelerometerX, int? AccelerometerY, int? AccelerometerZ, int AlarmBreak, int AlarmOvrspd, int Crnrpkt, int mCall, int PktType, int intacceleration, int AlarmWeather, int geo_fence, Int32 gc_id, string within_gc_id, int gps_error, int obd_error, string RecordTimeStamp, int is_self_driving, string provTime, string prov, string ptype, string hardware_id, long? user_id, decimal? accuracy, string timezone, Int32 intcallduration, Int32 intpenalize,
         double acc_x, double acc_y, double acc_z, double gravity_x, double gravity_y, double gravity_z, double gyroscope_x, double gyroscope_y, double gyroscope_z, double magnetometer_x, double magnetometer_y, double magnetometer_z, double rotation_x, double rotation_y, double rotation_z, double barometer_sen, double proximity_sen, double light_sen, double humidity_sen, double temperature_sen, double linear_acc_x, double linear_acc_y, double linear_acc_z,
         double device_orientation_alpha,double device_orientation_beta,double device_orientation_gamma,string activity_recognition)
        {

            long cntCount;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    #region check  data avaliable in iot_hub2Sec
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_hub2Sec WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_hub2Sec

                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_hub2Sec(deviceno, mobileno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                        " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                        " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate,mcall,packettype,sysdatetime,weather,geo_fence_alert,gc_id,within_gc_id,obd_error,gps_error,is_self_driving,provTime,prov,ptype,hardware_id,user_id,accuracy,timezone,call_duration,call_penalize," +
                        " acc_x,acc_y,acc_z,gravity_x,gravity_y,gravity_z,gyroscope_x,gyroscope_y,gyroscope_z,magnetometer_x,magnetometer_y,magnetometer_z,rotation_x,rotation_y,rotation_z,barometer_sen,proximity_sen,light_sen,humidity_sen,temperature_sen,linear_acc_x,linear_acc_y,linear_acc_z,device_orientation_alpha,device_orientation_beta,device_orientation_gamma,activity_recognition)" +
                        " VALUES(@deviceno, @mobileno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                        " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                        " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate,@mcall,@packettype,@sysdatetime,@weather,@geo_fence_alert,@gc_id,@within_gc_id,@obd_error,@gps_error,@is_self_driving,@provTime,@prov,@ptype,@hardware_id,@user_id,@accuracy,@timezone,@call_duration,@call_penalize," +
                        " @acc_x,@acc_y,@acc_z,@gravity_x,@gravity_y,@gravity_z,@gyroscope_x,@gyroscope_y,@gyroscope_z,@magnetometer_x,@magnetometer_y,@magnetometer_z,@rotation_x,@rotation_y,@rotation_z,@barometer_sen,@proximity_sen,@light_sen,@humidity_sen,@temperature_sen,@linear_acc_x,@linear_acc_y,@linear_acc_z,@device_orientation_alpha,@device_orientation_beta,@device_orientation_gamma,@activity_recognition)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_alpha", device_orientation_alpha));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_beta", device_orientation_beta));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_gamma", device_orientation_gamma));
                        cmd.Parameters.Add(new NpgsqlParameter("@activity_recognition", activity_recognition));
                                         
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_hub2Sec
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_hub2Sec SET mobileno=@mobileno, latitude = @latitude, latitudedir = @latitudedir, longitude = @longitude, longitudedir = @longitudedir, " +
                                        " speed = @speed, distance = @distance, directionindegree = @directionindegree, overspeedstarted = @overspeedstarted, mainpowerstatus = @mainpowerstatus, " +
                                        " ignitionstatus = @ignitionstatus, cornerpacket = @cornerpacket, hardacceleration = @hardacceleration, hardbraking = @hardbraking,  " +
                                        " accelerometerx = @accelerometerx, accelerometery = @accelerometery, accelerometerz = @accelerometerz, isactive = @isactive, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate,mcall=@mcall,packettype=@packettype,weather=@weather,geo_fence_alert=@geo_fence_alert, " +
                                        " gc_id=@gc_id,within_gc_id=@within_gc_id,gps_error=@gps_error,obd_error=@obd_error,is_self_driving=@is_self_driving,provTime=@provTime,prov=@prov,ptype=@ptype,hardware_id=@hardware_id,user_id=@user_id,accuracy=@accuracy,timezone=@timezone," +
                                        " call_duration=@call_duration,call_penalize=@call_penalize,acc_x = @acc_x ,acc_y =@acc_y ,acc_z =@acc_z ,gravity_x =@gravity_x ,gravity_y =@gravity_y ,gravity_z =@gravity_z ,gyroscope_x =@gyroscope_x , " +
                                        " gyroscope_y =@gyroscope_y  ,gyroscope_z =@gyroscope_z ,magnetometer_x =@magnetometer_x  ,magnetometer_y =@magnetometer_y  ,magnetometer_z =@magnetometer_z,rotation_x =@rotation_x,rotation_y =@rotation_y,rotation_z =@rotation_z, " +
                                        " barometer_sen =@barometer_sen  ,proximity_sen =@proximity_sen,light_sen =@light_sen  ,humidity_sen =@humidity_sen  ,temperature_sen =@temperature_sen,linear_acc_x=@linear_acc_x,linear_acc_y=@linear_acc_y,linear_acc_z=@linear_acc_z, " +
                                        " device_orientation_alpha=@device_orientation_alpha,device_orientation_beta=@device_orientation_beta,device_orientation_gamma=@device_orientation_gamma,activity_recognition=@activity_recognition WHERE deviceno = @deviceno and recorddatetime=@recorddatetime";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        //cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));

                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_alpha", device_orientation_alpha));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_beta", device_orientation_beta));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_gamma", device_orientation_gamma));
                        cmd.Parameters.Add(new NpgsqlParameter("@activity_recognition", activity_recognition));
                        

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    #region check  data avaliable in iot_live
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_live(deviceno, mobileno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                       " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                       " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate,mcall,packettype,sysdatetime,weather,geo_fence_alert,gc_id,within_gc_id,gps_error,obd_error,is_self_driving,provTime,prov,ptype,hardware_id,user_id,accuracy,timezone,call_duration,call_penalize," +
                       " acc_x,acc_y,acc_z,gravity_x,gravity_y,gravity_z,gyroscope_x,gyroscope_y,gyroscope_z,magnetometer_x,magnetometer_y,magnetometer_z,rotation_x,rotation_y,rotation_z,barometer_sen,proximity_sen,light_sen,humidity_sen,temperature_sen,linear_acc_x,linear_acc_y,linear_acc_z,device_orientation_alpha,device_orientation_beta,device_orientation_gamma,activity_recognition)" +
                       " VALUES(@deviceno, @mobileno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                       " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                       " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate,@mcall,@packettype,@sysdatetime,@weather,@geo_fence_alert,@gc_id,@within_gc_id,@gps_error,@obd_error,@is_self_driving,@provTime,@prov,@ptype,@hardware_id,@user_id,@accuracy,@timezone,@call_duration,@call_penalize," +
                       " @acc_x,@acc_y,@acc_z,@gravity_x,@gravity_y,@gravity_z,@gyroscope_x,@gyroscope_y,@gyroscope_z,@magnetometer_x,@magnetometer_y,@magnetometer_z,@rotation_x,@rotation_y,@rotation_z,@barometer_sen,@proximity_sen,@light_sen,@humidity_sen,@temperature_sen,@linear_acc_x,@linear_acc_y,@linear_acc_z,@device_orientation_alpha,@device_orientation_beta,@device_orientation_gamma,@activity_recognition)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));

                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_alpha", device_orientation_alpha));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_beta", device_orientation_beta));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_gamma", device_orientation_gamma));
                        cmd.Parameters.Add(new NpgsqlParameter("@activity_recognition", activity_recognition));
                        


                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_live SET mobileno=@mobileno, recorddatetime = @recorddatetime, latitude = @latitude, latitudedir = @latitudedir, longitude = @longitude, longitudedir = @longitudedir, " +
                                        " speed = @speed, distance = @distance, directionindegree = @directionindegree, overspeedstarted = @overspeedstarted, mainpowerstatus = @mainpowerstatus, " +
                                        " ignitionstatus = @ignitionstatus, cornerpacket = @cornerpacket, hardacceleration = @hardacceleration, hardbraking = @hardbraking,  " +
                                        " accelerometerx = @accelerometerx, accelerometery = @accelerometery, accelerometerz = @accelerometerz, isactive = @isactive, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate,mcall=@mcall,packettype=@packettype,weather=@weather,geo_fence_alert=@geo_fence_alert,gc_id=@gc_id,within_gc_id=@within_gc_id,gps_error=@gps_error,obd_error=@obd_error,is_self_driving=@is_self_driving,provTime=@provTime,prov=@prov,ptype=@ptype,hardware_id=@hardware_id,user_id=@user_id,accuracy=@accuracy,timezone=@timezone, " +
                                        " call_duration=@call_duration,call_penalize=@call_penalize,acc_x = @acc_x ,acc_y =@acc_y ,acc_z =@acc_z ,gravity_x =@gravity_x ,gravity_y =@gravity_y ,gravity_z =@gravity_z ,gyroscope_x =@gyroscope_x , " +
                                        " gyroscope_y =@gyroscope_y  ,gyroscope_z =@gyroscope_z ,magnetometer_x =@magnetometer_x  ,magnetometer_y =@magnetometer_y  ,magnetometer_z =@magnetometer_z,rotation_x =@rotation_x,rotation_y =@rotation_y,rotation_z =@rotation_z, " +
                                        " barometer_sen =@barometer_sen  ,proximity_sen =@proximity_sen,light_sen =@light_sen,humidity_sen =@humidity_sen ,temperature_sen =@temperature_sen,linear_acc_x=@linear_acc_x,linear_acc_y=@linear_acc_y,linear_acc_z=@linear_acc_z,"+
                                        " device_orientation_alpha=@device_orientation_alpha,device_orientation_beta=@device_orientation_beta,device_orientation_gamma=@device_orientation_gamma,activity_recognition=@activity_recognition WHERE deviceno = @deviceno ";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", Crnrpkt));
                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", AlarmOvrspd));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", ign));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", intacceleration));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", AlarmBreak));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", GPSOdo));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@mcall", mCall));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", PktType));
                        //cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", Convert.ToDateTime(DateTime.Now)));
                        cmd.Parameters.Add(new NpgsqlParameter("@weather", AlarmWeather));
                        cmd.Parameters.Add(new NpgsqlParameter("@geo_fence_alert", geo_fence));
                        cmd.Parameters.Add(new NpgsqlParameter("@gc_id", gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@within_gc_id", within_gc_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@obd_error", obd_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@gps_error", gps_error));
                        cmd.Parameters.Add(new NpgsqlParameter("@is_self_driving", is_self_driving));
                        cmd.Parameters.Add(new NpgsqlParameter("@provTime", provTime));
                        cmd.Parameters.Add(new NpgsqlParameter("@prov", prov));
                        cmd.Parameters.Add(new NpgsqlParameter("@ptype", ptype));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardware_id", hardware_id));
                        if (user_id == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@user_id", user_id));

                        if (accuracy == null)
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@accuracy", accuracy));

                        if (timezone == "")
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", DBNull.Value));
                        else
                            cmd.Parameters.Add(new NpgsqlParameter("@timezone", timezone));

                        cmd.Parameters.Add(new NpgsqlParameter("@call_duration", intcallduration));
                        cmd.Parameters.Add(new NpgsqlParameter("@call_penalize", intpenalize));

                        cmd.Parameters.Add(new NpgsqlParameter("@acc_x", acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_y", acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@acc_z", acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_x", gravity_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_y", gravity_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gravity_z", gravity_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_x", gyroscope_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_y", gyroscope_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@gyroscope_z", gyroscope_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_x", magnetometer_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_y", magnetometer_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@magnetometer_z", magnetometer_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_x", rotation_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_y", rotation_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@rotation_z", rotation_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@barometer_sen", barometer_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@proximity_sen", proximity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@light_sen", light_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@humidity_sen", humidity_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@temperature_sen", temperature_sen));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_x", linear_acc_x));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_y", linear_acc_y));
                        cmd.Parameters.Add(new NpgsqlParameter("@linear_acc_z", linear_acc_z));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_alpha", device_orientation_alpha));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_beta", device_orientation_beta));
                        cmd.Parameters.Add(new NpgsqlParameter("@device_orientation_gamma", device_orientation_gamma));
                        cmd.Parameters.Add(new NpgsqlParameter("@activity_recognition", activity_recognition));
                        

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    tran.Commit();
                    connection.Close();

                }

                return true;

            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(DeviceNo, "M-dataobj-DBSaveError", ex.ToString(), strjson);
                return false;
            }

        }

        public bool SaveOBDDataToDatabase(string strjson, long DeviceNo, decimal Latitude, decimal Longitude, string mobileno, int packettype, DateTime RecordDateTime, long SequenceNmbr, string DTCDatas, string[,] arryOBDData, string RecordTimeStamp)
        {

            bool functionReturnValue = false;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    cmd.Connection = connection;
                    cmd.Transaction = tran;

                    #region check  data avaliable in iot_obd_hub
                    long OBDCount;
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_obd_hub WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    OBDCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();

                    #endregion

                    if (OBDCount == 0)
                    {
                        #region insert data in iot_obd_hub
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_obd_hub(deviceno, recorddatetime, latitude, longitude, mobileno, packettype, sequencenumber, sysdatetime, isactive, dtcdetected, pidsupport1to20, statusdtccleared, freezedtc, fuelsystemstatus, calculatedengineload, coolanttemperature, shorttermfueltrim, longtermfueltrim, shorttermfueltrimbank2, longtermfueltrimbank2, fuelpressure, intakemanifoldabsolutepressure, rpm, speedcan, timingadvance, intakeairtemp, mafairflowrate, throttlepos, commandedsecondaryairstatus, oxygensensorspresent, oxygensensorvoltage1, oxygensensorvoltage2, oxygensensorvoltage3, oxygensensorvoltage4, oxygensensorvoltage5, oxygensensorvoltage6, oxygensensorvoltage7, oxygensensorvoltage8, obdstandards, oxygensensorspresent4banks, auxiliaryinputstatus, runtimesinceenginestart, pidsupported21to40, distancetraveledmil, relativefuelrailpressure, directfuelrailpressure, oxygensensorvoltagefuel1, oxygensensorvoltagefuel2, oxygensensorvoltagefuel3, oxygensensorvoltagefuel4, oxygensensorvoltagefuel5, oxygensensorvoltagefuel6, oxygensensorvoltagefuel7, oxygensensorvoltagefuel8, commandedegr, egrerror, commandedevaporativepurge, fuellevel, warmupssc, distsincecodecleared, systemvaporpressure, barometricpressure, oxygensensorcurrentfuel1, oxygensensorcurrentfuel2, oxygensensorcurrentfuel3, oxygensensorcurrentfuel4, oxygensensorcurrentfuel5, oxygensensorcurrentfuel6, oxygensensorcurrentfuel7, oxygensensorcurrentfuel8," +
                            " catalysttempbank1sensor1, catalysttempbank2sensor1, catalysttempbank1sensor2, catalysttempbank2sensor2, pidsupported41to60, monitorstatusthisdrivecycle, controlmodulevoltage, absoluteloadvalue, fuelaircommandedequratio, relativethrottleposition, ambientairtemp, absthrottlepositionb, absolutethrottlepositionc, acceleratorpedalpositiond, acceleratorpedalpositione, acceleratorpedalpositionf, commandedthrottleactuator, timerunwithmil, timesincetroublecodescleared, maximumvalueforfuelair, maximumvalueforairflow, fueltype, ethanolfuel, absoluteevapsystemvaporpressure, evapsystemvaporpressure, shorttermsecoxygenbank1bank3, longtermsecoxygenbank1bank3, shorttermsecoxygenbank2bank4, longtermsecoxygenbank2bank4, fuelrailabsolutepressure, relativeacceleratorpedalposition, hybridbatterypackremaininglife, engineoiltemperature, fuelinjectiontiming, fuelrate, emissionrequirements, pidsupported61to80, driversdemandengine, actualenginepercenttorque, enginereferencetorque, enginepercenttorquedata, auxiliaryinputoutputsupported, massairflowsensor, enginecoolanttemperature, intakeairtemperaturesensor, commandedegrandegrerror, commandeddieselintakeairflow, exhaustgasrecirculationtemperature, commandedthrottleactuatorcontrol, fuelpressurecontrolsystem, injectionpressurecontrolsystem, turbochargercompressorinletpressure, boostpressurecontrol, variablegeometryturbocontrol, wastegatecontrol, exhaustpressure, turbochargerrpm, turbochargertemperature1, turbochargertemperature2, chargeaircoolertemperature, exhaustgastemperaturebank1, exhaustgastemperaturebank2, dieselparticulatefilter1, dieselparticulatefilter2, dieselparticulatefiltertemperature, noxntecontrolareastatus, pmntecontrolareastatus, engineruntime, pidsupported81toa0, engineruntimeforaecd1, engineruntimeforaecd2, noxsensor, manifoldsurfacetemperature, noxreagentsystem, particulatemattersensor, intakemanifoldabsolutepressure2) " +
                            " VALUES(@deviceno, @recorddatetime, @latitude, @longitude, @mobileno, @packettype, @sequencenumber, @sysdatetime, @isactive, @dtcdetected, @pidsupport1to20, @statusdtccleared, @freezedtc, @fuelsystemstatus, @calculatedengineload, @coolanttemperature, @shorttermfueltrim, @longtermfueltrim, @shorttermfueltrimbank2, @longtermfueltrimbank2, @fuelpressure, @intakemanifoldabsolutepressure, @rpm, @speedcan, @timingadvance, @intakeairtemp, @mafairflowrate, @throttlepos, @commandedsecondaryairstatus, @oxygensensorspresent, @oxygensensorvoltage1, @oxygensensorvoltage2, @oxygensensorvoltage3, @oxygensensorvoltage4, @oxygensensorvoltage5, @oxygensensorvoltage6, @oxygensensorvoltage7, @oxygensensorvoltage8, @obdstandards, @oxygensensorspresent4banks, @auxiliaryinputstatus, @runtimesinceenginestart, @pidsupported21to40, @distancetraveledmil, @relativefuelrailpressure, @directfuelrailpressure, @oxygensensorvoltagefuel1, @oxygensensorvoltagefuel2, @oxygensensorvoltagefuel3, @oxygensensorvoltagefuel4, @oxygensensorvoltagefuel5, @oxygensensorvoltagefuel6, @oxygensensorvoltagefuel7, @oxygensensorvoltagefuel8, @commandedegr, @egrerror, @commandedevaporativepurge, @fuellevel, @warmupssc, @distsincecodecleared, @systemvaporpressure, @barometricpressure, @oxygensensorcurrentfuel1, @oxygensensorcurrentfuel2, @oxygensensorcurrentfuel3, @oxygensensorcurrentfuel4, @oxygensensorcurrentfuel5, @oxygensensorcurrentfuel6, @oxygensensorcurrentfuel7, @oxygensensorcurrentfuel8, @catalysttempbank1sensor1, @catalysttempbank2sensor1, @catalysttempbank1sensor2, @catalysttempbank2sensor2, @pidsupported41to60, @monitorstatusthisdrivecycle, @controlmodulevoltage, @absoluteloadvalue, @fuelaircommandedequratio, @relativethrottleposition, @ambientairtemp, @absthrottlepositionb, @absolutethrottlepositionc, @acceleratorpedalpositiond, @acceleratorpedalpositione, @acceleratorpedalpositionf, @commandedthrottleactuator, @timerunwithmil, @timesincetroublecodescleared, " +
                            " @maximumvalueforfuelair, @maximumvalueforairflow, @fueltype, @ethanolfuel, @absoluteevapsystemvaporpressure, @evapsystemvaporpressure, @shorttermsecoxygenbank1bank3, @longtermsecoxygenbank1bank3, @shorttermsecoxygenbank2bank4, @longtermsecoxygenbank2bank4, @fuelrailabsolutepressure, @relativeacceleratorpedalposition, @hybridbatterypackremaininglife, @engineoiltemperature, @fuelinjectiontiming, @fuelrate, @emissionrequirements, @pidsupported61to80, @driversdemandengine, @actualenginepercenttorque, @enginereferencetorque, @enginepercenttorquedata, @auxiliaryinputoutputsupported, @massairflowsensor, @enginecoolanttemperature, @intakeairtemperaturesensor, @commandedegrandegrerror, @commandeddieselintakeairflow, @exhaustgasrecirculationtemperature, @commandedthrottleactuatorcontrol, @fuelpressurecontrolsystem, @injectionpressurecontrolsystem, @turbochargercompressorinletpressure, @boostpressurecontrol, @variablegeometryturbocontrol, @wastegatecontrol, @exhaustpressure, @turbochargerrpm, @turbochargertemperature1, @turbochargertemperature2, @chargeaircoolertemperature, @exhaustgastemperaturebank1, @exhaustgastemperaturebank2, @dieselparticulatefilter1, @dieselparticulatefilter2, @dieselparticulatefiltertemperature, @noxntecontrolareastatus, @pmntecontrolareastatus, @engineruntime, @pidsupported81toa0, @engineruntimeforaecd1, @engineruntimeforaecd2, @noxsensor, @manifoldsurfacetemperature, @noxreagentsystem, @particulatemattersensor, @intakemanifoldabsolutepressure2)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DTCDatas));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (OBDCount > 0)
                    {
                        #region update data in iot_obd_hub
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_obd_hub SET latitude=@latitude,longitude=@longitude,mobileno=@mobileno,packettype=@packettype,sequencenumber=@sequencenumber,sysdatetime=@sysdatetime,isactive=@isactive,dtcdetected=@dtcdetected,pidsupport1to20=@pidsupport1to20,statusdtccleared=@statusdtccleared,freezedtc=@freezedtc,fuelsystemstatus=@fuelsystemstatus,calculatedengineload=@calculatedengineload,coolanttemperature=@coolanttemperature,shorttermfueltrim=@shorttermfueltrim,longtermfueltrim=@longtermfueltrim,shorttermfueltrimbank2=@shorttermfueltrimbank2,longtermfueltrimbank2=@longtermfueltrimbank2,fuelpressure=@fuelpressure,intakemanifoldabsolutepressure=@intakemanifoldabsolutepressure,rpm=@rpm,speedcan=@speedcan,timingadvance=@timingadvance,intakeairtemp=@intakeairtemp,mafairflowrate=@mafairflowrate,throttlepos=@throttlepos,commandedsecondaryairstatus=@commandedsecondaryairstatus,oxygensensorspresent=@oxygensensorspresent,oxygensensorvoltage1=@oxygensensorvoltage1,oxygensensorvoltage2=@oxygensensorvoltage2,oxygensensorvoltage3=@oxygensensorvoltage3,oxygensensorvoltage4=@oxygensensorvoltage4,oxygensensorvoltage5=@oxygensensorvoltage5,oxygensensorvoltage6=@oxygensensorvoltage6,oxygensensorvoltage7=@oxygensensorvoltage7,oxygensensorvoltage8=@oxygensensorvoltage8," +
                            " obdstandards=@obdstandards,oxygensensorspresent4banks=@oxygensensorspresent4banks,auxiliaryinputstatus=@auxiliaryinputstatus,runtimesinceenginestart=@runtimesinceenginestart,pidsupported21to40=@pidsupported21to40,distancetraveledmil=@distancetraveledmil,relativefuelrailpressure=@relativefuelrailpressure,directfuelrailpressure=@directfuelrailpressure,oxygensensorvoltagefuel1=@oxygensensorvoltagefuel1,oxygensensorvoltagefuel2=@oxygensensorvoltagefuel2,oxygensensorvoltagefuel3=@oxygensensorvoltagefuel3,oxygensensorvoltagefuel4=@oxygensensorvoltagefuel4,oxygensensorvoltagefuel5=@oxygensensorvoltagefuel5,oxygensensorvoltagefuel6=@oxygensensorvoltagefuel6,oxygensensorvoltagefuel7=@oxygensensorvoltagefuel7,oxygensensorvoltagefuel8=@oxygensensorvoltagefuel8,commandedegr=@commandedegr,egrerror=@egrerror,commandedevaporativepurge=@commandedevaporativepurge,fuellevel=@fuellevel,warmupssc=@warmupssc,distsincecodecleared=@distsincecodecleared,systemvaporpressure=@systemvaporpressure,barometricpressure=@barometricpressure,oxygensensorcurrentfuel1=@oxygensensorcurrentfuel1,oxygensensorcurrentfuel2=@oxygensensorcurrentfuel2,oxygensensorcurrentfuel3=@oxygensensorcurrentfuel3,oxygensensorcurrentfuel4=@oxygensensorcurrentfuel4,oxygensensorcurrentfuel5=@oxygensensorcurrentfuel5," +
                            " oxygensensorcurrentfuel6=@oxygensensorcurrentfuel6,oxygensensorcurrentfuel7=@oxygensensorcurrentfuel7,oxygensensorcurrentfuel8=@oxygensensorcurrentfuel8,catalysttempbank1sensor1=@catalysttempbank1sensor1,catalysttempbank2sensor1=@catalysttempbank2sensor1,catalysttempbank1sensor2=@catalysttempbank1sensor2,catalysttempbank2sensor2=@catalysttempbank2sensor2,pidsupported41to60=@pidsupported41to60,monitorstatusthisdrivecycle=@monitorstatusthisdrivecycle,controlmodulevoltage=@controlmodulevoltage,absoluteloadvalue=@absoluteloadvalue,fuelaircommandedequratio=@fuelaircommandedequratio,relativethrottleposition=@relativethrottleposition,ambientairtemp=@ambientairtemp,absthrottlepositionb=@absthrottlepositionb,absolutethrottlepositionc=@absolutethrottlepositionc,acceleratorpedalpositiond=@acceleratorpedalpositiond,acceleratorpedalpositione=@acceleratorpedalpositione,acceleratorpedalpositionf=@acceleratorpedalpositionf,commandedthrottleactuator=@commandedthrottleactuator,timerunwithmil=@timerunwithmil,timesincetroublecodescleared=@timesincetroublecodescleared,maximumvalueforfuelair=@maximumvalueforfuelair,maximumvalueforairflow=@maximumvalueforairflow,fueltype=@fueltype,ethanolfuel=@ethanolfuel,absoluteevapsystemvaporpressure=@absoluteevapsystemvaporpressure," +
                            " evapsystemvaporpressure=@evapsystemvaporpressure,shorttermsecoxygenbank1bank3=@shorttermsecoxygenbank1bank3,longtermsecoxygenbank1bank3=@longtermsecoxygenbank1bank3,shorttermsecoxygenbank2bank4=@shorttermsecoxygenbank2bank4,longtermsecoxygenbank2bank4=@longtermsecoxygenbank2bank4,fuelrailabsolutepressure=@fuelrailabsolutepressure,relativeacceleratorpedalposition=@relativeacceleratorpedalposition,hybridbatterypackremaininglife=@hybridbatterypackremaininglife,engineoiltemperature=@engineoiltemperature,fuelinjectiontiming=@fuelinjectiontiming,fuelrate=@fuelrate,emissionrequirements=@emissionrequirements,pidsupported61to80=@pidsupported61to80,driversdemandengine=@driversdemandengine,actualenginepercenttorque=@actualenginepercenttorque,enginereferencetorque=@enginereferencetorque,enginepercenttorquedata=@enginepercenttorquedata,auxiliaryinputoutputsupported=@auxiliaryinputoutputsupported,massairflowsensor=@massairflowsensor,enginecoolanttemperature=@enginecoolanttemperature,intakeairtemperaturesensor=@intakeairtemperaturesensor,commandedegrandegrerror=@commandedegrandegrerror,commandeddieselintakeairflow=@commandeddieselintakeairflow,exhaustgasrecirculationtemperature=@exhaustgasrecirculationtemperature,commandedthrottleactuatorcontrol=@commandedthrottleactuatorcontrol," +
                            " fuelpressurecontrolsystem=@fuelpressurecontrolsystem,injectionpressurecontrolsystem=@injectionpressurecontrolsystem,turbochargercompressorinletpressure=@turbochargercompressorinletpressure,boostpressurecontrol=@boostpressurecontrol,variablegeometryturbocontrol=@variablegeometryturbocontrol,wastegatecontrol=@wastegatecontrol,exhaustpressure=@exhaustpressure,turbochargerrpm=@turbochargerrpm,turbochargertemperature1=@turbochargertemperature1,turbochargertemperature2=@turbochargertemperature2,chargeaircoolertemperature=@chargeaircoolertemperature,exhaustgastemperaturebank1=@exhaustgastemperaturebank1,exhaustgastemperaturebank2=@exhaustgastemperaturebank2,dieselparticulatefilter1=@dieselparticulatefilter1,dieselparticulatefilter2=@dieselparticulatefilter2,dieselparticulatefiltertemperature=@dieselparticulatefiltertemperature,noxntecontrolareastatus=@noxntecontrolareastatus,pmntecontrolareastatus=@pmntecontrolareastatus,engineruntime=@engineruntime,pidsupported81toa0=@pidsupported81toa0,engineruntimeforaecd1=@engineruntimeforaecd1,engineruntimeforaecd2=@engineruntimeforaecd2,noxsensor=@noxsensor,manifoldsurfacetemperature=@manifoldsurfacetemperature,noxreagentsystem=@noxreagentsystem,particulatemattersensor=@particulatemattersensor,intakemanifoldabsolutepressure2=@intakemanifoldabsolutepressure2" +
                            " WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DTCDatas));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    #region check  data avaliable in iot_obd_hub
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_obd_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    OBDCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();

                    #endregion

                    if (OBDCount == 0)
                    {
                        #region insert data in iot_obd_live
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_obd_live(deviceno, recorddatetime, latitude, longitude, mobileno, packettype, sequencenumber, sysdatetime, isactive, dtcdetected, pidsupport1to20, statusdtccleared, freezedtc, fuelsystemstatus, calculatedengineload, coolanttemperature, shorttermfueltrim, longtermfueltrim, shorttermfueltrimbank2, longtermfueltrimbank2, fuelpressure, intakemanifoldabsolutepressure, rpm, speedcan, timingadvance, intakeairtemp, mafairflowrate, throttlepos, commandedsecondaryairstatus, oxygensensorspresent, oxygensensorvoltage1, oxygensensorvoltage2, oxygensensorvoltage3, oxygensensorvoltage4, oxygensensorvoltage5, oxygensensorvoltage6, oxygensensorvoltage7, oxygensensorvoltage8, obdstandards, oxygensensorspresent4banks, auxiliaryinputstatus, runtimesinceenginestart, pidsupported21to40, distancetraveledmil, relativefuelrailpressure, directfuelrailpressure, oxygensensorvoltagefuel1, oxygensensorvoltagefuel2, oxygensensorvoltagefuel3, oxygensensorvoltagefuel4, oxygensensorvoltagefuel5, oxygensensorvoltagefuel6, oxygensensorvoltagefuel7, oxygensensorvoltagefuel8, commandedegr, egrerror, commandedevaporativepurge, fuellevel, warmupssc, distsincecodecleared, systemvaporpressure, barometricpressure, oxygensensorcurrentfuel1, oxygensensorcurrentfuel2, oxygensensorcurrentfuel3, oxygensensorcurrentfuel4, oxygensensorcurrentfuel5, oxygensensorcurrentfuel6, oxygensensorcurrentfuel7, oxygensensorcurrentfuel8," +
                        " catalysttempbank1sensor1, catalysttempbank2sensor1, catalysttempbank1sensor2, catalysttempbank2sensor2, pidsupported41to60, monitorstatusthisdrivecycle, controlmodulevoltage, absoluteloadvalue, fuelaircommandedequratio, relativethrottleposition, ambientairtemp, absthrottlepositionb, absolutethrottlepositionc, acceleratorpedalpositiond, acceleratorpedalpositione, acceleratorpedalpositionf, commandedthrottleactuator, timerunwithmil, timesincetroublecodescleared, maximumvalueforfuelair, maximumvalueforairflow, fueltype, ethanolfuel, absoluteevapsystemvaporpressure, evapsystemvaporpressure, shorttermsecoxygenbank1bank3, longtermsecoxygenbank1bank3, shorttermsecoxygenbank2bank4, longtermsecoxygenbank2bank4, fuelrailabsolutepressure, relativeacceleratorpedalposition, hybridbatterypackremaininglife, engineoiltemperature, fuelinjectiontiming, fuelrate, emissionrequirements, pidsupported61to80, driversdemandengine, actualenginepercenttorque, enginereferencetorque, enginepercenttorquedata, auxiliaryinputoutputsupported, massairflowsensor, enginecoolanttemperature, intakeairtemperaturesensor, commandedegrandegrerror, commandeddieselintakeairflow, exhaustgasrecirculationtemperature, commandedthrottleactuatorcontrol, fuelpressurecontrolsystem, injectionpressurecontrolsystem, turbochargercompressorinletpressure, boostpressurecontrol, variablegeometryturbocontrol, wastegatecontrol, exhaustpressure, turbochargerrpm, turbochargertemperature1, turbochargertemperature2, chargeaircoolertemperature, exhaustgastemperaturebank1, exhaustgastemperaturebank2, dieselparticulatefilter1, dieselparticulatefilter2, dieselparticulatefiltertemperature, noxntecontrolareastatus, pmntecontrolareastatus, engineruntime, pidsupported81toa0, engineruntimeforaecd1, engineruntimeforaecd2, noxsensor, manifoldsurfacetemperature, noxreagentsystem, particulatemattersensor, intakemanifoldabsolutepressure2) " +
                        " VALUES(@deviceno, @recorddatetime, @latitude, @longitude, @mobileno, @packettype, @sequencenumber, @sysdatetime, @isactive, @dtcdetected, @pidsupport1to20, @statusdtccleared, @freezedtc, @fuelsystemstatus, @calculatedengineload, @coolanttemperature, @shorttermfueltrim, @longtermfueltrim, @shorttermfueltrimbank2, @longtermfueltrimbank2, @fuelpressure, @intakemanifoldabsolutepressure, @rpm, @speedcan, @timingadvance, @intakeairtemp, @mafairflowrate, @throttlepos, @commandedsecondaryairstatus, @oxygensensorspresent, @oxygensensorvoltage1, @oxygensensorvoltage2, @oxygensensorvoltage3, @oxygensensorvoltage4, @oxygensensorvoltage5, @oxygensensorvoltage6, @oxygensensorvoltage7, @oxygensensorvoltage8, @obdstandards, @oxygensensorspresent4banks, @auxiliaryinputstatus, @runtimesinceenginestart, @pidsupported21to40, @distancetraveledmil, @relativefuelrailpressure, @directfuelrailpressure, @oxygensensorvoltagefuel1, @oxygensensorvoltagefuel2, @oxygensensorvoltagefuel3, @oxygensensorvoltagefuel4, @oxygensensorvoltagefuel5, @oxygensensorvoltagefuel6, @oxygensensorvoltagefuel7, @oxygensensorvoltagefuel8, @commandedegr, @egrerror, @commandedevaporativepurge, @fuellevel, @warmupssc, @distsincecodecleared, @systemvaporpressure, @barometricpressure, @oxygensensorcurrentfuel1, @oxygensensorcurrentfuel2, @oxygensensorcurrentfuel3, @oxygensensorcurrentfuel4, @oxygensensorcurrentfuel5, @oxygensensorcurrentfuel6, @oxygensensorcurrentfuel7, @oxygensensorcurrentfuel8, @catalysttempbank1sensor1, @catalysttempbank2sensor1, @catalysttempbank1sensor2, @catalysttempbank2sensor2, @pidsupported41to60, @monitorstatusthisdrivecycle, @controlmodulevoltage, @absoluteloadvalue, @fuelaircommandedequratio, @relativethrottleposition, @ambientairtemp, @absthrottlepositionb, @absolutethrottlepositionc, @acceleratorpedalpositiond, @acceleratorpedalpositione, @acceleratorpedalpositionf, @commandedthrottleactuator, @timerunwithmil, @timesincetroublecodescleared, " +
                        " @maximumvalueforfuelair, @maximumvalueforairflow, @fueltype, @ethanolfuel, @absoluteevapsystemvaporpressure, @evapsystemvaporpressure, @shorttermsecoxygenbank1bank3, @longtermsecoxygenbank1bank3, @shorttermsecoxygenbank2bank4, @longtermsecoxygenbank2bank4, @fuelrailabsolutepressure, @relativeacceleratorpedalposition, @hybridbatterypackremaininglife, @engineoiltemperature, @fuelinjectiontiming, @fuelrate, @emissionrequirements, @pidsupported61to80, @driversdemandengine, @actualenginepercenttorque, @enginereferencetorque, @enginepercenttorquedata, @auxiliaryinputoutputsupported, @massairflowsensor, @enginecoolanttemperature, @intakeairtemperaturesensor, @commandedegrandegrerror, @commandeddieselintakeairflow, @exhaustgasrecirculationtemperature, @commandedthrottleactuatorcontrol, @fuelpressurecontrolsystem, @injectionpressurecontrolsystem, @turbochargercompressorinletpressure, @boostpressurecontrol, @variablegeometryturbocontrol, @wastegatecontrol, @exhaustpressure, @turbochargerrpm, @turbochargertemperature1, @turbochargertemperature2, @chargeaircoolertemperature, @exhaustgastemperaturebank1, @exhaustgastemperaturebank2, @dieselparticulatefilter1, @dieselparticulatefilter2, @dieselparticulatefiltertemperature, @noxntecontrolareastatus, @pmntecontrolareastatus, @engineruntime, @pidsupported81toa0, @engineruntimeforaecd1, @engineruntimeforaecd2, @noxsensor, @manifoldsurfacetemperature, @noxreagentsystem, @particulatemattersensor, @intakemanifoldabsolutepressure2)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DTCDatas));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (OBDCount > 0)
                    {
                        #region update data in iot_obd_live
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_obd_live SET recorddatetime=@recorddatetime,latitude=@latitude,longitude=@longitude,mobileno=@mobileno,packettype=@packettype,sequencenumber=@sequencenumber,sysdatetime=@sysdatetime,isactive=@isactive,dtcdetected=@dtcdetected,pidsupport1to20=@pidsupport1to20,statusdtccleared=@statusdtccleared,freezedtc=@freezedtc,fuelsystemstatus=@fuelsystemstatus,calculatedengineload=@calculatedengineload,coolanttemperature=@coolanttemperature,shorttermfueltrim=@shorttermfueltrim,longtermfueltrim=@longtermfueltrim,shorttermfueltrimbank2=@shorttermfueltrimbank2,longtermfueltrimbank2=@longtermfueltrimbank2,fuelpressure=@fuelpressure,intakemanifoldabsolutepressure=@intakemanifoldabsolutepressure,rpm=@rpm,speedcan=@speedcan,timingadvance=@timingadvance,intakeairtemp=@intakeairtemp,mafairflowrate=@mafairflowrate,throttlepos=@throttlepos,commandedsecondaryairstatus=@commandedsecondaryairstatus,oxygensensorspresent=@oxygensensorspresent,oxygensensorvoltage1=@oxygensensorvoltage1,oxygensensorvoltage2=@oxygensensorvoltage2,oxygensensorvoltage3=@oxygensensorvoltage3,oxygensensorvoltage4=@oxygensensorvoltage4,oxygensensorvoltage5=@oxygensensorvoltage5,oxygensensorvoltage6=@oxygensensorvoltage6,oxygensensorvoltage7=@oxygensensorvoltage7,oxygensensorvoltage8=@oxygensensorvoltage8," +
                            " obdstandards=@obdstandards,oxygensensorspresent4banks=@oxygensensorspresent4banks,auxiliaryinputstatus=@auxiliaryinputstatus,runtimesinceenginestart=@runtimesinceenginestart,pidsupported21to40=@pidsupported21to40,distancetraveledmil=@distancetraveledmil,relativefuelrailpressure=@relativefuelrailpressure,directfuelrailpressure=@directfuelrailpressure,oxygensensorvoltagefuel1=@oxygensensorvoltagefuel1,oxygensensorvoltagefuel2=@oxygensensorvoltagefuel2,oxygensensorvoltagefuel3=@oxygensensorvoltagefuel3,oxygensensorvoltagefuel4=@oxygensensorvoltagefuel4,oxygensensorvoltagefuel5=@oxygensensorvoltagefuel5,oxygensensorvoltagefuel6=@oxygensensorvoltagefuel6,oxygensensorvoltagefuel7=@oxygensensorvoltagefuel7,oxygensensorvoltagefuel8=@oxygensensorvoltagefuel8,commandedegr=@commandedegr,egrerror=@egrerror,commandedevaporativepurge=@commandedevaporativepurge,fuellevel=@fuellevel,warmupssc=@warmupssc,distsincecodecleared=@distsincecodecleared,systemvaporpressure=@systemvaporpressure,barometricpressure=@barometricpressure,oxygensensorcurrentfuel1=@oxygensensorcurrentfuel1,oxygensensorcurrentfuel2=@oxygensensorcurrentfuel2,oxygensensorcurrentfuel3=@oxygensensorcurrentfuel3,oxygensensorcurrentfuel4=@oxygensensorcurrentfuel4,oxygensensorcurrentfuel5=@oxygensensorcurrentfuel5," +
                            " oxygensensorcurrentfuel6=@oxygensensorcurrentfuel6,oxygensensorcurrentfuel7=@oxygensensorcurrentfuel7,oxygensensorcurrentfuel8=@oxygensensorcurrentfuel8,catalysttempbank1sensor1=@catalysttempbank1sensor1,catalysttempbank2sensor1=@catalysttempbank2sensor1,catalysttempbank1sensor2=@catalysttempbank1sensor2,catalysttempbank2sensor2=@catalysttempbank2sensor2,pidsupported41to60=@pidsupported41to60,monitorstatusthisdrivecycle=@monitorstatusthisdrivecycle,controlmodulevoltage=@controlmodulevoltage,absoluteloadvalue=@absoluteloadvalue,fuelaircommandedequratio=@fuelaircommandedequratio,relativethrottleposition=@relativethrottleposition,ambientairtemp=@ambientairtemp,absthrottlepositionb=@absthrottlepositionb,absolutethrottlepositionc=@absolutethrottlepositionc,acceleratorpedalpositiond=@acceleratorpedalpositiond,acceleratorpedalpositione=@acceleratorpedalpositione,acceleratorpedalpositionf=@acceleratorpedalpositionf,commandedthrottleactuator=@commandedthrottleactuator,timerunwithmil=@timerunwithmil,timesincetroublecodescleared=@timesincetroublecodescleared,maximumvalueforfuelair=@maximumvalueforfuelair,maximumvalueforairflow=@maximumvalueforairflow,fueltype=@fueltype,ethanolfuel=@ethanolfuel,absoluteevapsystemvaporpressure=@absoluteevapsystemvaporpressure," +
                            " evapsystemvaporpressure=@evapsystemvaporpressure,shorttermsecoxygenbank1bank3=@shorttermsecoxygenbank1bank3,longtermsecoxygenbank1bank3=@longtermsecoxygenbank1bank3,shorttermsecoxygenbank2bank4=@shorttermsecoxygenbank2bank4,longtermsecoxygenbank2bank4=@longtermsecoxygenbank2bank4,fuelrailabsolutepressure=@fuelrailabsolutepressure,relativeacceleratorpedalposition=@relativeacceleratorpedalposition,hybridbatterypackremaininglife=@hybridbatterypackremaininglife,engineoiltemperature=@engineoiltemperature,fuelinjectiontiming=@fuelinjectiontiming,fuelrate=@fuelrate,emissionrequirements=@emissionrequirements,pidsupported61to80=@pidsupported61to80,driversdemandengine=@driversdemandengine,actualenginepercenttorque=@actualenginepercenttorque,enginereferencetorque=@enginereferencetorque,enginepercenttorquedata=@enginepercenttorquedata,auxiliaryinputoutputsupported=@auxiliaryinputoutputsupported,massairflowsensor=@massairflowsensor,enginecoolanttemperature=@enginecoolanttemperature,intakeairtemperaturesensor=@intakeairtemperaturesensor,commandedegrandegrerror=@commandedegrandegrerror,commandeddieselintakeairflow=@commandeddieselintakeairflow,exhaustgasrecirculationtemperature=@exhaustgasrecirculationtemperature,commandedthrottleactuatorcontrol=@commandedthrottleactuatorcontrol," +
                            " fuelpressurecontrolsystem=@fuelpressurecontrolsystem,injectionpressurecontrolsystem=@injectionpressurecontrolsystem,turbochargercompressorinletpressure=@turbochargercompressorinletpressure,boostpressurecontrol=@boostpressurecontrol,variablegeometryturbocontrol=@variablegeometryturbocontrol,wastegatecontrol=@wastegatecontrol,exhaustpressure=@exhaustpressure,turbochargerrpm=@turbochargerrpm,turbochargertemperature1=@turbochargertemperature1,turbochargertemperature2=@turbochargertemperature2,chargeaircoolertemperature=@chargeaircoolertemperature,exhaustgastemperaturebank1=@exhaustgastemperaturebank1,exhaustgastemperaturebank2=@exhaustgastemperaturebank2,dieselparticulatefilter1=@dieselparticulatefilter1,dieselparticulatefilter2=@dieselparticulatefilter2,dieselparticulatefiltertemperature=@dieselparticulatefiltertemperature,noxntecontrolareastatus=@noxntecontrolareastatus,pmntecontrolareastatus=@pmntecontrolareastatus,engineruntime=@engineruntime,pidsupported81toa0=@pidsupported81toa0,engineruntimeforaecd1=@engineruntimeforaecd1,engineruntimeforaecd2=@engineruntimeforaecd2,noxsensor=@noxsensor,manifoldsurfacetemperature=@manifoldsurfacetemperature,noxreagentsystem=@noxreagentsystem,particulatemattersensor=@particulatemattersensor,intakemanifoldabsolutepressure2=@intakemanifoldabsolutepressure2" +
                            " WHERE deviceno = @DeviceNo";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude.ToString()));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", mobileno));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DTCDatas));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    tran.Commit();
                    connection.Close();
                }

                functionReturnValue = true;


            }
            catch (Exception ex)
            {
                ErrorLog.SaveLogToDatabase(DeviceNo, "M-dataobj-OBD-DBSaveError", ex.ToString(), strjson);
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        private DateTime ConvertToDatetimeFormat(string strdate, string strtime)
        {

            if (strdate.Length == 5)
            {
                strdate = "0" + strdate;
            }
            if (strtime.Length == 5)
            {
                strtime = "0" + strtime;
            }

            string DatePart = string.Empty;
            string TimePart;
            DateTime ActualDateTime;

            if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "DD/MM/YYYY" | CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "DD-MM-YYYY")
            {
                DatePart = strdate.Substring(0, 2) + "/" + strdate.Substring(2, 2) + "/" + strdate.Substring(4, 2);
            }
            else if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "MM/DD/YYYY" | CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "MM-DD-YYYY")
            {
                DatePart = strdate.Substring(2, 2) + "/" + strdate.Substring(0, 2) + "/" + strdate.Substring(4, 2);
            }
            else if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "M/D/YYYY")
            {
                DatePart = strdate.Substring(2, 2) + "/" + strdate.Substring(0, 2) + "/" + strdate.Substring(4, 2);
            }

            TimePart = strtime.Substring(0, 2) + ":" + strtime.Substring(2, 2) + ":" + strtime.Substring(4, 2);
            ActualDateTime = Convert.ToDateTime(DatePart + " " + TimePart);
            //ActualDateTime = ActualDateTime.AddMinutes(330) 'GMT + 5:30 Hrs
            return ActualDateTime;

        }

        private string ConvertDateFormat(string strdate)
        {
            string[] arrdate = strdate.Split('-');
            //for old server
            string newDate = arrdate[2].ToString() + "-" + arrdate[1].ToString() + "-" + arrdate[0].ToString();
            //for new server
            //   string newDate = arrdate[0].ToString() + "-" + arrdate[1].ToString() + "-" + arrdate[2].ToString();
            return newDate;

        }

        private string ConvertDateFormatddmmyyyyhhmmss(string strdate)
        {

            string[] arrdate = strdate.Split(' ');
            string[] arrTime = arrdate[1].ToString().Split('-');
            //ddmmyyyyhhmmss
            // string newDate = strdate.Substring(4,4) + "-" + strdate.Substring(2,2) + "-" + strdate.Substring(0,2) + " " + strdate.Substring(8,2) + ":" + strdate.Substring(10,2) + ":" + strdate.Substring(12,2); 
            string newDate = arrdate[0].ToString() + " " + arrTime[0].ToString() + ":" + arrTime[1].ToString() + ":" + arrTime[2].ToString(); ;
            return newDate;

        }
        #region arrOBDData
        string[,] arrOBDData = new string[,]
        {
            {"pidsupport1to20", ""},
            {"statusdtccleared", ""},
            {"freezedtc", ""},
            {"fuelsystemstatus", ""},
            {"calculatedengineload", ""},
            {"coolanttemperature", ""},
            {"shorttermfueltrim", ""},
            {"longtermfueltrim", ""},
            {"shorttermfueltrimbank2", ""},
            {"longtermfueltrimbank2", ""},
            {"fuelpressure", ""},
            {"intakemanifoldabsolutepressure", ""},
            {"rpm", ""},
            {"speedcan", ""},
            {"timingadvance", ""},
            {"intakeairtemp", ""},
            {"mafairflowrate", ""},
            {"throttlepos", ""},
            {"commandedsecondaryairstatus", ""},
            {"oxygensensorspresent", ""},
            {"oxygensensorvoltage1", ""},
            {"oxygensensorvoltage2", ""},
            {"oxygensensorvoltage3", ""},
            {"oxygensensorvoltage4", ""},
            {"oxygensensorvoltage5", ""},
            {"oxygensensorvoltage6", ""},
            {"oxygensensorvoltage7", ""},
            {"oxygensensorvoltage8", ""},
            {"obdstandards", ""},
            {"oxygensensorspresent4banks", ""},
            {"auxiliaryinputstatus", ""},
            {"runtimesinceenginestart", ""},
            {"pidsupported21to40", ""},
            {"distancetraveledmil", ""},
            {"relativefuelrailpressure", ""},
            {"directfuelrailpressure", ""},
            {"oxygensensorvoltagefuel1", ""},
            {"oxygensensorvoltagefuel2", ""},
            {"oxygensensorvoltagefuel3", ""},
            {"oxygensensorvoltagefuel4", ""},
            {"oxygensensorvoltagefuel5", ""},
            {"oxygensensorvoltagefuel6", ""},
            {"oxygensensorvoltagefuel7", ""},
            {"oxygensensorvoltagefuel8", ""},
            {"commandedegr", ""},
            {"egrerror", ""},
            {"commandedevaporativepurge", ""},
            {"fuellevel", ""},
            {"warmupssc", ""},
            {"distsincecodecleared", ""},
            {"systemvaporpressure", ""},
            {"barometricpressure", ""},
            {"oxygensensorcurrentfuel1", ""},
            {"oxygensensorcurrentfuel2", ""},
            {"oxygensensorcurrentfuel3", ""},
            {"oxygensensorcurrentfuel4", ""},
            {"oxygensensorcurrentfuel5", ""},
            {"oxygensensorcurrentfuel6", ""},
            {"oxygensensorcurrentfuel7", ""},
            {"oxygensensorcurrentfuel8", ""},
            {"catalysttempbank1sensor1", ""},
            {"catalysttempbank2sensor1", ""},
            {"catalysttempbank1sensor2", ""},
            {"catalysttempbank2sensor2", ""},
            {"pidsupported41to60", ""},
            {"monitorstatusthisdrivecycle", ""},
            {"controlmodulevoltage", ""},
            {"absoluteloadvalue", ""},
            {"fuelaircommandedequratio", ""},
            {"relativethrottleposition", ""},
            {"ambientairtemp", ""},
            {"absthrottlepositionb", ""},
            {"absolutethrottlepositionc", ""},
            {"acceleratorpedalpositiond", ""},
            {"acceleratorpedalpositione", ""},
            {"acceleratorpedalpositionf", ""},
            {"commandedthrottleactuator", ""},
            {"timerunwithmil", ""},
            {"timesincetroublecodescleared", ""},
            {"maximumvalueforfuelair", ""},
            {"maximumvalueforairflow", ""},
            {"fueltype", ""},
            {"ethanolfuel", ""},
            {"absoluteevapsystemvaporpressure", ""},
            {"evapsystemvaporpressure", ""},
            {"shorttermsecoxygenbank1bank3", ""},
            {"longtermsecoxygenbank1bank3", ""},
            {"shorttermsecoxygenbank2bank4", ""},
            {"longtermsecoxygenbank2bank4", ""},
            {"fuelrailabsolutepressure", ""},
            {"relativeacceleratorpedalposition", ""},
            {"hybridbatterypackremaininglife", ""},
            {"engineoiltemperature", ""},
            {"fuelinjectiontiming", ""},
            {"fuelrate", ""},
            {"emissionrequirements", ""},
            {"pidsupported61to80", ""},
            {"driversdemandengine", ""},
            {"actualenginepercenttorque", ""},
            {"enginereferencetorque", ""},
            {"enginepercenttorquedata", ""},
            {"auxiliaryinputoutputsupported", ""},
            {"massairflowsensor", ""},
            {"enginecoolanttemperature", ""},
            {"intakeairtemperaturesensor", ""},
            {"commandedegrandegrerror", ""},
            {"commandeddieselintakeairflow", ""},
            {"exhaustgasrecirculationtemperature", ""},
            {"commandedthrottleactuatorcontrol", ""},
            {"fuelpressurecontrolsystem", ""},
            {"injectionpressurecontrolsystem", ""},
            {"turbochargercompressorinletpressure", ""},
            {"boostpressurecontrol", ""},
            {"variablegeometryturbocontrol", ""},
            {"wastegatecontrol", ""},
            {"exhaustpressure", ""},
            {"turbochargerrpm", ""},
            {"turbochargertemperature1", ""},
            {"turbochargertemperature2", ""},
            {"chargeaircoolertemperature", ""},
            {"exhaustgastemperaturebank1", ""},
            {"exhaustgastemperaturebank2", ""},
            {"dieselparticulatefilter1", ""},
            {"dieselparticulatefilter2", ""},
            {"dieselparticulatefiltertemperature", ""},
            {"noxntecontrolareastatus", ""},
            {"pmntecontrolareastatus", ""},
            {"engineruntime", ""},
            {"pidsupported81toa0", ""},
            {"engineruntimeforaecd1", ""},
            {"engineruntimeforaecd2", ""},
            {"noxsensor", ""},
            {"manifoldsurfacetemperature", ""},
            {"noxreagentsystem", ""},
            {"particulatemattersensor", ""},
            {"intakemanifoldabsolutepressure2", ""}
        };
        #endregion

    }
}
