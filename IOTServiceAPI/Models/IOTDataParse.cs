﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTServiceAPI.Models;

namespace IOTService.Models
{
    public class IOTDataParse
    {
        public Task<bool> TM_IOTData(dynamic jsonIOT, string strjson)
        {
            try
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_datalog", strjson, ConfigurationManager.AppSettings["flgSaveLog"].ToString());
                ErrorLog.SaveLogToDatabase(0, "D-dataobj-save", "", strjson);
                string command = jsonIOT.command;

                if (command == "location")
                {
                    TM_LocationData(jsonIOT, strjson);
                }
                else if (command == "alarms")
                {
                    TM_AlarmData(jsonIOT, strjson);
                }
                else if (command == "OBDdata")
                {
                    TM_OBDData(jsonIOT, strjson);
                }

                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Location json data parsing and save

        private bool TM_LocationData(dynamic jsonIOT, string strjson)
        {
            try
            {
                long deviceId = jsonIOT.DeviceID;
                string command = jsonIOT.command;   //Not saved in database
                string state = jsonIOT.state;   //Not saved in database
                decimal latitude = jsonIOT.@params.lat;
                decimal longitude = jsonIOT.@params.lng;
                decimal speed = jsonIOT.@params.Speed;
                decimal heading = jsonIOT.@params.Heading;
                int time = jsonIOT.@params.time;
                int date = jsonIOT.@params.date;
                ///temp solution before move to production remove below solution
                ///{"lat":"0.00000","lng":"0.00000","Speed":"0.0","Heading":"0","time":"000730","date":"0601","SqNmbr":"92","dist":"0.0","GPSValid":"0","History":"1","ign":"1","command":"Accelerometer","params":{"X":"257","Y":"-12","Z":"-15"}}}
                long sequencenumbr;
                decimal dist = 0;
                int ign = 0;


                sequencenumbr = jsonIOT.@params.SqNmbr;
                dist = jsonIOT.@params.dist;
                ign = jsonIOT.@params.ign;
                int GPSValid = jsonIOT.@params.GPSValid;    //Not saved in database
                int history = jsonIOT.@params.History;  //Not saved in database
                string parmCommand = jsonIOT.@params.command;
                int AccX = 0;
                int AccY = 0;
                int AccZ = 0;
                if (parmCommand == "Accelerometer")
                {
                    AccX = jsonIOT.@params.@params.X;
                    AccY = jsonIOT.@params.@params.Y;
                    AccZ = jsonIOT.@params.@params.Z;
                }

                DateTime dtDateTime = ConvertToDatetimeFormat(date.ToString(), time.ToString());

                //if (SaveLocationDataToDatabase(deviceId, command, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, history, parmCommand, AccX, AccY, AccZ) == false)
                //{
                //    TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", strjson, ConfigurationManager.AppSettings["flgDBError"].ToString());
                //}
                SaveLocationDataToDatabase(strjson, deviceId, command, state, latitude, longitude, speed, Convert.ToInt32(heading), dtDateTime, sequencenumbr, dist, GPSValid, history, parmCommand, AccX, AccY, AccZ);
                return true;
            }
            catch (Exception ex)
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", ex.ToString(), ConfigurationManager.AppSettings["flgParseError"].ToString(), false);
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", strjson, ConfigurationManager.AppSettings["flgParseError"].ToString());
                 ErrorLog.SaveLogToDatabase(0, "D-dataobj-ParseError", ex.ToString(), strjson);
                return false;
            }
        }

        public bool SaveLocationDataToDatabase(string strjson,long DeviceNo, string command, string state, decimal Latitude, decimal Longitude, decimal Speed, int Heading, DateTime RecordDateTime, long sequencenumbr, decimal dist, int GPSValid, int history, string parmCommand, int AccelerometerX, int AccelerometerY, int AccelerometerZ)
        {
            bool functionReturnValue = false;
            long cntCount;

            try
            {
                //Insert into database

                //System.Diagnostics.EventLog.WriteEntry("IOT_JSON_TM_v1 ", RecordDateTime + " with convert " + Convert.ToDateTime(RecordDateTime) + EventLogEntryType.Error);

                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    #region check  data avaliable in iot_hub
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_hub WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion


                    if (cntCount == 0)
                    {
                        #region insert data in iot_hub
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_hub(deviceno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                    " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                    " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate)" +
                    " VALUES(@deviceno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                    " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                    " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", dist));

                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", dist));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_hub
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_hub SET latitude = @latitude, longitude = @longitude, " +
                                        " speed = @speed, distance = @distance, accelerometerx = @accelerometerx, accelerometery = @accelerometery, " +
                                        " accelerometerz = @accelerometerz, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate " +
                                        " WHERE deviceno = @deviceno and recorddatetime=@recorddatetime";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", dist));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", dist));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    #region check  data avaliable in iot_live
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_live(deviceno, recorddatetime, latitude,latitudedir,longitude,longitudedir,speed, distance, directionindegree," +
                       " overspeedstarted, mainpowerstatus,ignitionstatus,cornerpacket, hardacceleration, hardbraking,accelerometerx, accelerometery, " +
                       " accelerometerz, sequencenumber, isactive,tmheading,tmgpsOdo,tmgpsvalid,tmhistory,tmpacketstate)" +
                       " VALUES(@deviceno, @recorddatetime, @latitude,@latitudedir,@longitude,@longitudedir,@speed, @distance, @directionindegree," +
                       " @overspeedstarted, @mainpowerstatus,@ignitionstatus,@cornerpacket, @hardacceleration, @hardbraking,@accelerometerx, @accelerometery, " +
                       " @accelerometerz, @sequencenumber, @isactive,@tmheading,@tmgpsOdo,@tmgpsvalid,@tmhistory,@tmpacketstate)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", dist));

                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", dist));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    else if (cntCount > 0)
                    {
                        #region update data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_live SET recorddatetime = @recorddatetime, latitude = @latitude, latitudedir = @latitudedir, longitude = @longitude, longitudedir = @longitudedir, " +
                                        " speed = @speed, distance = @distance, directionindegree = @directionindegree, overspeedstarted = @overspeedstarted, mainpowerstatus = @mainpowerstatus, " +
                                        " ignitionstatus = @ignitionstatus, cornerpacket = @cornerpacket, hardacceleration = @hardacceleration, hardbraking = @hardbraking,  " +
                                        " accelerometerx = @accelerometerx, accelerometery = @accelerometery, accelerometerz = @accelerometerz, isactive = @isactive, sequencenumber = @sequencenumber, sysdatetime = @sysdatetime, " +
                                        " tmheading=@tmheading,tmgpsOdo=@tmgpsOdo,tmgpsvalid=@tmgpsvalid,tmhistory=@tmhistory,tmpacketstate=@tmpacketstate WHERE deviceno = @deviceno";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitudedir", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        ///consider gpsood as distance temp
                        cmd.Parameters.Add(new NpgsqlParameter("@distance", dist));

                        cmd.Parameters.Add(new NpgsqlParameter("@directionindegree", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@overspeedstarted", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@mainpowerstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@ignitionstatus", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@cornerpacket", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardacceleration", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@hardbraking", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerx", AccelerometerX));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometery", AccelerometerY));
                        cmd.Parameters.Add(new NpgsqlParameter("@accelerometerz", AccelerometerZ));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmheading", Heading));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsOdo", dist));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmgpsvalid", GPSValid));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmhistory", history));
                        cmd.Parameters.Add(new NpgsqlParameter("@tmpacketstate", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    tran.Commit();
                    connection.Close();

                }

                functionReturnValue = true;

            }
            catch (Exception ex)
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", ex.ToString(), ConfigurationManager.AppSettings["flgDBError"].ToString(), false);
                ErrorLog.SaveLogToDatabase(DeviceNo, "D-dataobj-DBSaveError", ex.ToString(), strjson);
                functionReturnValue = false;
            }

            return functionReturnValue;
        }

        #endregion

        #region ALARMS json data parsing and save

        private void TM_AlarmData(dynamic jsonIOT, string strjson)
        {
            try
            {
                string DBkeyName = string.Empty;
                string key = jsonIOT.@params.key;

                if (key.ToLower() == "ignition")
                {
                    DBkeyName = "ignitionstatus";
                }

                if (key.ToLower() == "break")
                {
                    DBkeyName = "hardbraking";
                }
                if (key.ToLower() == "acceleration")
                {
                    DBkeyName = "hardacceleration";
                }
                if (key == "crash")
                {
                    DBkeyName = "tamperalert";
                }
                if (key.ToLower() == "power")
                {
                    DBkeyName = "mainpowerstatus";
                }
                if (key.ToLower() == "overspeed")
                {
                    DBkeyName = "overspeedstarted";
                }


                if (DBkeyName != string.Empty)
                {
                    int state = 0;

                    long deviceId = jsonIOT.DeviceID;
                    string command = jsonIOT.command;

                    state = jsonIOT.@params.state;

                    decimal latitude = jsonIOT.@params.lat;
                    decimal longitude = jsonIOT.@params.lng;
                    decimal speed = jsonIOT.@params.Speed;
                    int time = jsonIOT.@params.time;
                    int date = jsonIOT.@params.date;

                    DateTime dtDateTime = ConvertToDatetimeFormat(date.ToString(), time.ToString());


                    long sequencenumbr;
                    try
                    {
                        sequencenumbr = jsonIOT.@params.SqNmbr;
                    }
                    catch (Exception ex1)
                    {
                        sequencenumbr = jsonIOT.@params.SequenceNmbr;
                    }

                    //if (SaveAlertsToDatabase(deviceId, command, DBkeyName, state, latitude, longitude, speed, dtDateTime, sequencenumbr) == false)
                    //{
                    //    TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", strjson, ConfigurationManager.AppSettings["flgDBError"].ToString());
                    //}
                    SaveAlertsToDatabase(strjson,deviceId, command, DBkeyName, state, latitude, longitude, speed, dtDateTime, sequencenumbr);
                }

            }
            catch (Exception ex)
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", ex.ToString(), ConfigurationManager.AppSettings["flgParseError"].ToString(), false);
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", strjson, ConfigurationManager.AppSettings["flgParseError"].ToString());
                ErrorLog.SaveLogToDatabase(0, "D-dataobj-ParseError", ex.ToString(), strjson);
            }
        }
        public bool SaveAlertsToDatabase(string strjson,long DeviceNo, string command, string key, int state, decimal Latitude, decimal Longitude, decimal Speed, DateTime RecordDateTime, long sequencenumbr)
        {

            bool functionReturnValue = false;
            long cntCount;

            try
            {
                //Insert into database

                //System.Diagnostics.EventLog.WriteEntry("IOT_JSON_TM_v1 ", RecordDateTime + " with convert " + Convert.ToDateTime(RecordDateTime) + EventLogEntryType.Error);

                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    #region check  data avaliable in iot_hub
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_hub WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion


                    if (cntCount == 0)
                    {
                        #region insert data in iot_hub
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        //long DeviceNo, string command, string key, int state,decimal Latitude, decimal Longitude, decimal Speed,DateTime RecordDateTime, long sequencenumbr
                        cmd.CommandText = " INSERT INTO iot_hub(deviceno, recorddatetime, latitude,longitude,speed, " + key + ",sequencenumber,isactive)" +
                                   " VALUES(@deviceno, @recorddatetime, @latitude,@longitude,@speed, @" + key + ",@sequencenumber, @isactive)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        cmd.Parameters.Add(new NpgsqlParameter("@" + key + "", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (cntCount > 0)
                    {
                        #region update data in iot_hub
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_hub SET sysdatetime=@sysdatetime," + key + " = @" + key + " WHERE deviceno = @DeviceNo and recorddatetime=@recorddatetime ";

                        cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@" + key + "", state));
                        //cmd.Parameters.Add(new NpgsqlParameter("@SequenceNumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    #region check  data avaliable in iot_live
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    cntCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    #endregion

                    if (cntCount == 0)
                    {
                        #region insert data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_live(deviceno, recorddatetime, latitude,longitude,speed, " + key + ",sequencenumber,isactive)" +
                                   " VALUES(@deviceno, @recorddatetime, @latitude,@longitude,@speed, @" + key + ",@sequencenumber, @isactive)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        cmd.Parameters.Add(new NpgsqlParameter("@" + key + "", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    else if (cntCount > 0)
                    {
                        #region update data in iot_live
                        cmd = new NpgsqlCommand();
                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_live SET recorddatetime=@recorddatetime, latitude=@latitude,longitude=@longitude,speed=@speed,sysdatetime=@sysdatetime," + key + " = @" + key + " WHERE deviceno = @DeviceNo ";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@speed", Speed));
                        cmd.Parameters.Add(new NpgsqlParameter("@" + key + "", state));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", sequencenumbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    tran.Commit();
                    connection.Close();

                }

                functionReturnValue = true;

            }
            catch (Exception ex)
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", ex.ToString(), ConfigurationManager.AppSettings["flgDBError"].ToString(), false);
                ErrorLog.SaveLogToDatabase(DeviceNo, "D-dataobj-DBSaveError", ex.ToString(), strjson);
                functionReturnValue = false;
            }

            return functionReturnValue;

        }

        #endregion

        #region OBDa json data parsing and save

        private void TM_OBDData(dynamic jsonIOT, string strjson)
        {
            try
            {
                long DeviceID = jsonIOT.DeviceID;
                string command = jsonIOT.command;

                //string mil = jsonIOT.@params.mil;
                //string fuels = jsonIOT.@params.fuels;
                //string egl = jsonIOT.@params.egl;
                //string engtemp = jsonIOT.@params.engtemp;

                //string rpm = jsonIOT.@params.rpm;
                //string spd = jsonIOT.@params.spd;

                //string airtemp = jsonIOT.@params.airtemp;
                //string maf = jsonIOT.@params.maf;
                //string sensep = jsonIOT.@params.sensep;
                //string prot = jsonIOT.@params.prot;

                //string rtes = jsonIOT.@params.rtes;
                //string dtmilon = jsonIOT.@params.dtmilon;

                //string prs = jsonIOT.@params.prs;
                //string cmodv = jsonIOT.@params.cmodv;

                string time = jsonIOT.time;
                string date = jsonIOT.date;

                DateTime RecordDateTime = ConvertToDatetimeFormat(date.ToString(), time.ToString());
                long sequencenumbr = 0;

                //try
                //{
                sequencenumbr = jsonIOT.SqNmbr;
                //}
                //catch (Exception ex1)
                //{
                //    sequencenumbr = jsonIOT.@params.SequenceNmbr;
                //}

                foreach (JProperty OBDItem in jsonIOT.@params)
                {
                    try
                    {
                        int OBDindex = Convert.ToInt32(OBDItem.Name, 16);
                        if (OBDindex < arrOBDData.Length)
                        {
                            arrOBDData[OBDindex, 1] = OBDItem.Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }

                //if (SaveOBDDataToDatabase(DeviceID, 0, 0, "", 0, RecordDateTime, sequencenumbr, "", arrOBDData) == false)
                //{
                //    TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", strjson, ConfigurationManager.AppSettings["flgDBError"].ToString());
                //}

                SaveOBDDataToDatabase(strjson, DeviceID, 0, 0, "", 0, RecordDateTime, sequencenumbr, "", arrOBDData);

            }
            catch (Exception ex)
            {
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", strjson, ConfigurationManager.AppSettings["flgParseError"].ToString());
                //TM_SaveLogToFile("IOT_JSON_TM_v1_ParseError", ex.Message, ConfigurationManager.AppSettings["flgParseError"].ToString());
                ErrorLog.SaveLogToDatabase(0, "D-dataobj-ParseError", ex.ToString(), strjson);
            }
        }

        public bool SaveOBDDataToDatabase(string strjson, long DeviceNo, decimal Latitude, decimal Longitude, string mobileno, int packettype, DateTime RecordDateTime, long SequenceNmbr, string DTCDatas, string[,] arryOBDData)
        {

            bool functionReturnValue = false;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection())
                {
                    connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
                    connection.Open();

                    NpgsqlTransaction tran = connection.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand();

                    cmd.Connection = connection;
                    cmd.Transaction = tran;


                    #region insert data in iot_obd_hub
                    cmd = new NpgsqlCommand();

                    cmd.Connection = connection;
                    cmd.Transaction = tran;

                    cmd.CommandText = " INSERT INTO iot_obd_hub(deviceno, recorddatetime, latitude, longitude, mobileno, packettype, sequencenumber, sysdatetime, isactive, dtcdetected, pidsupport1to20, statusdtccleared, freezedtc, fuelsystemstatus, calculatedengineload, coolanttemperature, shorttermfueltrim, longtermfueltrim, shorttermfueltrimbank2, longtermfueltrimbank2, fuelpressure, intakemanifoldabsolutepressure, rpm, speedcan, timingadvance, intakeairtemp, mafairflowrate, throttlepos, commandedsecondaryairstatus, oxygensensorspresent, oxygensensorvoltage1, oxygensensorvoltage2, oxygensensorvoltage3, oxygensensorvoltage4, oxygensensorvoltage5, oxygensensorvoltage6, oxygensensorvoltage7, oxygensensorvoltage8, obdstandards, oxygensensorspresent4banks, auxiliaryinputstatus, runtimesinceenginestart, pidsupported21to40, distancetraveledmil, relativefuelrailpressure, directfuelrailpressure, oxygensensorvoltagefuel1, oxygensensorvoltagefuel2, oxygensensorvoltagefuel3, oxygensensorvoltagefuel4, oxygensensorvoltagefuel5, oxygensensorvoltagefuel6, oxygensensorvoltagefuel7, oxygensensorvoltagefuel8, commandedegr, egrerror, commandedevaporativepurge, fuellevel, warmupssc, distsincecodecleared, systemvaporpressure, barometricpressure, oxygensensorcurrentfuel1, oxygensensorcurrentfuel2, oxygensensorcurrentfuel3, oxygensensorcurrentfuel4, oxygensensorcurrentfuel5, oxygensensorcurrentfuel6, oxygensensorcurrentfuel7, oxygensensorcurrentfuel8," +
                        " catalysttempbank1sensor1, catalysttempbank2sensor1, catalysttempbank1sensor2, catalysttempbank2sensor2, pidsupported41to60, monitorstatusthisdrivecycle, controlmodulevoltage, absoluteloadvalue, fuelaircommandedequratio, relativethrottleposition, ambientairtemp, absthrottlepositionb, absolutethrottlepositionc, acceleratorpedalpositiond, acceleratorpedalpositione, acceleratorpedalpositionf, commandedthrottleactuator, timerunwithmil, timesincetroublecodescleared, maximumvalueforfuelair, maximumvalueforairflow, fueltype, ethanolfuel, absoluteevapsystemvaporpressure, evapsystemvaporpressure, shorttermsecoxygenbank1bank3, longtermsecoxygenbank1bank3, shorttermsecoxygenbank2bank4, longtermsecoxygenbank2bank4, fuelrailabsolutepressure, relativeacceleratorpedalposition, hybridbatterypackremaininglife, engineoiltemperature, fuelinjectiontiming, fuelrate, emissionrequirements, pidsupported61to80, driversdemandengine, actualenginepercenttorque, enginereferencetorque, enginepercenttorquedata, auxiliaryinputoutputsupported, massairflowsensor, enginecoolanttemperature, intakeairtemperaturesensor, commandedegrandegrerror, commandeddieselintakeairflow, exhaustgasrecirculationtemperature, commandedthrottleactuatorcontrol, fuelpressurecontrolsystem, injectionpressurecontrolsystem, turbochargercompressorinletpressure, boostpressurecontrol, variablegeometryturbocontrol, wastegatecontrol, exhaustpressure, turbochargerrpm, turbochargertemperature1, turbochargertemperature2, chargeaircoolertemperature, exhaustgastemperaturebank1, exhaustgastemperaturebank2, dieselparticulatefilter1, dieselparticulatefilter2, dieselparticulatefiltertemperature, noxntecontrolareastatus, pmntecontrolareastatus, engineruntime, pidsupported81toa0, engineruntimeforaecd1, engineruntimeforaecd2, noxsensor, manifoldsurfacetemperature, noxreagentsystem, particulatemattersensor, intakemanifoldabsolutepressure2) " +
                        " VALUES(@deviceno, @recorddatetime, @latitude, @longitude, @mobileno, @packettype, @sequencenumber, @sysdatetime, @isactive, @dtcdetected, @pidsupport1to20, @statusdtccleared, @freezedtc, @fuelsystemstatus, @calculatedengineload, @coolanttemperature, @shorttermfueltrim, @longtermfueltrim, @shorttermfueltrimbank2, @longtermfueltrimbank2, @fuelpressure, @intakemanifoldabsolutepressure, @rpm, @speedcan, @timingadvance, @intakeairtemp, @mafairflowrate, @throttlepos, @commandedsecondaryairstatus, @oxygensensorspresent, @oxygensensorvoltage1, @oxygensensorvoltage2, @oxygensensorvoltage3, @oxygensensorvoltage4, @oxygensensorvoltage5, @oxygensensorvoltage6, @oxygensensorvoltage7, @oxygensensorvoltage8, @obdstandards, @oxygensensorspresent4banks, @auxiliaryinputstatus, @runtimesinceenginestart, @pidsupported21to40, @distancetraveledmil, @relativefuelrailpressure, @directfuelrailpressure, @oxygensensorvoltagefuel1, @oxygensensorvoltagefuel2, @oxygensensorvoltagefuel3, @oxygensensorvoltagefuel4, @oxygensensorvoltagefuel5, @oxygensensorvoltagefuel6, @oxygensensorvoltagefuel7, @oxygensensorvoltagefuel8, @commandedegr, @egrerror, @commandedevaporativepurge, @fuellevel, @warmupssc, @distsincecodecleared, @systemvaporpressure, @barometricpressure, @oxygensensorcurrentfuel1, @oxygensensorcurrentfuel2, @oxygensensorcurrentfuel3, @oxygensensorcurrentfuel4, @oxygensensorcurrentfuel5, @oxygensensorcurrentfuel6, @oxygensensorcurrentfuel7, @oxygensensorcurrentfuel8, @catalysttempbank1sensor1, @catalysttempbank2sensor1, @catalysttempbank1sensor2, @catalysttempbank2sensor2, @pidsupported41to60, @monitorstatusthisdrivecycle, @controlmodulevoltage, @absoluteloadvalue, @fuelaircommandedequratio, @relativethrottleposition, @ambientairtemp, @absthrottlepositionb, @absolutethrottlepositionc, @acceleratorpedalpositiond, @acceleratorpedalpositione, @acceleratorpedalpositionf, @commandedthrottleactuator, @timerunwithmil, @timesincetroublecodescleared, " +
                        " @maximumvalueforfuelair, @maximumvalueforairflow, @fueltype, @ethanolfuel, @absoluteevapsystemvaporpressure, @evapsystemvaporpressure, @shorttermsecoxygenbank1bank3, @longtermsecoxygenbank1bank3, @shorttermsecoxygenbank2bank4, @longtermsecoxygenbank2bank4, @fuelrailabsolutepressure, @relativeacceleratorpedalposition, @hybridbatterypackremaininglife, @engineoiltemperature, @fuelinjectiontiming, @fuelrate, @emissionrequirements, @pidsupported61to80, @driversdemandengine, @actualenginepercenttorque, @enginereferencetorque, @enginepercenttorquedata, @auxiliaryinputoutputsupported, @massairflowsensor, @enginecoolanttemperature, @intakeairtemperaturesensor, @commandedegrandegrerror, @commandeddieselintakeairflow, @exhaustgasrecirculationtemperature, @commandedthrottleactuatorcontrol, @fuelpressurecontrolsystem, @injectionpressurecontrolsystem, @turbochargercompressorinletpressure, @boostpressurecontrol, @variablegeometryturbocontrol, @wastegatecontrol, @exhaustpressure, @turbochargerrpm, @turbochargertemperature1, @turbochargertemperature2, @chargeaircoolertemperature, @exhaustgastemperaturebank1, @exhaustgastemperaturebank2, @dieselparticulatefilter1, @dieselparticulatefilter2, @dieselparticulatefiltertemperature, @noxntecontrolareastatus, @pmntecontrolareastatus, @engineruntime, @pidsupported81toa0, @engineruntimeforaecd1, @engineruntimeforaecd2, @noxsensor, @manifoldsurfacetemperature, @noxreagentsystem, @particulatemattersensor, @intakemanifoldabsolutepressure2)";

                    cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                    cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                    cmd.Parameters.Add(new NpgsqlParameter("@latitude", DBNull.Value));
                    cmd.Parameters.Add(new NpgsqlParameter("@longitude", DBNull.Value));
                    cmd.Parameters.Add(new NpgsqlParameter("@mobileno", DBNull.Value));
                    cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                    cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                    cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                    cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                    cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DBNull.Value));

                    for (int i = 0; i < arryOBDData.GetLength(0); i++)
                    {
                        if (arryOBDData[i, 1] == "")
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                        }
                        else
                        {
                            cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                        }
                    }

                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    #endregion

                    #region check  data avaliable in iot_obd_live
                    long OBDCount;
                    cmd = new NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.Transaction = tran;
                    cmd.CommandText = "SELECT count(*) FROM iot_obd_live WHERE deviceno = @DeviceNo";
                    cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
                    OBDCount = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    //----------------------------------
                    #endregion

                    if (OBDCount == 0)
                    {

                        #region insert data in iot_obd_live
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " INSERT INTO iot_obd_live(deviceno, recorddatetime, latitude, longitude, mobileno, packettype, sequencenumber, sysdatetime, isactive, dtcdetected, pidsupport1to20, statusdtccleared, freezedtc, fuelsystemstatus, calculatedengineload, coolanttemperature, shorttermfueltrim, longtermfueltrim, shorttermfueltrimbank2, longtermfueltrimbank2, fuelpressure, intakemanifoldabsolutepressure, rpm, speedcan, timingadvance, intakeairtemp, mafairflowrate, throttlepos, commandedsecondaryairstatus, oxygensensorspresent, oxygensensorvoltage1, oxygensensorvoltage2, oxygensensorvoltage3, oxygensensorvoltage4, oxygensensorvoltage5, oxygensensorvoltage6, oxygensensorvoltage7, oxygensensorvoltage8, obdstandards, oxygensensorspresent4banks, auxiliaryinputstatus, runtimesinceenginestart, pidsupported21to40, distancetraveledmil, relativefuelrailpressure, directfuelrailpressure, oxygensensorvoltagefuel1, oxygensensorvoltagefuel2, oxygensensorvoltagefuel3, oxygensensorvoltagefuel4, oxygensensorvoltagefuel5, oxygensensorvoltagefuel6, oxygensensorvoltagefuel7, oxygensensorvoltagefuel8, commandedegr, egrerror, commandedevaporativepurge, fuellevel, warmupssc, distsincecodecleared, systemvaporpressure, barometricpressure, oxygensensorcurrentfuel1, oxygensensorcurrentfuel2, oxygensensorcurrentfuel3, oxygensensorcurrentfuel4, oxygensensorcurrentfuel5, oxygensensorcurrentfuel6, oxygensensorcurrentfuel7, oxygensensorcurrentfuel8," +
                        " catalysttempbank1sensor1, catalysttempbank2sensor1, catalysttempbank1sensor2, catalysttempbank2sensor2, pidsupported41to60, monitorstatusthisdrivecycle, controlmodulevoltage, absoluteloadvalue, fuelaircommandedequratio, relativethrottleposition, ambientairtemp, absthrottlepositionb, absolutethrottlepositionc, acceleratorpedalpositiond, acceleratorpedalpositione, acceleratorpedalpositionf, commandedthrottleactuator, timerunwithmil, timesincetroublecodescleared, maximumvalueforfuelair, maximumvalueforairflow, fueltype, ethanolfuel, absoluteevapsystemvaporpressure, evapsystemvaporpressure, shorttermsecoxygenbank1bank3, longtermsecoxygenbank1bank3, shorttermsecoxygenbank2bank4, longtermsecoxygenbank2bank4, fuelrailabsolutepressure, relativeacceleratorpedalposition, hybridbatterypackremaininglife, engineoiltemperature, fuelinjectiontiming, fuelrate, emissionrequirements, pidsupported61to80, driversdemandengine, actualenginepercenttorque, enginereferencetorque, enginepercenttorquedata, auxiliaryinputoutputsupported, massairflowsensor, enginecoolanttemperature, intakeairtemperaturesensor, commandedegrandegrerror, commandeddieselintakeairflow, exhaustgasrecirculationtemperature, commandedthrottleactuatorcontrol, fuelpressurecontrolsystem, injectionpressurecontrolsystem, turbochargercompressorinletpressure, boostpressurecontrol, variablegeometryturbocontrol, wastegatecontrol, exhaustpressure, turbochargerrpm, turbochargertemperature1, turbochargertemperature2, chargeaircoolertemperature, exhaustgastemperaturebank1, exhaustgastemperaturebank2, dieselparticulatefilter1, dieselparticulatefilter2, dieselparticulatefiltertemperature, noxntecontrolareastatus, pmntecontrolareastatus, engineruntime, pidsupported81toa0, engineruntimeforaecd1, engineruntimeforaecd2, noxsensor, manifoldsurfacetemperature, noxreagentsystem, particulatemattersensor, intakemanifoldabsolutepressure2) " +
                        " VALUES(@deviceno, @recorddatetime, @latitude, @longitude, @mobileno, @packettype, @sequencenumber, @sysdatetime, @isactive, @dtcdetected, @pidsupport1to20, @statusdtccleared, @freezedtc, @fuelsystemstatus, @calculatedengineload, @coolanttemperature, @shorttermfueltrim, @longtermfueltrim, @shorttermfueltrimbank2, @longtermfueltrimbank2, @fuelpressure, @intakemanifoldabsolutepressure, @rpm, @speedcan, @timingadvance, @intakeairtemp, @mafairflowrate, @throttlepos, @commandedsecondaryairstatus, @oxygensensorspresent, @oxygensensorvoltage1, @oxygensensorvoltage2, @oxygensensorvoltage3, @oxygensensorvoltage4, @oxygensensorvoltage5, @oxygensensorvoltage6, @oxygensensorvoltage7, @oxygensensorvoltage8, @obdstandards, @oxygensensorspresent4banks, @auxiliaryinputstatus, @runtimesinceenginestart, @pidsupported21to40, @distancetraveledmil, @relativefuelrailpressure, @directfuelrailpressure, @oxygensensorvoltagefuel1, @oxygensensorvoltagefuel2, @oxygensensorvoltagefuel3, @oxygensensorvoltagefuel4, @oxygensensorvoltagefuel5, @oxygensensorvoltagefuel6, @oxygensensorvoltagefuel7, @oxygensensorvoltagefuel8, @commandedegr, @egrerror, @commandedevaporativepurge, @fuellevel, @warmupssc, @distsincecodecleared, @systemvaporpressure, @barometricpressure, @oxygensensorcurrentfuel1, @oxygensensorcurrentfuel2, @oxygensensorcurrentfuel3, @oxygensensorcurrentfuel4, @oxygensensorcurrentfuel5, @oxygensensorcurrentfuel6, @oxygensensorcurrentfuel7, @oxygensensorcurrentfuel8, @catalysttempbank1sensor1, @catalysttempbank2sensor1, @catalysttempbank1sensor2, @catalysttempbank2sensor2, @pidsupported41to60, @monitorstatusthisdrivecycle, @controlmodulevoltage, @absoluteloadvalue, @fuelaircommandedequratio, @relativethrottleposition, @ambientairtemp, @absthrottlepositionb, @absolutethrottlepositionc, @acceleratorpedalpositiond, @acceleratorpedalpositione, @acceleratorpedalpositionf, @commandedthrottleactuator, @timerunwithmil, @timesincetroublecodescleared, " +
                        " @maximumvalueforfuelair, @maximumvalueforairflow, @fueltype, @ethanolfuel, @absoluteevapsystemvaporpressure, @evapsystemvaporpressure, @shorttermsecoxygenbank1bank3, @longtermsecoxygenbank1bank3, @shorttermsecoxygenbank2bank4, @longtermsecoxygenbank2bank4, @fuelrailabsolutepressure, @relativeacceleratorpedalposition, @hybridbatterypackremaininglife, @engineoiltemperature, @fuelinjectiontiming, @fuelrate, @emissionrequirements, @pidsupported61to80, @driversdemandengine, @actualenginepercenttorque, @enginereferencetorque, @enginepercenttorquedata, @auxiliaryinputoutputsupported, @massairflowsensor, @enginecoolanttemperature, @intakeairtemperaturesensor, @commandedegrandegrerror, @commandeddieselintakeairflow, @exhaustgasrecirculationtemperature, @commandedthrottleactuatorcontrol, @fuelpressurecontrolsystem, @injectionpressurecontrolsystem, @turbochargercompressorinletpressure, @boostpressurecontrol, @variablegeometryturbocontrol, @wastegatecontrol, @exhaustpressure, @turbochargerrpm, @turbochargertemperature1, @turbochargertemperature2, @chargeaircoolertemperature, @exhaustgastemperaturebank1, @exhaustgastemperaturebank2, @dieselparticulatefilter1, @dieselparticulatefilter2, @dieselparticulatefiltertemperature, @noxntecontrolareastatus, @pmntecontrolareastatus, @engineruntime, @pidsupported81toa0, @engineruntimeforaecd1, @engineruntimeforaecd2, @noxsensor, @manifoldsurfacetemperature, @noxreagentsystem, @particulatemattersensor, @intakemanifoldabsolutepressure2)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DBNull.Value));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }
                    else if (OBDCount > 0)
                    {

                        #region update data in iot_obd_live
                        cmd = new NpgsqlCommand();

                        cmd.Connection = connection;
                        cmd.Transaction = tran;

                        cmd.CommandText = " UPDATE iot_obd_live SET recorddatetime=@recorddatetime,latitude=@latitude,longitude=@longitude,mobileno=@mobileno,packettype=@packettype,sequencenumber=@sequencenumber,sysdatetime=@sysdatetime,isactive=@isactive,dtcdetected=@dtcdetected,pidsupport1to20=@pidsupport1to20,statusdtccleared=@statusdtccleared,freezedtc=@freezedtc,fuelsystemstatus=@fuelsystemstatus,calculatedengineload=@calculatedengineload,coolanttemperature=@coolanttemperature,shorttermfueltrim=@shorttermfueltrim,longtermfueltrim=@longtermfueltrim,shorttermfueltrimbank2=@shorttermfueltrimbank2,longtermfueltrimbank2=@longtermfueltrimbank2,fuelpressure=@fuelpressure,intakemanifoldabsolutepressure=@intakemanifoldabsolutepressure,rpm=@rpm,speedcan=@speedcan,timingadvance=@timingadvance,intakeairtemp=@intakeairtemp,mafairflowrate=@mafairflowrate,throttlepos=@throttlepos,commandedsecondaryairstatus=@commandedsecondaryairstatus,oxygensensorspresent=@oxygensensorspresent,oxygensensorvoltage1=@oxygensensorvoltage1,oxygensensorvoltage2=@oxygensensorvoltage2,oxygensensorvoltage3=@oxygensensorvoltage3,oxygensensorvoltage4=@oxygensensorvoltage4,oxygensensorvoltage5=@oxygensensorvoltage5,oxygensensorvoltage6=@oxygensensorvoltage6,oxygensensorvoltage7=@oxygensensorvoltage7,oxygensensorvoltage8=@oxygensensorvoltage8," +
                            " obdstandards=@obdstandards,oxygensensorspresent4banks=@oxygensensorspresent4banks,auxiliaryinputstatus=@auxiliaryinputstatus,runtimesinceenginestart=@runtimesinceenginestart,pidsupported21to40=@pidsupported21to40,distancetraveledmil=@distancetraveledmil,relativefuelrailpressure=@relativefuelrailpressure,directfuelrailpressure=@directfuelrailpressure,oxygensensorvoltagefuel1=@oxygensensorvoltagefuel1,oxygensensorvoltagefuel2=@oxygensensorvoltagefuel2,oxygensensorvoltagefuel3=@oxygensensorvoltagefuel3,oxygensensorvoltagefuel4=@oxygensensorvoltagefuel4,oxygensensorvoltagefuel5=@oxygensensorvoltagefuel5,oxygensensorvoltagefuel6=@oxygensensorvoltagefuel6,oxygensensorvoltagefuel7=@oxygensensorvoltagefuel7,oxygensensorvoltagefuel8=@oxygensensorvoltagefuel8,commandedegr=@commandedegr,egrerror=@egrerror,commandedevaporativepurge=@commandedevaporativepurge,fuellevel=@fuellevel,warmupssc=@warmupssc,distsincecodecleared=@distsincecodecleared,systemvaporpressure=@systemvaporpressure,barometricpressure=@barometricpressure,oxygensensorcurrentfuel1=@oxygensensorcurrentfuel1,oxygensensorcurrentfuel2=@oxygensensorcurrentfuel2,oxygensensorcurrentfuel3=@oxygensensorcurrentfuel3,oxygensensorcurrentfuel4=@oxygensensorcurrentfuel4,oxygensensorcurrentfuel5=@oxygensensorcurrentfuel5," +
                            " oxygensensorcurrentfuel6=@oxygensensorcurrentfuel6,oxygensensorcurrentfuel7=@oxygensensorcurrentfuel7,oxygensensorcurrentfuel8=@oxygensensorcurrentfuel8,catalysttempbank1sensor1=@catalysttempbank1sensor1,catalysttempbank2sensor1=@catalysttempbank2sensor1,catalysttempbank1sensor2=@catalysttempbank1sensor2,catalysttempbank2sensor2=@catalysttempbank2sensor2,pidsupported41to60=@pidsupported41to60,monitorstatusthisdrivecycle=@monitorstatusthisdrivecycle,controlmodulevoltage=@controlmodulevoltage,absoluteloadvalue=@absoluteloadvalue,fuelaircommandedequratio=@fuelaircommandedequratio,relativethrottleposition=@relativethrottleposition,ambientairtemp=@ambientairtemp,absthrottlepositionb=@absthrottlepositionb,absolutethrottlepositionc=@absolutethrottlepositionc,acceleratorpedalpositiond=@acceleratorpedalpositiond,acceleratorpedalpositione=@acceleratorpedalpositione,acceleratorpedalpositionf=@acceleratorpedalpositionf,commandedthrottleactuator=@commandedthrottleactuator,timerunwithmil=@timerunwithmil,timesincetroublecodescleared=@timesincetroublecodescleared,maximumvalueforfuelair=@maximumvalueforfuelair,maximumvalueforairflow=@maximumvalueforairflow,fueltype=@fueltype,ethanolfuel=@ethanolfuel,absoluteevapsystemvaporpressure=@absoluteevapsystemvaporpressure," +
                            " evapsystemvaporpressure=@evapsystemvaporpressure,shorttermsecoxygenbank1bank3=@shorttermsecoxygenbank1bank3,longtermsecoxygenbank1bank3=@longtermsecoxygenbank1bank3,shorttermsecoxygenbank2bank4=@shorttermsecoxygenbank2bank4,longtermsecoxygenbank2bank4=@longtermsecoxygenbank2bank4,fuelrailabsolutepressure=@fuelrailabsolutepressure,relativeacceleratorpedalposition=@relativeacceleratorpedalposition,hybridbatterypackremaininglife=@hybridbatterypackremaininglife,engineoiltemperature=@engineoiltemperature,fuelinjectiontiming=@fuelinjectiontiming,fuelrate=@fuelrate,emissionrequirements=@emissionrequirements,pidsupported61to80=@pidsupported61to80,driversdemandengine=@driversdemandengine,actualenginepercenttorque=@actualenginepercenttorque,enginereferencetorque=@enginereferencetorque,enginepercenttorquedata=@enginepercenttorquedata,auxiliaryinputoutputsupported=@auxiliaryinputoutputsupported,massairflowsensor=@massairflowsensor,enginecoolanttemperature=@enginecoolanttemperature,intakeairtemperaturesensor=@intakeairtemperaturesensor,commandedegrandegrerror=@commandedegrandegrerror,commandeddieselintakeairflow=@commandeddieselintakeairflow,exhaustgasrecirculationtemperature=@exhaustgasrecirculationtemperature,commandedthrottleactuatorcontrol=@commandedthrottleactuatorcontrol," +
                            " fuelpressurecontrolsystem=@fuelpressurecontrolsystem,injectionpressurecontrolsystem=@injectionpressurecontrolsystem,turbochargercompressorinletpressure=@turbochargercompressorinletpressure,boostpressurecontrol=@boostpressurecontrol,variablegeometryturbocontrol=@variablegeometryturbocontrol,wastegatecontrol=@wastegatecontrol,exhaustpressure=@exhaustpressure,turbochargerrpm=@turbochargerrpm,turbochargertemperature1=@turbochargertemperature1,turbochargertemperature2=@turbochargertemperature2,chargeaircoolertemperature=@chargeaircoolertemperature,exhaustgastemperaturebank1=@exhaustgastemperaturebank1,exhaustgastemperaturebank2=@exhaustgastemperaturebank2,dieselparticulatefilter1=@dieselparticulatefilter1,dieselparticulatefilter2=@dieselparticulatefilter2,dieselparticulatefiltertemperature=@dieselparticulatefiltertemperature,noxntecontrolareastatus=@noxntecontrolareastatus,pmntecontrolareastatus=@pmntecontrolareastatus,engineruntime=@engineruntime,pidsupported81toa0=@pidsupported81toa0,engineruntimeforaecd1=@engineruntimeforaecd1,engineruntimeforaecd2=@engineruntimeforaecd2,noxsensor=@noxsensor,manifoldsurfacetemperature=@manifoldsurfacetemperature,noxreagentsystem=@noxreagentsystem,particulatemattersensor=@particulatemattersensor,intakemanifoldabsolutepressure2=@intakemanifoldabsolutepressure2" +
                            " WHERE deviceno = @DeviceNo";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", DeviceNo));
                        cmd.Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(RecordDateTime)));
                        cmd.Parameters.Add(new NpgsqlParameter("@latitude", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@longitude", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@mobileno", DBNull.Value));
                        cmd.Parameters.Add(new NpgsqlParameter("@packettype", packettype));
                        cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", SequenceNmbr));
                        cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
                        cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
                        cmd.Parameters.Add(new NpgsqlParameter("@dtcdetected", DBNull.Value));

                        for (int i = 0; i < arryOBDData.GetLength(0); i++)
                        {
                            if (arryOBDData[i, 1] == "")
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", DBNull.Value));
                            }
                            else
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@" + arryOBDData[i, 0] + "", arryOBDData[i, 1]));
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        #endregion
                    }

                    tran.Commit();
                    connection.Close();
                }

                functionReturnValue = true;


            }
            catch (Exception ex)
            {
               // TM_SaveLogToFile("IOTMob_JSON_TM_v1_DBSaveError", ex.ToString(), ConfigurationManager.AppSettings["flgDBError"].ToString(), false);
                ErrorLog.SaveLogToDatabase(DeviceNo, "D-dataobj-OBD-DBSaveError", ex.ToString(), strjson);
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        //public bool SaveOBDDataToDatabase(long DeviceNo, string mil, string fuels, string egl, string engtemp, string rpm, string spd, string airtemp, string maf, string sensep, string prot, string rtes, string dtmilon, string prs, string cmodv, DateTime RecordDateTime, string SequenceNmbr)
        //{

        //    bool functionReturnValue = false;

        //    try
        //    {

        //        using (NpgsqlConnection connection = new NpgsqlConnection())
        //        {
        //            connection.ConnectionString = ConfigurationManager.AppSettings["ConStr"];
        //            connection.Open();

        //            NpgsqlTransaction tran = connection.BeginTransaction();
        //            NpgsqlCommand cmd = new NpgsqlCommand();

        //            cmd.Connection = connection;
        //            cmd.Transaction = tran;


        //            #region insert data in iot_obd_hub
        //            cmd = new NpgsqlCommand();

        //            cmd.Connection = connection;
        //            cmd.Transaction = tran;

        //            cmd.CommandText = " INSERT INTO iot_obd_hub(deviceno,recorddatetime,coolanttemperature,rpm,speedcan,throttlepos,runtimesinceenginestart,fuellevel,calculatedengineload,mafairflowrate,barometricpressure, " +
        //            " shorttermfueltrim,intakemanifoldabsolutepressure,timingadvance,intakeairtemp,controlmodulevoltage,absoluteloadvalue,sequencenumber,sysdatetime,isactive," +
        //            " malfunctionindicatorlamp,longtermfueltrim,oxygensensorspresent,oxygensensorvoltage1,oxygensensorvoltage2,obdstandards,distancetraveledMIL,commandedevaporativepurge," +
        //            " warmupssc,distancetraveledscc,equivalenceratiocurrent,fuelaircommandedequratio,relativethrottleposition,absthrottlepositionb,acceleratorpedalpositiond,acceleratorpedalpositione,commandedthrottleactuator) " +
        //            " VALUES(@deviceno,@recorddatetime,@coolanttemperature,@rpm,@speedcan,@throttlepos,@runtimesinceenginestart,@fuellevel,@calculatedengineload,@mafairflowrate,@barometricpressure, " +
        //            " @shorttermfueltrim,@intakemanifoldabsolutepressure,@timingadvance,@intakeairtemp,@controlmodulevoltage,@absoluteloadvalue,@sequencenumber,@sysdatetime,@isactive," +
        //            " @malfunctionindicatorlamp,@longtermfueltrim,@oxygensensorspresent,@oxygensensorvoltage1,@oxygensensorvoltage2,@obdstandards,@distancetraveledMIL,@commandedevaporativepurge," +
        //            " @warmupssc,@distancetraveledscc,@equivalenceratiocurrent,@fuelaircommandedequratio,@relativethrottleposition,@absthrottlepositionb,@acceleratorpedalpositiond,@acceleratorpedalpositione,@commandedthrottleactuator)";

        //            cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
        //            cmd.Parameters.Add(new NpgsqlParameter("@RecordDateTime", Convert.ToDateTime(RecordDateTime)));

        //            cmd.Parameters.Add(new NpgsqlParameter("@coolanttemperature", Convert.ToDecimal(engtemp)));
        //            cmd.Parameters.Add(new NpgsqlParameter("@rpm", Convert.ToDecimal(rpm)));
        //            cmd.Parameters.Add(new NpgsqlParameter("@speedcan", Convert.ToDecimal(spd)));
        //            cmd.Parameters.Add(new NpgsqlParameter("@throttlepos", DBNull.Value)); //throtpos NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@runtimesinceenginestart", rtes));
        //            cmd.Parameters.Add(new NpgsqlParameter("@fuellevel", fuels)); 
        //            cmd.Parameters.Add(new NpgsqlParameter("@calculatedengineload", egl));
        //            cmd.Parameters.Add(new NpgsqlParameter("@mafairflowrate", maf));
        //            cmd.Parameters.Add(new NpgsqlParameter("@barometricpressure", prs));
        //            cmd.Parameters.Add(new NpgsqlParameter("@shorttermfueltrim", DBNull.Value)); //shtmfl NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@intakemanifoldabsolutepressure", DBNull.Value)); //abp NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@timingadvance", DBNull.Value)); //timeadv NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@intakeairtemp", airtemp));
        //            cmd.Parameters.Add(new NpgsqlParameter("@controlmodulevoltage", cmodv));
        //            cmd.Parameters.Add(new NpgsqlParameter("@absoluteloadvalue", DBNull.Value)); //absload NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", Convert.ToInt64(SequenceNmbr)));
        //            cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
        //            cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
        //            cmd.Parameters.Add(new NpgsqlParameter("@malfunctionindicatorlamp", mil));
        //            cmd.Parameters.Add(new NpgsqlParameter("@longtermfueltrim", DBNull.Value)); //lngtmfl NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorspresent", sensep));
        //            cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage1", DBNull.Value)); //sensev1 NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage2", DBNull.Value)); //sensv2 NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@obdstandards", prot));
        //            cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledMIL", dtmilon));
        //            cmd.Parameters.Add(new NpgsqlParameter("@commandedevaporativepurge", DBNull.Value)); //comvp NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@warmupssc", DBNull.Value)); //wpscc NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledscc", DBNull.Value)); //dtscc NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@equivalenceratiocurrent", DBNull.Value)); //ratio NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@fuelaircommandedequratio", DBNull.Value)); //cer NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@relativethrottleposition", DBNull.Value)); //rtp NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@absthrottlepositionb", DBNull.Value)); //atpb NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositiond", DBNull.Value)); //appd NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositione", DBNull.Value)); //appe NOT RECIVED
        //            cmd.Parameters.Add(new NpgsqlParameter("@commandedthrottleactuator", DBNull.Value)); //cta NOT RECIVED

        //            cmd.ExecuteNonQuery();
        //            cmd.Dispose();
        //            #endregion

        //            #region check  data avaliable in iot_obd_live
        //            long OBDCount;
        //            cmd = new NpgsqlCommand();
        //            cmd.Connection = connection;
        //            cmd.Transaction = tran;
        //            cmd.CommandText = "SELECT count(*) FROM iot_obd_live WHERE deviceno = @DeviceNo";
        //            cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
        //            OBDCount = Convert.ToInt32(cmd.ExecuteScalar());
        //            cmd.Dispose();
        //            //----------------------------------
        //            #endregion

        //            if (OBDCount == 0)
        //            {

        //                #region insert data in iot_obd_live
        //                cmd = new NpgsqlCommand();

        //                cmd.Connection = connection;
        //                cmd.Transaction = tran;

        //                cmd.CommandText = " INSERT INTO iot_obd_live(deviceno,recorddatetime,coolanttemperature,rpm,speedcan,throttlepos,runtimesinceenginestart,fuellevel,calculatedengineload,mafairflowrate,barometricpressure, " +
        //                  " shorttermfueltrim,intakemanifoldabsolutepressure,timingadvance,intakeairtemp,controlmodulevoltage,absoluteloadvalue,sequencenumber,sysdatetime,isactive," +
        //                  " malfunctionindicatorlamp,longtermfueltrim,oxygensensorspresent,oxygensensorvoltage1,oxygensensorvoltage2,obdstandards,distancetraveledMIL,commandedevaporativepurge," +
        //                  " warmupssc,distancetraveledscc,equivalenceratiocurrent,fuelaircommandedequratio,relativethrottleposition,absthrottlepositionb,acceleratorpedalpositiond,acceleratorpedalpositione,commandedthrottleactuator) " +
        //                  " VALUES(@deviceno,@recorddatetime,@coolanttemperature,@rpm,@speedcan,@throttlepos,@runtimesinceenginestart,@fuellevel,@calculatedengineload,@mafairflowrate,@barometricpressure, " +
        //                  " @shorttermfueltrim,@intakemanifoldabsolutepressure,@timingadvance,@intakeairtemp,@controlmodulevoltage,@absoluteloadvalue,@sequencenumber,@sysdatetime,@isactive," +
        //                  " @malfunctionindicatorlamp,@longtermfueltrim,@oxygensensorspresent,@oxygensensorvoltage1,@oxygensensorvoltage2,@obdstandards,@distancetraveledMIL,@commandedevaporativepurge," +
        //                  " @warmupssc,@distancetraveledscc,@equivalenceratiocurrent,@fuelaircommandedequratio,@relativethrottleposition,@absthrottlepositionb,@acceleratorpedalpositiond,@acceleratorpedalpositione,@commandedthrottleactuator)";

        //                cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
        //                cmd.Parameters.Add(new NpgsqlParameter("@RecordDateTime", Convert.ToDateTime(RecordDateTime)));

        //                cmd.Parameters.Add(new NpgsqlParameter("@coolanttemperature", Convert.ToDecimal(engtemp)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@rpm", Convert.ToDecimal(rpm)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@speedcan", Convert.ToDecimal(spd)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@throttlepos", DBNull.Value)); //throtpos NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@runtimesinceenginestart", rtes));
        //                cmd.Parameters.Add(new NpgsqlParameter("@fuellevel", DBNull.Value)); //fuel NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@calculatedengineload", egl));
        //                cmd.Parameters.Add(new NpgsqlParameter("@mafairflowrate", maf));
        //                cmd.Parameters.Add(new NpgsqlParameter("@barometricpressure", prs));
        //                cmd.Parameters.Add(new NpgsqlParameter("@shorttermfueltrim", DBNull.Value)); //shtmfl NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@intakemanifoldabsolutepressure", DBNull.Value)); //abp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@timingadvance", DBNull.Value)); //timeadv NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@intakeairtemp", airtemp));
        //                cmd.Parameters.Add(new NpgsqlParameter("@controlmodulevoltage", cmodv));
        //                cmd.Parameters.Add(new NpgsqlParameter("@absoluteloadvalue", DBNull.Value)); //absload NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", Convert.ToInt64(SequenceNmbr)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
        //                cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
        //                cmd.Parameters.Add(new NpgsqlParameter("@malfunctionindicatorlamp", mil));
        //                cmd.Parameters.Add(new NpgsqlParameter("@longtermfueltrim", DBNull.Value)); //lngtmfl NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorspresent", sensep));
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage1", DBNull.Value)); //sensev1 NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage2", DBNull.Value)); //sensv2 NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@obdstandards", prot));
        //                cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledMIL", dtmilon));
        //                cmd.Parameters.Add(new NpgsqlParameter("@commandedevaporativepurge", DBNull.Value)); //comvp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@warmupssc", DBNull.Value)); //wpscc NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledscc", DBNull.Value)); //dtscc NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@equivalenceratiocurrent", DBNull.Value)); //ratio NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@fuelaircommandedequratio", DBNull.Value)); //cer NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@relativethrottleposition", DBNull.Value)); //rtp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@absthrottlepositionb", DBNull.Value)); //atpb NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositiond", DBNull.Value)); //appd NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositione", DBNull.Value)); //appe NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@commandedthrottleactuator", DBNull.Value)); //cta NOT RECIVED


        //                cmd.ExecuteNonQuery();
        //                cmd.Dispose();
        //                #endregion
        //            }
        //            else if (OBDCount > 0)
        //            {

        //                #region update data in iot_obd_live
        //                cmd = new NpgsqlCommand();

        //                cmd.Connection = connection;
        //                cmd.Transaction = tran;

        //                cmd.CommandText = " UPDATE iot_obd_live SET recorddatetime=@recorddatetime,coolanttemperature=@coolanttemperature,rpm=@rpm,speedcan=@speedcan,throttlepos=@throttlepos,runtimesinceenginestart=@runtimesinceenginestart,fuellevel=@fuellevel,calculatedengineload=@calculatedengineload,mafairflowrate=@mafairflowrate,barometricpressure=@barometricpressure, " +
        //                  " shorttermfueltrim=@shorttermfueltrim,intakemanifoldabsolutepressure=@intakemanifoldabsolutepressure,timingadvance=@timingadvance,intakeairtemp=@intakeairtemp,controlmodulevoltage=@controlmodulevoltage,absoluteloadvalue=@absoluteloadvalue,sequencenumber=@sequencenumber,sysdatetime=@sysdatetime,isactive=@isactive," +
        //                  " malfunctionindicatorlamp=@malfunctionindicatorlamp,longtermfueltrim=@longtermfueltrim,oxygensensorspresent=@oxygensensorspresent,oxygensensorvoltage1=@oxygensensorvoltage1,oxygensensorvoltage2=@oxygensensorvoltage2,obdstandards=@obdstandards,distancetraveledMIL=@distancetraveledMIL,commandedevaporativepurge=@commandedevaporativepurge," +
        //                  " warmupssc=@warmupssc,distancetraveledscc=@distancetraveledscc,equivalenceratiocurrent=@equivalenceratiocurrent,fuelaircommandedequratio=@fuelaircommandedequratio,relativethrottleposition=@relativethrottleposition,absthrottlepositionb=@absthrottlepositionb,acceleratorpedalpositiond=@acceleratorpedalpositiond,acceleratorpedalpositione=@acceleratorpedalpositione,commandedthrottleactuator=@commandedthrottleactuator WHERE deviceno = @DeviceNo";

        //                cmd.Parameters.Add(new NpgsqlParameter("@DeviceNo", DeviceNo));
        //                cmd.Parameters.Add(new NpgsqlParameter("@RecordDateTime", Convert.ToDateTime(RecordDateTime)));

        //                cmd.Parameters.Add(new NpgsqlParameter("@coolanttemperature", Convert.ToDecimal(engtemp)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@rpm", Convert.ToDecimal(rpm)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@speedcan", Convert.ToDecimal(spd)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@throttlepos", DBNull.Value)); //throtpos NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@runtimesinceenginestart", rtes));
        //                cmd.Parameters.Add(new NpgsqlParameter("@fuellevel", fuels)); 
        //                cmd.Parameters.Add(new NpgsqlParameter("@calculatedengineload", egl));
        //                cmd.Parameters.Add(new NpgsqlParameter("@mafairflowrate", maf));
        //                cmd.Parameters.Add(new NpgsqlParameter("@barometricpressure", prs));
        //                cmd.Parameters.Add(new NpgsqlParameter("@shorttermfueltrim", DBNull.Value)); //shtmfl NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@intakemanifoldabsolutepressure", DBNull.Value)); //abp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@timingadvance", DBNull.Value)); //timeadv NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@intakeairtemp", airtemp));
        //                cmd.Parameters.Add(new NpgsqlParameter("@controlmodulevoltage", cmodv));
        //                cmd.Parameters.Add(new NpgsqlParameter("@absoluteloadvalue", DBNull.Value)); //absload NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@sequencenumber", Convert.ToInt64(SequenceNmbr)));
        //                cmd.Parameters.Add(new NpgsqlParameter("@sysdatetime", DateTime.Now));
        //                cmd.Parameters.Add(new NpgsqlParameter("@isactive", true));
        //                cmd.Parameters.Add(new NpgsqlParameter("@malfunctionindicatorlamp", mil));
        //                cmd.Parameters.Add(new NpgsqlParameter("@longtermfueltrim", DBNull.Value)); //lngtmfl NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorspresent", sensep));
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage1", DBNull.Value)); //sensev1 NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@oxygensensorvoltage2", DBNull.Value)); //sensv2 NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@obdstandards", prot));
        //                cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledMIL", dtmilon));
        //                cmd.Parameters.Add(new NpgsqlParameter("@commandedevaporativepurge", DBNull.Value)); //comvp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@warmupssc", DBNull.Value)); //wpscc NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@distancetraveledscc", DBNull.Value)); //dtscc NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@equivalenceratiocurrent", DBNull.Value)); //ratio NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@fuelaircommandedequratio", DBNull.Value)); //cer NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@relativethrottleposition", DBNull.Value)); //rtp NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@absthrottlepositionb", DBNull.Value)); //atpb NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositiond", DBNull.Value)); //appd NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@acceleratorpedalpositione", DBNull.Value)); //appe NOT RECIVED
        //                cmd.Parameters.Add(new NpgsqlParameter("@commandedthrottleactuator", DBNull.Value)); //cta NOT RECIVED

        //                cmd.ExecuteNonQuery();
        //                cmd.Dispose();
        //                #endregion
        //            }

        //            tran.Commit();
        //            connection.Close();
        //        }

        //        functionReturnValue = true;


        //    }
        //    catch (Exception ex)
        //    {
        //        TM_SaveLogToFile("IOT_JSON_TM_v1_DBSaveError", ex.ToString(), ConfigurationManager.AppSettings["flgDBError"].ToString(), false);
        //        functionReturnValue = false;
        //    }
        //    return functionReturnValue;
        //}

        #endregion

        //private void TM_SaveLogToFile(string filesname, string logdata, string saveFlag, bool writeline = true)
        //{
        //    try
        //    {
        //        if (saveFlag == "1")
        //        {
        //            string filePath = ConfigurationManager.AppSettings["FileLogPath"].ToString();

        //            StreamWriter fs = default(StreamWriter);
        //            string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        //            if (File.Exists(filename))
        //            {
        //                fs = new StreamWriter(filename, true);
        //            }
        //            else
        //            {
        //                fs = new StreamWriter(filename);
        //            }
        //            fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + " > " + logdata + System.Environment.NewLine);
        //            if (writeline)
        //            {
        //                fs.Write("==================================================================================" + System.Environment.NewLine);
        //            }
        //            fs.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private DateTime ConvertToDatetimeFormat(string strdate, string strtime)
        {

            if (strdate.Length == 5)
            {
                strdate = "0" + strdate;
            }
            if (strtime.Length == 5)
            {
                strtime = "0" + strtime;
            }

            string DatePart = string.Empty;
            string TimePart;
            DateTime ActualDateTime;

            if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "DD/MM/YYYY" | CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "DD-MM-YYYY")
            {
                DatePart = strdate.Substring(0, 2) + "/" + strdate.Substring(2, 2) + "/" + strdate.Substring(4, 2);
            }
            else if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "MM/DD/YYYY" | CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "MM-DD-YYYY")
            {
                DatePart = strdate.Substring(2, 2) + "/" + strdate.Substring(0, 2) + "/" + strdate.Substring(4, 2);
            }
            else if (CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpper() == "M/D/YYYY")
            {
                DatePart = strdate.Substring(2, 2) + "/" + strdate.Substring(0, 2) + "/" + strdate.Substring(4, 2);
            }

            TimePart = strtime.Substring(0, 2) + ":" + strtime.Substring(2, 2) + ":" + strtime.Substring(4, 2);
            ActualDateTime = Convert.ToDateTime(DatePart + " " + TimePart);
            //ActualDateTime = ActualDateTime.AddMinutes(330) 'GMT + 5:30 Hrs
            return ActualDateTime;

        }

        string[,] arrOBDData = new string[,]
        {
            {"pidsupport1to20", ""},
            {"statusdtccleared", ""},
            {"freezedtc", ""},
            {"fuelsystemstatus", ""},
            {"calculatedengineload", ""},
            {"coolanttemperature", ""},
            {"shorttermfueltrim", ""},
            {"longtermfueltrim", ""},
            {"shorttermfueltrimbank2", ""},
            {"longtermfueltrimbank2", ""},
            {"fuelpressure", ""},
            {"intakemanifoldabsolutepressure", ""},
            {"rpm", ""},
            {"speedcan", ""},
            {"timingadvance", ""},
            {"intakeairtemp", ""},
            {"mafairflowrate", ""},
            {"throttlepos", ""},
            {"commandedsecondaryairstatus", ""},
            {"oxygensensorspresent", ""},
            {"oxygensensorvoltage1", ""},
            {"oxygensensorvoltage2", ""},
            {"oxygensensorvoltage3", ""},
            {"oxygensensorvoltage4", ""},
            {"oxygensensorvoltage5", ""},
            {"oxygensensorvoltage6", ""},
            {"oxygensensorvoltage7", ""},
            {"oxygensensorvoltage8", ""},
            {"obdstandards", ""},
            {"oxygensensorspresent4banks", ""},
            {"auxiliaryinputstatus", ""},
            {"runtimesinceenginestart", ""},
            {"pidsupported21to40", ""},
            {"distancetraveledmil", ""},
            {"relativefuelrailpressure", ""},
            {"directfuelrailpressure", ""},
            {"oxygensensorvoltagefuel1", ""},
            {"oxygensensorvoltagefuel2", ""},
            {"oxygensensorvoltagefuel3", ""},
            {"oxygensensorvoltagefuel4", ""},
            {"oxygensensorvoltagefuel5", ""},
            {"oxygensensorvoltagefuel6", ""},
            {"oxygensensorvoltagefuel7", ""},
            {"oxygensensorvoltagefuel8", ""},
            {"commandedegr", ""},
            {"egrerror", ""},
            {"commandedevaporativepurge", ""},
            {"fuellevel", ""},
            {"warmupssc", ""},
            {"distsincecodecleared", ""},
            {"systemvaporpressure", ""},
            {"barometricpressure", ""},
            {"oxygensensorcurrentfuel1", ""},
            {"oxygensensorcurrentfuel2", ""},
            {"oxygensensorcurrentfuel3", ""},
            {"oxygensensorcurrentfuel4", ""},
            {"oxygensensorcurrentfuel5", ""},
            {"oxygensensorcurrentfuel6", ""},
            {"oxygensensorcurrentfuel7", ""},
            {"oxygensensorcurrentfuel8", ""},
            {"catalysttempbank1sensor1", ""},
            {"catalysttempbank2sensor1", ""},
            {"catalysttempbank1sensor2", ""},
            {"catalysttempbank2sensor2", ""},
            {"pidsupported41to60", ""},
            {"monitorstatusthisdrivecycle", ""},
            {"controlmodulevoltage", ""},
            {"absoluteloadvalue", ""},
            {"fuelaircommandedequratio", ""},
            {"relativethrottleposition", ""},
            {"ambientairtemp", ""},
            {"absthrottlepositionb", ""},
            {"absolutethrottlepositionc", ""},
            {"acceleratorpedalpositiond", ""},
            {"acceleratorpedalpositione", ""},
            {"acceleratorpedalpositionf", ""},
            {"commandedthrottleactuator", ""},
            {"timerunwithmil", ""},
            {"timesincetroublecodescleared", ""},
            {"maximumvalueforfuelair", ""},
            {"maximumvalueforairflow", ""},
            {"fueltype", ""},
            {"ethanolfuel", ""},
            {"absoluteevapsystemvaporpressure", ""},
            {"evapsystemvaporpressure", ""},
            {"shorttermsecoxygenbank1bank3", ""},
            {"longtermsecoxygenbank1bank3", ""},
            {"shorttermsecoxygenbank2bank4", ""},
            {"longtermsecoxygenbank2bank4", ""},
            {"fuelrailabsolutepressure", ""},
            {"relativeacceleratorpedalposition", ""},
            {"hybridbatterypackremaininglife", ""},
            {"engineoiltemperature", ""},
            {"fuelinjectiontiming", ""},
            {"fuelrate", ""},
            {"emissionrequirements", ""},
            {"pidsupported61to80", ""},
            {"driversdemandengine", ""},
            {"actualenginepercenttorque", ""},
            {"enginereferencetorque", ""},
            {"enginepercenttorquedata", ""},
            {"auxiliaryinputoutputsupported", ""},
            {"massairflowsensor", ""},
            {"enginecoolanttemperature", ""},
            {"intakeairtemperaturesensor", ""},
            {"commandedegrandegrerror", ""},
            {"commandeddieselintakeairflow", ""},
            {"exhaustgasrecirculationtemperature", ""},
            {"commandedthrottleactuatorcontrol", ""},
            {"fuelpressurecontrolsystem", ""},
            {"injectionpressurecontrolsystem", ""},
            {"turbochargercompressorinletpressure", ""},
            {"boostpressurecontrol", ""},
            {"variablegeometryturbocontrol", ""},
            {"wastegatecontrol", ""},
            {"exhaustpressure", ""},
            {"turbochargerrpm", ""},
            {"turbochargertemperature1", ""},
            {"turbochargertemperature2", ""},
            {"chargeaircoolertemperature", ""},
            {"exhaustgastemperaturebank1", ""},
            {"exhaustgastemperaturebank2", ""},
            {"dieselparticulatefilter1", ""},
            {"dieselparticulatefilter2", ""},
            {"dieselparticulatefiltertemperature", ""},
            {"noxntecontrolareastatus", ""},
            {"pmntecontrolareastatus", ""},
            {"engineruntime", ""},
            {"pidsupported81toa0", ""},
            {"engineruntimeforaecd1", ""},
            {"engineruntimeforaecd2", ""},
            {"noxsensor", ""},
            {"manifoldsurfacetemperature", ""},
            {"noxreagentsystem", ""},
            {"particulatemattersensor", ""},
            {"intakemanifoldabsolutepressure2", ""}
        };

    }

}