﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using Npgsql;
using System.Diagnostics;

namespace IOTServiceAPI.Models
{
    public class ErrorLog
    {

        public static void SaveLogToDatabase(Int64 deviceno, string logtype, string error, string logdata)
        {
            try
            {
                string saveFlag = System.Configuration.ConfigurationManager.AppSettings["flgDBLog"].ToString();
                if (saveFlag == "1")
                {
                    #region log into database
                    using (NpgsqlConnection connection = new NpgsqlConnection())
                    {
                        connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConStrlog"];
                        connection.Open();
                        NpgsqlCommand cmd = new NpgsqlCommand();
                        cmd.Connection = connection;

                        cmd.CommandText = " INSERT INTO error_data_log(deviceno, logtype, error, logdata, created_at) VALUES (@deviceno, @logtype, @error, @logdata, @created_at)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", deviceno));
                        cmd.Parameters.Add(new NpgsqlParameter("@logtype", logtype));
                        cmd.Parameters.Add(new NpgsqlParameter("@error", error));
                        cmd.Parameters.Add(new NpgsqlParameter("@logdata", logdata));
                        cmd.Parameters.Add(new NpgsqlParameter("@created_at", Convert.ToDateTime(DateTime.Now)));
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        connection.Close();
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {
                LogToFile(ex, "SaveLogToDatabase");
            }

        }


        public static void LogToFile(Exception logdata, string source)
        {

            try
            {
                #region write log into file
                string filesname = "IOT_JSON_TM_v1_APIUnhandleErrors";
                string filePath = System.Configuration.ConfigurationManager.AppSettings["FileLogPath"].ToString();

                StreamWriter fs = default(StreamWriter);
                string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

                if (File.Exists(filename))
                {
                    fs = new StreamWriter(filename, true);
                }
                else
                {
                    fs = new StreamWriter(filename);
                }
                fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss.FFF") + "> " + source + System.Environment.NewLine + logdata.ToString() + System.Environment.NewLine + System.Environment.NewLine);
                fs.Write("==================================================================================" + System.Environment.NewLine);
                fs.Close();
                #endregion
            }
            catch (Exception ex)
            {
                try
                {
                    #region log write inot windows event
                    System.Diagnostics.EventLog.WriteEntry("IOTAPI", ex.ToString(), EventLogEntryType.Error);
                    #endregion
                }
                catch (Exception innerex)
                {
                }
            }
        }


    }
}

