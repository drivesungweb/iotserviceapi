﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using IOTDataEntity;
using IOTRawDataParsing;

namespace IOTAPIForUpload.Controllers
{
    public class ServerMonitoringController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Test()
        {
            return Ok(IotZenSDKRawDataParsing.getOkResponse());
        }

        [HttpPost]
        public IHttpActionResult GetServerHealth(ServerMonitoringEntity Req)
        {
            using (PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total"))
            {
                dynamic firstValue = cpuCounter.NextValue();
                System.Threading.Thread.Sleep(1000);
                // now matches task manager reading
                Req.cpu_utilization = Math.Round(cpuCounter.NextValue());
            }

            using (PerformanceCounter RamCounter = new PerformanceCounter("Memory", "Available MBytes"))
            {
                Req.memory_avaliable = RamCounter.NextValue();
            }

            return Ok(IotZenSDKRawDataParsing.getOkResponse(Req));
        }

    }
}