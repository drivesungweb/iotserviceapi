﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTService.Models;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using IOTServiceAPI.Filters;
using System.Collections;
using IOTServiceAPI.Models;
using IOTRawDataParsing;
using IOTDataEntity;
using System.Data;

//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTService.Controllers
{
    [CustomExceptionFilter]
    public class IOTMobController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> IOTMobData(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "L-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                IOTMobDataParse objIOT = new IOTMobDataParse();
                await objIOT.TM_IOTMobData(jsonObject, jsonString);

                jsonString = null;
                jsonObject = null;
                objIOT = null;

                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            else
            {
                return BadRequest("null data");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> IOTMobDataH(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "H-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                JArray data = jsonObject.@data;
                foreach (JObject items in jsonObject.data)
                {
                    dynamic jsonChildObject = items;
                    string jsonchildString = Newtonsoft.Json.JsonConvert.SerializeObject(jsonChildObject);
                    IOTMobDataParse objIOT = new IOTMobDataParse();
                    await objIOT.TM_IOTMobData(jsonChildObject, jsonchildString);
                    objIOT = null;
                }
                jsonString = null;
                jsonObject = null;

                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            else
            {
                return BadRequest("null data");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> IOTTextData(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "IOTTextData-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);
                string[] rowDataPackets = jsonObject.data.ToString().Split(new string[] { "$$" }, StringSplitOptions.None); //seperate recived packtes by $$

                foreach (string rowDataPacket in rowDataPackets)
                {

                    if (rowDataPacket.Length > 0)
                    {
                        IOTMobDataParse objIOT = new IOTMobDataParse();
                        await objIOT.Drivesung_IOTTextData(rowDataPacket);
                        objIOT = null;
                    }

                }
                jsonString = null;
                jsonObject = null;
                rowDataPackets = null;

                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);


            }
            else
            {
                return BadRequest("null data");
            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> IOTTextData2Sec(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "IOTTextData2Sec-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);
                string[] rowDataPackets = jsonObject.data.ToString().Split(new string[] { "$$" }, StringSplitOptions.None); //seperate recived packtes by $$

                foreach (string rowDataPacket in rowDataPackets)
                {

                    if (rowDataPacket.Length > 0)
                    {
                        IOTMobDataParse objIOT = new IOTMobDataParse();
                        await objIOT.Drivesung_IOTTextDatas2Sec(rowDataPacket);
                        objIOT = null;
                    }

                }
                jsonString = null;
                jsonObject = null;
                rowDataPackets = null;

                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);


            }
            else
            {
                return BadRequest("null data");
            }
        }

        //[HttpPost]
        //public async Task<IHttpActionResult> IOTTextData2Sec(object obj)
        //{
        //    #region authentication
        //    if (!CommonUtils.VerifyKeyAuthentication())
        //        return BadRequest(CommonUtils.AuthorizationMessage());
        //    #endregion

        //    if (obj != null)
        //    {
        //        ErrorLog.SaveLogToDatabase(0, "IOTTextData2Sec-Rec", "", obj.ToString());
        //        string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        //        dynamic jsonObject = JObject.Parse(jsonString);
        //        string[] rowDataPackets = jsonObject.data.ToString().Split(new string[] { "$$" }, StringSplitOptions.None); //seperate recived packtes by $$
        //        string prvrowDataPacket = "";
        //        foreach (string rowDataPacket in rowDataPackets)
        //        {

        //            if (rowDataPacket.Length > 0)
        //            {
        //                if (prvrowDataPacket != rowDataPacket)
        //                {
        //                    IOTMobDataParse objIOT = new IOTMobDataParse();
        //                    await objIOT.Drivesung_IOTTextDatas2Sec(rowDataPacket);
        //                    objIOT = null;
        //                }
        //                prvrowDataPacket = rowDataPacket;
        //            }

        //        }
        //        jsonString = null;
        //        jsonObject = null;
        //        rowDataPackets = null;

        //        var resmsg = new responsemsg { code = 200, msg = "ok" };
        //        return Ok(resmsg);


        //    }
        //    else
        //    {
        //        return BadRequest("null data");
        //    }
        //}

        [HttpPost]
        public async Task<IHttpActionResult> IOTMobDataHRawData(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "HRawData-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                JArray data = jsonObject.@data;
                foreach (JObject items in jsonObject.data)
                {
                    dynamic jsonChildObject = items;
                    string jsonchildString = Newtonsoft.Json.JsonConvert.SerializeObject(jsonChildObject);
                    IOTMobDataParse objIOT = new IOTMobDataParse();
                    await objIOT.TM_IOTMobData(jsonChildObject, jsonchildString);
                    objIOT = null;
                }
                jsonString = null;
                jsonObject = null;

                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            else
            {
                return BadRequest("null data");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> IOTTextRawData(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj == null) return BadRequest("null data");

            IEnumerable<string> u_name = null;
            Request.Headers.TryGetValues("username", out u_name);
            string user_name = "";
            if (u_name != null) user_name = u_name.ToList()[0].ToString();


            IotRawDataErrorLog.SaveLogToDatabase(0, "IOTTextRawData-Rec", "", obj.ToString());
            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            dynamic jsonObject = JObject.Parse(jsonString);
            using (IotZenSDKRawDataParsing IotZenSDKRawDataParsing = new IotZenSDKRawDataParsing())
            {
                await IotZenSDKRawDataParsing.RawDataParsing(user_name, jsonObject.data.ToString());
            }
            jsonString = null;
            jsonObject = null;

            var resmsg = new responsemsg { code = 200, msg = "ok" };
            return Ok(resmsg);

        }

        [HttpPost]
        public async Task<IHttpActionResult> StartTripSDK(IotRawDataTripEntity tripEntity)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            using (IotZenSDKRawDataParsing IotZenSDKRawDataParsing = new IotZenSDKRawDataParsing())
            {
                string strResult = IotZenSDKRawDataParsing.ValidateTrip(tripEntity, user_name: true, trip_start_time: true);

                if (strResult != "")
                {
                    return BadRequest(strResult);
                }
                return Ok(IotZenSDKRawDataParsing.getOkResponse(IotZenSDKRawDataParsing.StartTripSDK(tripEntity)));
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> EndTripSDK(IotRawDataTripEntity tripEntity)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            using (IotZenSDKRawDataParsing IotZenSDKRawDataParsing = new IotZenSDKRawDataParsing())
            {
                string strResult = IotZenSDKRawDataParsing.ValidateTrip(tripEntity, trip_id: true, user_name: true, trip_start_time: true, trip_end_time: true);

                if (strResult != "")
                {
                    return BadRequest(strResult);
                }
                if (IotZenSDKRawDataParsing.EndTripSDK(tripEntity))
                {
                    return Ok(IotZenSDKRawDataParsing.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> TripEventLogSDK(IotRawDataTripEventLogEntity tripEventLogEntity)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            using (IotZenSDKRawDataParsing IotZenSDKRawDataParsing = new IotZenSDKRawDataParsing())
            {
                string strResult = IotZenSDKRawDataParsing.ValidateTripEventLog(tripEventLogEntity, trip_id: true, user_name: true, event_time: true);

                if (strResult != "")
                {
                    return BadRequest(strResult);
                }
                if (IotZenSDKRawDataParsing.TripEventLogSDK(tripEventLogEntity))
                {
                    return Ok(IotZenSDKRawDataParsing.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }


    }
}
