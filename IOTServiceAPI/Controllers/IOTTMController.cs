﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTService.Models;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using IOTServiceAPI.Filters;
using IOTServiceAPI.Models;
using IOTRawDataParsing;

//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTService.Controllers
{
    [CustomExceptionFilter]
    public class IOTTMController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> IOTData(object obj)
        {
            #region authentication
            if (!CommonUtils.VerifyKeyAuthentication())
                return BadRequest(CommonUtils.AuthorizationMessage());
            #endregion

            if (obj != null)
            {
                ErrorLog.SaveLogToDatabase(0, "L-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                IOTDataParse objIOT = new IOTDataParse();
                await objIOT.TM_IOTData(jsonObject, jsonString);

                objIOT = null;
                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            else
            {
                return BadRequest("null data");
            }
        }
    }

}
