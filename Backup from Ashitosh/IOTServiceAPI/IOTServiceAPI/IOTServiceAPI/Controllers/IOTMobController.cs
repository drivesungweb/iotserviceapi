﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTService.Models;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using IOTServiceAPI.Filters;
using System.Collections;
using IOTServiceAPI.Models;

//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTService.Controllers
{
    [CustomExceptionFilter]
    public class IOTMobController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> IOTMobData(object obj)
        {
            if (obj != null)
            {
                //TM_SaveLogToFile("IOTMob_JSON_TM_v1_datalogobj", obj.ToString(), ConfigurationManager.AppSettings["flgSaveLog"].ToString());
                ErrorLog.SaveLogToDatabase(0, "L-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                IOTMobDataParse objIOT = new IOTMobDataParse();
                await objIOT.TM_IOTMobData(jsonObject, jsonString);

                objIOT = null;
                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            {
                return BadRequest("null data");
            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> IOTMobDataH(object obj)
        {
            if (obj != null)
            {
               // TM_SaveLogToFile("IOTMob_JSON_TM_v1_datalogobjobj", obj.ToString(), ConfigurationManager.AppSettings["flgSaveLog"].ToString());
                ErrorLog.SaveLogToDatabase(0, "H-dataobj-Rec", "", obj.ToString());
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                dynamic jsonObject = JObject.Parse(jsonString);

                JArray data = jsonObject.@data;
                foreach (JObject items in jsonObject.data)
                {
                    dynamic jsonChildObject = items;
                    string jsonchildString = Newtonsoft.Json.JsonConvert.SerializeObject(jsonChildObject);
                    IOTMobDataParse objIOT = new IOTMobDataParse();
                    await objIOT.TM_IOTMobData(jsonChildObject, jsonchildString);
                    objIOT = null;
                }
                var resmsg = new responsemsg { code = 200, msg = "ok" };
                return Ok(resmsg);

            }
            {
                return BadRequest("null data");
            }
        }

        //private void TM_SaveLogToFile(string filesname, string logdata, string saveFlag, bool writeline = true)
        //{
        //    try
        //    {
        //        if (saveFlag == "1")
        //        {
        //            string filePath = ConfigurationManager.AppSettings["FileLogPath"].ToString();

        //            StreamWriter fs = default(StreamWriter);
        //            string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        //            if (File.Exists(filename))
        //            {
        //                fs = new StreamWriter(filename, true);
        //            }
        //            else
        //            {
        //                fs = new StreamWriter(filename);
        //            }
        //            fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + " > " + logdata + System.Environment.NewLine);
        //            if (writeline)
        //            {
        //                fs.Write("==================================================================================" + System.Environment.NewLine);
        //            }
        //            fs.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
