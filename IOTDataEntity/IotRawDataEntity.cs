﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTDataEntity
{
    public class IotRawDataEntity
    {
        public string RawDataPacket { get; set; }
        public Int64 r_id { get; set; }
        public Int64 seqno { get; set; }
        public string apk_version { get; set; }
        public int gps_valid { get; set; }
        public int history { get; set; }
        public string state { get; set; }
        public string recorddatetime { get; set; }
        public decimal heading { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public int ign { get; set; }
        public int is_self_driving { get; set; }
        public decimal speed { get; set; }
        public decimal distance { get; set; }
        public Int64 device_no { get; set; }
        public Int64 user_id { get; set; }
        public string hardware_id { get; set; }
        public string provtime { get; set; }
        public string prov { get; set; }
        public string ptype { get; set; }
        public decimal accuracy { get; set; }
        public string timezone { get; set; }
        public int hb { get; set; }
        public int ha { get; set; }
        public int os { get; set; }
        public int call { get; set; }
        public int bad_weather { get; set; }
        public int hc { get; set; }
        public string user_name { get; set; }
        public string pwd { get; set; }
        public int o_id { get; set; }

    }

    public class IotRawDataTripEntity
    {
        public int o_id { get; set; }
        public Int64 trip_id { get; set; }
        public string user_name { get; set; }
        public Int64 user_id { get; set; }
        public DateTime? trip_start_time { get; set; }
        public DateTime? trip_end_time { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public bool isactive { get; set; }
        public bool tripprocess { get; set; }
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string p3 { get; set; }
        public string p4 { get; set; }
        public string p5 { get; set; }
        public string p6 { get; set; }
        public string p7 { get; set; }
        public string p8 { get; set; }
        public string p9 { get; set; }
        public string p10 { get; set; }

    }

    public class ResponseEntity
    {
        public bool IsComplete { get; set; }
        public string Description { get; set; }
        public object data { get; set; }
        public object message { get; set; }
    }

    public class IotRawDataTripEventLogEntity
    {
        public Int64 trip_id { get; set; }
        public Int64 user_id { get; set; }
        public string user_name { get; set; }
        public DateTime? event_time { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string p3 { get; set; }
        public string p4 { get; set; }
        public string p5 { get; set; }
        public string p6 { get; set; }
        public string p7 { get; set; }
        public string p8 { get; set; }
        public string p9 { get; set; }
        public string p10 { get; set; }

    }
}



