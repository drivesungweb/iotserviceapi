﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTDataEntity
{
    public class ServerMonitoringEntity
    {
        public double cpu_utilization { get; set; }
        public double memory_avaliable { get; set; }

    }
}
