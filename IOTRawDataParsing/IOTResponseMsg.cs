﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;

namespace IOTRawDataParsing
{
    public enum MsgCode
    {
        [Description("trip_id required.")]
        trip_id = 162,
        [Description("empty trip entity not allowed.")]
        trip_entity = 209,
        [Description("trip_start_time required.")]
        trip_start_time = 165,
        [Description("trip_end_time required.")]
        trip_end_time = 166,
        [Description("user_id required.")]
        user_id = 116,
        [Description("user_name required.")]
        user_name = 117,

        [Description("empty trip event entity not allowed.")]
        trip_event_entity = 700,
        [Description("event_time required.")]
        event_time = 701,

        //common
        [Description("bad request")]
        bad_request = 400,
        [Description("ok")]
        ok = 200


    }

    public class MessageCode
    {

        public static string ErrorMsgInJson(object enumValue)
        {
            string defDesc = "";
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    defDesc = ((DescriptionAttribute)attrs[0]).Description;
            }

            // ErrorMsgInJson
            return getJsonMsg((int)enumValue, defDesc);
        }

        public static string getJsonMsg(int msgcode, string msg)
        {

            return @"{'code':'" + msgcode + "','desc':'" + msg + "'}";

            // return new JavaScriptSerializer().Serialize(new
            //{
            //    code = msgcode,
            //    desc = msg
            //});
        }

        public static string MsgInJson(object enumValue)
        {
            string defDesc = "";
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    defDesc = ((DescriptionAttribute)attrs[0]).Description;
            }

            return getJsonMsg((int)enumValue, defDesc);
        }

    }

}
