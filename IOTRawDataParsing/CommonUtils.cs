﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace IOTRawDataParsing
{
    public class CommonUtils
    {
        #region Conversion

        public static string ConvertToString(object data)
        {
            if (data == null || Convert.IsDBNull(data))
                return "";
            else
                return Convert.ToString(data);
        }

        public static Int32 ConvertToInt(object data)
        {
            Int32 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int32.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static Int32? ConvertToIntWithNull(object data)
        {
            Int32 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int32.TryParse(data.ToString(), out returnData))
                return null;
            else
                return returnData;
        }

        public static Int64 ConvertToInt64(object data)
        {
            Int64 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int64.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static Int64? ConvertToInt64WithNull(object data)
        {
            Int64 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int64.TryParse(data.ToString(), out returnData))
                return null;
            else
                return returnData;
        }

        public static double ConvertToDouble(object data)
        {
            double returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !double.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static decimal ConvertToDecimal(object data)
        {
            decimal returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !decimal.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static bool ConvertToBoolean(object data)
        {
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)))
                return false;
            else
                return Convert.ToBoolean(data);
        }

        public static DateTime? ConvertToDateTime(object data)
        {
            DateTime result = new DateTime();
            if (data == null || Convert.IsDBNull(data) || !DateTime.TryParse(data.ToString(), out result))
                return null;
            else
                return Convert.ToDateTime(data);
        }

        public static string ConvertDateFormatddmmyyyyhhmmss(string strdate)
        {

            string[] arrdate = strdate.Split(' ');
            string[] arrTime = arrdate[1].ToString().Split('-');
            //ddmmyyyyhhmmss
            // string newDate = strdate.Substring(4,4) + "-" + strdate.Substring(2,2) + "-" + strdate.Substring(0,2) + " " + strdate.Substring(8,2) + ":" + strdate.Substring(10,2) + ":" + strdate.Substring(12,2); 
            string newDate = arrdate[0].ToString() + " " + arrTime[0].ToString() + ":" + arrTime[1].ToString() + ":" + arrTime[2].ToString(); ;
            return newDate;

        }

        #endregion

        public static bool VerifyKeyAuthentication()
        {
            bool isValid = true;
            if (CommonUtils.ConvertToString(System.Configuration.ConfigurationManager.AppSettings["CustomKeyAuthentication"]) == "1")
            {
                if (GetApiSecretKey() == CommonUtils.ConvertToString(System.Configuration.ConfigurationManager.AppSettings["api_secret_key"])
                    && GetApiAccessId() == CommonUtils.ConvertToString(System.Configuration.ConfigurationManager.AppSettings["api_access_id"]))
                    isValid = true;
                else
                    isValid = false;
            }
            return isValid;
        }

        public static string GetApiSecretKey()
        {
            string api_secret_key = "";
            var headers = System.Web.HttpContext.Current.Request.Headers;
            if (headers.GetValues("api_secret_key") != null)
                api_secret_key = CommonUtils.ConvertToString(headers.GetValues("api_secret_key").First());
            return api_secret_key;
        }

        public static string GetApiAccessId()
        {
            string api_access_id = "";
            var headers = System.Web.HttpContext.Current.Request.Headers;
            if (headers.GetValues("api_access_id") != null)
                api_access_id = CommonUtils.ConvertToString(headers.GetValues("api_access_id").First());
            return api_access_id;
        }

        public static string AuthorizationMessage()
        {
            return "Authorization has been denied for this request.";
        }
    }
}