﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTDataEntity;
using System.Threading.Tasks;

namespace IOTRawDataParsing
{

    public class IotZenSDKRawDataParsing : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        public IotZenSDKRawDataParsing()
        {
            objDAL = new BaseDAL();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
                strSqlQry = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~IotZenSDKRawDataParsing()
        {
            Dispose(false);
        }

        #endregion

        public Task<bool> RawDataParsing(string user_name, string rawData)
        {
            try
            {
                string[] rawDataPackets = rawData.ToString().Split(new string[] { "$$" }, StringSplitOptions.None); //seperate recived packtes by $$

                objDAL.CreateConnection();
                Int64 user_id = 0;
                if (user_name != "") user_id = CommonUtils.ConvertToInt64(objDAL.FetchSingleRecord("select user_id from user_master where user_name='" + user_name.Trim() + "' limit 1"));

                foreach (string rawDataPacket in rawDataPackets)
                {
                    if (rawDataPacket.Length > 0)
                    {
                        IotRawDataEntity IotRawDataEntity = GetIotRawDataEntity(rawDataPacket);
                        if (IotRawDataEntity != null && user_id > 0)
                        {
                            objDAL.CreateConnection();
                            IotRawDataEntity.RawDataPacket = rawDataPacket;
                            // if user_name send as header parameter 
                            if (user_id > 0)
                            {
                                IotRawDataEntity.user_id = user_id;
                                IotRawDataEntity.device_no = user_id;
                                IotRawDataEntity.user_name = user_name;
                            }

                            SaveLocationRawData(IotRawDataEntity);
                        }

                    }

                }
                objDAL.CloseConnection();
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                IotRawDataErrorLog.SaveLogToDatabase(0, "IOTTextRawData-ParseError-RawDataParsing", ex.ToString(), rawData);
                return Task.FromResult(false);
            }
        }


        public IotRawDataEntity GetIotRawDataEntity(string rowDataPacket)
        {
            IotRawDataEntity IotRawDataEntity = new IotRawDataEntity();
            try
            {
                string[] rowDataArray = rowDataPacket.Split(new string[] { "," }, StringSplitOptions.None); //seperate recived packtes by comma

                #region variable declaration and packet value parsing
                IotRawDataEntity.seqno = Convert.ToInt64(rowDataArray[1]);
                IotRawDataEntity.apk_version = Convert.ToString(rowDataArray[2]);
                IotRawDataEntity.gps_valid = Convert.ToInt16(rowDataArray[3]);
                IotRawDataEntity.history = Convert.ToInt16(rowDataArray[4]);
                IotRawDataEntity.state = Convert.ToString(rowDataArray[5]);

                IotRawDataEntity.recorddatetime = CommonUtils.ConvertDateFormatddmmyyyyhhmmss(Convert.ToString(rowDataArray[6]));

                IotRawDataEntity.heading = Convert.ToDecimal(rowDataArray[7]);
                IotRawDataEntity.latitude = Convert.ToDecimal(rowDataArray[8]);
                IotRawDataEntity.longitude = Convert.ToDecimal(rowDataArray[9]);
                //IotRawDataEntity.ign = Convert.ToInt16(rowDataArray[10]);
                IotRawDataEntity.ign = 1;
                IotRawDataEntity.is_self_driving = Convert.ToInt16(rowDataArray[11]);
                IotRawDataEntity.speed = Convert.ToDecimal(rowDataArray[12]);
                IotRawDataEntity.distance = Convert.ToDecimal(rowDataArray[13]);
                IotRawDataEntity.device_no = CommonUtils.ConvertToInt64(rowDataArray[14]);
                IotRawDataEntity.user_id = CommonUtils.ConvertToInt64(rowDataArray[15]);  //new
                IotRawDataEntity.hardware_id = Convert.ToString(rowDataArray[16]);
                IotRawDataEntity.provtime = Convert.ToString(rowDataArray[17]);
                IotRawDataEntity.prov = Convert.ToString(rowDataArray[18]);
                IotRawDataEntity.ptype = Convert.ToString(rowDataArray[19]);
                decimal accuracy = 0;
                if (rowDataArray[20].ToString() != "null") accuracy = Convert.ToDecimal(rowDataArray[20]);  //new
                IotRawDataEntity.accuracy = accuracy;
                IotRawDataEntity.timezone = Convert.ToString(rowDataArray[21]); //new
                IotRawDataEntity.hb = Convert.ToInt16(rowDataArray[22]);
                IotRawDataEntity.ha = Convert.ToInt16(rowDataArray[23]);
                IotRawDataEntity.os = Convert.ToInt16(rowDataArray[24]);
                IotRawDataEntity.call = Convert.ToInt16(rowDataArray[25]);
                IotRawDataEntity.bad_weather = Convert.ToInt16(rowDataArray[26]);

                int intCrnrVal = Convert.ToInt16(rowDataArray[27]);
                int intCrnr = intCrnrVal;
                if (intCrnr != 1) intCrnr = 0;
                IotRawDataEntity.hc = intCrnr;
                IotRawDataEntity.user_name = "";
                IotRawDataEntity.pwd = "";
                IotRawDataEntity.o_id = 28;
                #endregion

                return IotRawDataEntity;
            }
            catch (Exception ex)
            {
                IotRawDataErrorLog.SaveLogToDatabase(0, "IOTTextRawData-ParseError-GetIotRawDataEntity", ex.ToString(), rowDataPacket);
                return IotRawDataEntity;
            }
            finally
            {
                if (IotRawDataEntity != null) IotRawDataEntity = null;
            }
        }

        public bool SaveLocationRawData(IotRawDataEntity IotRawDataEntity)
        {
            try
            {

                strSqlQry = @" INSERT INTO iot_hub_raw_data (seqno, apk_version, gps_valid, history, state, recorddatetime, heading, latitude, longitude, ign, is_self_driving, speed, distance, device_no, user_id, hardware_id, provtime, prov, ptype, accuracy, timezone, hb, ha, os, call, bad_weather, hc, user_name, pwd, o_id)
                             VALUES (@seqno, @apk_version, @gps_valid, @history, @state, @recorddatetime, @heading, @latitude, @longitude, @ign, @is_self_driving, @speed, @distance, @device_no, @user_id, @hardware_id, @provtime, @prov, @ptype, @accuracy, @timezone, @hb, @ha, @os, @call, @bad_weather, @hc, @user_name, @pwd, @o_id);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@seqno", IotRawDataEntity.seqno));
                Parameters.Add(new NpgsqlParameter("@apk_version", IotRawDataEntity.apk_version));
                Parameters.Add(new NpgsqlParameter("@gps_valid", IotRawDataEntity.gps_valid));
                Parameters.Add(new NpgsqlParameter("@history", IotRawDataEntity.history));
                Parameters.Add(new NpgsqlParameter("@state", IotRawDataEntity.state));
                Parameters.Add(new NpgsqlParameter("@recorddatetime", Convert.ToDateTime(IotRawDataEntity.recorddatetime)));
                Parameters.Add(new NpgsqlParameter("@heading", IotRawDataEntity.heading));
                Parameters.Add(new NpgsqlParameter("@latitude", IotRawDataEntity.latitude));
                Parameters.Add(new NpgsqlParameter("@longitude", IotRawDataEntity.longitude));
                Parameters.Add(new NpgsqlParameter("@ign", IotRawDataEntity.ign));
                Parameters.Add(new NpgsqlParameter("@is_self_driving", IotRawDataEntity.is_self_driving));
                Parameters.Add(new NpgsqlParameter("@speed", IotRawDataEntity.speed));
                Parameters.Add(new NpgsqlParameter("@distance", IotRawDataEntity.distance));
                Parameters.Add(new NpgsqlParameter("@device_no", IotRawDataEntity.device_no));
                Parameters.Add(new NpgsqlParameter("@user_id", IotRawDataEntity.user_id));
                Parameters.Add(new NpgsqlParameter("@hardware_id", IotRawDataEntity.hardware_id));
                Parameters.Add(new NpgsqlParameter("@provtime", IotRawDataEntity.provtime));
                Parameters.Add(new NpgsqlParameter("@prov", IotRawDataEntity.prov));
                Parameters.Add(new NpgsqlParameter("@ptype", IotRawDataEntity.ptype));
                Parameters.Add(new NpgsqlParameter("@accuracy", IotRawDataEntity.accuracy));
                Parameters.Add(new NpgsqlParameter("@timezone", IotRawDataEntity.timezone));
                Parameters.Add(new NpgsqlParameter("@hb", IotRawDataEntity.hb));
                Parameters.Add(new NpgsqlParameter("@ha", IotRawDataEntity.ha));
                Parameters.Add(new NpgsqlParameter("@os", IotRawDataEntity.os));
                Parameters.Add(new NpgsqlParameter("@call", IotRawDataEntity.call));
                Parameters.Add(new NpgsqlParameter("@bad_weather", IotRawDataEntity.bad_weather));
                Parameters.Add(new NpgsqlParameter("@hc", IotRawDataEntity.hc));
                Parameters.Add(new NpgsqlParameter("@user_name", IotRawDataEntity.user_name));
                Parameters.Add(new NpgsqlParameter("@pwd", IotRawDataEntity.pwd));
                Parameters.Add(new NpgsqlParameter("@o_id", IotRawDataEntity.o_id));

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                return true;

            }
            catch (Exception ex)
            {
                IotRawDataErrorLog.SaveLogToDatabase(IotRawDataEntity.device_no, "IOTTextRawData-DBSaveError", ex.ToString(), IotRawDataEntity.RawDataPacket);
                return false;
            }

        }

        public DataTable StartTripSDK(IotRawDataTripEntity tripEntity)
        {
            DataTable dtTrip = new DataTable();
            try
            {
                objDAL.CreateConnection();

                Int64 user_id = 0;
                if (tripEntity.user_name != "")
                {
                    tripEntity.user_id = CommonUtils.ConvertToInt64(objDAL.FetchSingleRecord("select user_id from user_master where user_name='" + tripEntity.user_name.Trim() + "' limit 1"));

                    strSqlQry = @"INSERT INTO trip_planning_master_sdk( user_id, user_name, trip_start_time, is_self_driving, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, isactive, created_at)
	                          VALUES (@user_id, @user_name, @trip_start_time, @is_self_driving, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @isactive, @created_at) RETURNING trip_id";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(tripEntity.user_id)));
                    Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(tripEntity.user_name)));
                    Parameters.Add(new NpgsqlParameter("@trip_start_time", CommonUtils.ConvertToDateTime(tripEntity.trip_start_time)));
                    Parameters.Add(new NpgsqlParameter("@is_self_driving", true));
                    Parameters.Add(new NpgsqlParameter("@p1", CommonUtils.ConvertToString(tripEntity.p1)));
                    Parameters.Add(new NpgsqlParameter("@p2", CommonUtils.ConvertToString(tripEntity.p2)));
                    Parameters.Add(new NpgsqlParameter("@p3", CommonUtils.ConvertToString(tripEntity.p3)));
                    Parameters.Add(new NpgsqlParameter("@p4", CommonUtils.ConvertToString(tripEntity.p4)));
                    Parameters.Add(new NpgsqlParameter("@p5", CommonUtils.ConvertToString(tripEntity.p5)));
                    Parameters.Add(new NpgsqlParameter("@p6", CommonUtils.ConvertToString(tripEntity.p6)));
                    Parameters.Add(new NpgsqlParameter("@p7", CommonUtils.ConvertToString(tripEntity.p7)));
                    Parameters.Add(new NpgsqlParameter("@p8", CommonUtils.ConvertToString(tripEntity.p8)));
                    Parameters.Add(new NpgsqlParameter("@p9", CommonUtils.ConvertToString(tripEntity.p9)));
                    Parameters.Add(new NpgsqlParameter("@p10", CommonUtils.ConvertToString(tripEntity.p10)));
                    Parameters.Add(new NpgsqlParameter("@isactive", true));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                    object objtrip_id = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

                    Int64 _t_id = CommonUtils.ConvertToInt(objtrip_id.ToString());
                    strSqlQry = @"SELECT * FROM trip_planning_master_sdk where trip_id=" + _t_id;
                    dtTrip = objDAL.FetchRecords(strSqlQry);

                }
                objDAL.CloseConnection();
                return dtTrip;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool EndTripSDK(IotRawDataTripEntity tripEntity)
        {
            try
            {
                if (CommonUtils.ConvertToInt(tripEntity.trip_id) == 0)
                {
                    strSqlQry = @"UPDATE trip_planning_master_sdk SET isactive=false, trip_end_time=@trip_end_time WHERE  trip_start_time =@trip_start_time and user_id=@user_id ";
                }
                else
                {
                    strSqlQry = @"UPDATE trip_planning_master_sdk SET isactive=false, trip_end_time=@trip_end_time, updated_at=@updated_at WHERE  trip_id=@trip_id ";

                }
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt(tripEntity.trip_id)));
                Parameters.Add(new NpgsqlParameter("@trip_end_time", CommonUtils.ConvertToDateTime(tripEntity.trip_end_time)));
                if (CommonUtils.ConvertToDateTime(tripEntity.trip_start_time) != null)
                {
                    Parameters.Add(new NpgsqlParameter("@trip_start_time", CommonUtils.ConvertToDateTime(tripEntity.trip_start_time)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@trip_start_time", DBNull.Value));
                }

                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToDecimal(tripEntity.user_id)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool TripEventLogSDK(IotRawDataTripEventLogEntity tripEventLogEntity)
        {
            try
            {
                objDAL.CreateConnection();

                Int64 user_id = 0;
                if (tripEventLogEntity.user_name != "")
                {
                    tripEventLogEntity.user_id = CommonUtils.ConvertToInt64(objDAL.FetchSingleRecord("select user_id from user_master where user_name='" + tripEventLogEntity.user_name.Trim() + "' limit 1"));

                    strSqlQry = @"INSERT INTO trip_planning_event_log_sdk( trip_id, user_id, user_name, event_time,lat,lng, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, created_at)
	                          VALUES (@trip_id, @user_id, @user_name, @event_time,@lat,@lng, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @created_at)";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt64(tripEventLogEntity.trip_id)));
                    Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(tripEventLogEntity.user_id)));
                    Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(tripEventLogEntity.user_name)));
                    Parameters.Add(new NpgsqlParameter("@event_time", CommonUtils.ConvertToDateTime(tripEventLogEntity.event_time)));
                    Parameters.Add(new NpgsqlParameter("@lat", CommonUtils.ConvertToString(tripEventLogEntity.lat)));
                    Parameters.Add(new NpgsqlParameter("@lng", CommonUtils.ConvertToString(tripEventLogEntity.lng)));
                    Parameters.Add(new NpgsqlParameter("@p1", CommonUtils.ConvertToString(tripEventLogEntity.p1)));
                    Parameters.Add(new NpgsqlParameter("@p2", CommonUtils.ConvertToString(tripEventLogEntity.p2)));
                    Parameters.Add(new NpgsqlParameter("@p3", CommonUtils.ConvertToString(tripEventLogEntity.p3)));
                    Parameters.Add(new NpgsqlParameter("@p4", CommonUtils.ConvertToString(tripEventLogEntity.p4)));
                    Parameters.Add(new NpgsqlParameter("@p5", CommonUtils.ConvertToString(tripEventLogEntity.p5)));
                    Parameters.Add(new NpgsqlParameter("@p6", CommonUtils.ConvertToString(tripEventLogEntity.p6)));
                    Parameters.Add(new NpgsqlParameter("@p7", CommonUtils.ConvertToString(tripEventLogEntity.p7)));
                    Parameters.Add(new NpgsqlParameter("@p8", CommonUtils.ConvertToString(tripEventLogEntity.p8)));
                    Parameters.Add(new NpgsqlParameter("@p9", CommonUtils.ConvertToString(tripEventLogEntity.p9)));
                    Parameters.Add(new NpgsqlParameter("@p10", CommonUtils.ConvertToString(tripEventLogEntity.p10)));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);


                }
                objDAL.CloseConnection();
                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }



        public static string ValidateTrip(IotRawDataTripEntity tripEntity,
             bool trip_id = false,
             bool user_name = false,
             bool user_id = false,
             bool trip_start_time = false,
             bool trip_end_time = false

      )
        {
            string strResult = "";

            // check  tripEntity
            if (tripEntity == null)
            {
                //return "empty trip entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.trip_entity);
            }


            // check  trip_id
            if (trip_id)
            {
                if (tripEntity.trip_id == 0)
                {
                    // return "trip_id required";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_id);

                }
            }


            // check  user_id
            if (user_id)
            {
                if (tripEntity.user_id == null || tripEntity.user_id == 0)
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }
            }

            // check  user_id
            if (user_name)
            {
                if (tripEntity.user_name == null || tripEntity.user_name == "")
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_name);
                }
            }

            // check  trip_start_time
            if (trip_start_time)
            {
                if (tripEntity.trip_start_time == null)
                {
                    // return "trip_start_time required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_start_time);
                }
            }

            if (trip_end_time)
            {
                if (tripEntity.trip_end_time == null)
                {
                    // return "trip_end_time required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_end_time);
                }
            }


            return strResult;
        }

        public static string ValidateTripEventLog(IotRawDataTripEventLogEntity tripEventLogEntity,
           bool trip_id = false,
           bool user_name = false,
           bool user_id = false,
           bool event_time = false
        )
        {
            string strResult = "";

            // check  tripEntity
            if (tripEventLogEntity == null)
            {
                //return "empty trip entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.trip_event_entity);
            }


            // check  trip_id
            if (trip_id)
            {
                if (tripEventLogEntity.trip_id == 0)
                {
                    // return "trip_id required";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_id);

                }
            }

            // check  user_id
            if (user_name)
            {
                if (tripEventLogEntity.user_name == null || tripEventLogEntity.user_name == "")
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_name);
                }
            }

            // check  trip_start_time
            if (event_time)
            {
                if (tripEventLogEntity.event_time == null)
                {
                    // return "trip_start_time required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.event_time);
                }
            }

            return strResult;
        }


        public static ResponseEntity getOkResponse()
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = true;
            objResponseEntity.Description = "success";
            objResponseEntity.data = null;
            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

        public static ResponseEntity getOkResponse(object data)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = true;
            objResponseEntity.Description = "success";
            objResponseEntity.data = data;
            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

    }

}


